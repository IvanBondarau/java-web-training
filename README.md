### Java Web Training

This repository contains solutions for the Java Web Training course. The course was conducted by the EPAM SYSTEMS laboratory in the fall-winter of 2019.

The repository consists of two folders. The "tasks" folder contains solutions to the course’s training tasks on various topics, such as multithreading, logging, working with databases, etc.

The "auction" folder contains the final project of the course. The project is a platform for online auctions. The project implements the possibilities of putting things up for auction, making bids, choosing the winner of the auction at the highest bid.

The project is implemented as a classic Java Servlets web application. As a template engine is used JSP. The project uses the PostgeSQL database through JDBC.