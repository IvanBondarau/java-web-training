package by.epam.training.parser;

import by.epam.training.model.Text;
import by.epam.training.model.TextLeaf;

import java.util.Optional;

public class TextParser extends TextLeafParser {

    private Text currentText;

    public TextParser() {
        currentText = new Text();
    }

    @Override
    public Optional<TextLeaf> parseLine(String line) {

        Optional<TextLeaf> parseResult = parseNext(line);

        parseResult.ifPresent(textLeaf -> currentText.addTextLeaf(textLeaf));

        return Optional.empty();

    }

    @Override
    public TextLeaf finish() {
        TextLeaf next = finishNext();
        if (next != null) {
            currentText.addTextLeaf(next);
        }

        return currentText;
    }
}
