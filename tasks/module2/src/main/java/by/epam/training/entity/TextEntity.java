package by.epam.training.entity;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class TextEntity implements Entity {

    private long id;
    private long number;

    public TextEntity(long number) {
        this.number = number;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TextEntity that = (TextEntity) o;
        return id == that.id &&
                number == that.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number);
    }
}
