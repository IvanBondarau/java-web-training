package by.epam.training.model;

import java.util.ArrayList;
import java.util.List;

public class Text implements TextComposite {

    private List<TextLeaf> paragraphs;

    public Text() {
        paragraphs = new ArrayList<>();
    }

    @Override
    public void addTextLeaf(TextLeaf textLeaf) {
        paragraphs.add(textLeaf);
    }

    @Override
    public List<TextLeaf> getAllLeafs() {
        return paragraphs;
    }

    @Override
    public String getText() {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < paragraphs.size(); i++) {
            result.append(paragraphs.get(i).getText());
        }

        return result.toString();
    }


}
