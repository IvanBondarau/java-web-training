package by.epam.training.validator;

import org.apache.log4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidator {

    private Logger LOGGER = Logger.getLogger(FileValidator.class);

    public ValidationResult validate(String pathToFIle) {

        ValidationResult result = new ValidationResult();

        try {
            URI fileUri = new URI("file:///" + pathToFIle);
            Path filePath = Paths.get(fileUri.getPath());
            if (Files.exists(filePath)) {
                if (Files.isRegularFile(filePath)) {
                    if (Files.isReadable(filePath)) {
                        return result;
                    } else {
                        result.addError(ErrorCode.FILE_NOT_READABLE, filePath + " not is not readable");
                    }
                } else {
                    result.addError(ErrorCode.NOT_A_FILE, filePath + " is not a file");
                }
            } else {
                result.addError(ErrorCode.FILE_DOESNT_EXIST, "File" + filePath + " doesn't exist");
            }

        } catch (URISyntaxException e) {
            result.addError(ErrorCode.INVALID_PATH, e.getMessage());
            return result;
        }

        return result;
    }
}
