package by.epam.training.entity;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class LexemeEntity implements Entity {

    private String lexeme;
    private long id;
    private long numInSentence;
    private long sentenceId;

    public LexemeEntity(String lexeme, long sentenceId, long numInSentence) {
        this.lexeme = lexeme;
        this.sentenceId = sentenceId;
        this.numInSentence = numInSentence;
    }

    public String getLexeme() {
        return lexeme;
    }

    public void setLexeme(String lexeme) {
        this.lexeme = lexeme;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(long sentenceId) {
        this.sentenceId = sentenceId;
    }

    public long getNumInSentence() {
        return numInSentence;
    }

    public void setNumInSentence(long numInSentence) {
        this.numInSentence = numInSentence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LexemeEntity entity = (LexemeEntity) o;
        return id == entity.id &&
                numInSentence == entity.numInSentence &&
                sentenceId == entity.sentenceId &&
                Objects.equals(lexeme, entity.lexeme);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lexeme, id, numInSentence, sentenceId);
    }
}
