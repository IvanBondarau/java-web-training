package by.epam.training.controller;

import by.epam.training.model.*;
import by.epam.training.parser.*;
import by.epam.training.reader.FileLineReader;
import by.epam.training.service.TextService;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.ValidationResult;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

public class TextController {

    private FileValidator fileValidator;
    private TextService textService;
    private FileLineReader lineReader;

    private ParserChain<TextLeaf> parser;

    public TextController(FileValidator validator, TextService textService) {
        this.fileValidator = validator;
        this.textService = textService;
    }


    public void loadTextFromFile(String pathToFile, int textNumber) throws IOException {

        ValidationResult validationResult = fileValidator.validate(pathToFile);

        if (!validationResult.isValid()) {
            throw new IOException("File path is invalid.");
        }

        rebuildParserChain();

        lineReader = new FileLineReader(pathToFile);
        String line;
        while ((line = lineReader.readLine()) != null) {
            parser.parseLine(line);
        }

        Text text = (Text)parser.finish();
        textService.save(textNumber, text);

    }

    public Optional<Text> findTextByNumber(int textNumber) {
        return textService.findTextByNumber(textNumber);

    }

    public void sortWordsInSentences(Text text) {
        textService.sortWordsByLength(text);
    }

    private void rebuildParserChain() {
        LexemeParser lexemeParser = new LexemeParser();
        SentenceParser sentenceParser = new SentenceParser();
        ParagraphParser paragraphParser = new ParagraphParser();
        TextParser textParser = new TextParser();

        parser = lexemeParser.linkWith(sentenceParser).linkWith(paragraphParser).linkWith(textParser);

    }
}
