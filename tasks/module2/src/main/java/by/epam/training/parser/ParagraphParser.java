package by.epam.training.parser;

import by.epam.training.model.Paragraph;
import by.epam.training.model.TextLeaf;
import org.apache.log4j.Logger;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser extends TextLeafParser {

    private static final Logger LOGGER = Logger.getLogger(ParagraphParser.class);

    private static final Pattern SENTENCE_PATTERN = Pattern.compile("([A-Z])(.|\n)+?([.?!])+\n?");

    private Paragraph currentParagraph;
    private String remainder;

    @Override
    public Optional<TextLeaf> parseLine(String line) {

        if (remainder != null) {
            line = remainder + line;
            remainder = null;
        }

        Optional<TextLeaf> ans;
        if (line.startsWith("\t")) {
            ans = Optional.ofNullable(currentParagraph);
            currentParagraph = new Paragraph();
        } else {
            ans = Optional.empty();
            if (currentParagraph == null) {
                currentParagraph = new Paragraph();
            }


        }

        line = line.trim();
        if (!line.endsWith("\n")) {
            line = line + "\n";
        }

        if (line.length() > 0 && Character.isAlphabetic(line.charAt(0))) {
            if (Character.isLowerCase(line.charAt(0))) {
                line = line.length() > 1 ? Character.toUpperCase(line.charAt(0)) + line.substring(1)
                        : line.toUpperCase();
            }
        }

        Matcher matcher = SENTENCE_PATTERN.matcher(line);

        int lastGroupIndex = 0;
        while (matcher.find()) {
            Optional<TextLeaf> parseResult = parseNext(matcher.group());

            parseResult.ifPresent(textLeaf -> currentParagraph.addTextLeaf(textLeaf));

            lastGroupIndex = matcher.end();
        }

        if (lastGroupIndex != line.length()) {
            remainder = line.substring(lastGroupIndex);
        }

        return ans;

    }

    @Override
    public TextLeaf finish() {
        if (remainder != null && remainder.length() != 0) {
            currentParagraph.addTextLeaf(parseNext(remainder).get());
        }
        return currentParagraph.getAllLeafs().isEmpty() ? null : currentParagraph;
    }
}
