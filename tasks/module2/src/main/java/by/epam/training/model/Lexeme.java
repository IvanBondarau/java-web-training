package by.epam.training.model;

public class Lexeme implements TextLeaf {

    private String prefix;
    private String word;
    private String suffix;


    @Override
    public String getText() {
        return prefix + word + suffix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }


}
