package by.epam.training.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

public class FileLineReader {

    private String path;
    private BufferedReader reader;

    public FileLineReader(String pathToFile) {
        this.path = pathToFile;
    }

    public String readLine() throws IOException {

        if (reader == null) {
            reader = new BufferedReader(new java.io.FileReader(path));
        }

        return reader.readLine();


    }
}
