package by.epam.training.model;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Paragraph implements TextComposite {

    private static final Logger LOGGER = Logger.getLogger(Paragraph.class);

    private List<TextLeaf> sentences;

    public Paragraph() {
        sentences = new ArrayList<>();
    }

    @Override
    public void addTextLeaf(TextLeaf textLeaf) {
        sentences.add(textLeaf);
    }

    @Override
    public List<TextLeaf> getAllLeafs() {
        return sentences;
    }

    @Override
    public String getText() {
        StringBuilder result = new StringBuilder();

        result.append("\t");

        for (int i = 0; i < sentences.size() - 1; i++) {
            String sentence = sentences.get(i).getText();
            result.append(sentence);
            if (!sentence.endsWith("\n")) {
                result.append(" ");
            }
        }

        result.append(sentences.get(sentences.size() - 1).getText());

        return result.toString();
    }
}
