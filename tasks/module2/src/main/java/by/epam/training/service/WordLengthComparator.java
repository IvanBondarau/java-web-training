package by.epam.training.service;

import by.epam.training.model.Lexeme;
import by.epam.training.model.TextLeaf;

import java.util.Comparator;

public class WordLengthComparator implements Comparator<TextLeaf> {

    @Override
    public int compare(TextLeaf textLeaf, TextLeaf t1) {
        return Integer.compare(((Lexeme)textLeaf).getWord().length(), ((Lexeme)t1).getWord().length());
    }
}
