package by.epam.training.service;

import by.epam.training.entity.LexemeEntity;
import by.epam.training.model.Lexeme;
import by.epam.training.parser.LexemeParser;
import by.epam.training.repository.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class LexemeService implements Service<Lexeme> {

    private Repository<LexemeEntity> repository;
    private LexemeParser parser;

    public LexemeService(Repository<LexemeEntity> repository) {
        this.repository = repository;
        parser = new LexemeParser();
    }

    public long save(long parentId, long numInSentence, Lexeme item) {
        LexemeEntity entity = new LexemeEntity(item.getText(), parentId, numInSentence);
        return repository.create(entity);
    }

    @Override
    public Lexeme load(long id) {
        LexemeEntity entity = repository.read(id);
        return (Lexeme)parser.parseLine(entity.getLexeme()).get();
    }

    public List<Lexeme> filterBySentenceId(long sentenceId) {
        List<LexemeEntity> entities = repository.filter((LexemeEntity entity) -> entity.getSentenceId() == sentenceId);
        return entities.stream()
                .sorted(Comparator.comparingLong(LexemeEntity::getNumInSentence))
                .map((LexemeEntity entity) -> load(entity.getId()))
                .collect(Collectors.toList());
    }
}
