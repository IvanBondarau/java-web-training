package by.epam.training.entity;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class SentenceEntity implements Entity{

    private long id;
    private long paragraphId;
    private long numInParagraph;


    public SentenceEntity(long paragraphId, long numInParagraph) {
        this.paragraphId = paragraphId;
        this.numInParagraph = numInParagraph;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getParagraphId() {
        return paragraphId;
    }

    public void setParagraphId(long paragraphId) {
        this.paragraphId = paragraphId;
    }

    public long getNumInParagraph() {
        return numInParagraph;
    }

    public void setNumInParagraph(long numInParagraph) {
        this.numInParagraph = numInParagraph;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SentenceEntity entity = (SentenceEntity) o;


        return id == entity.id &&
                paragraphId == entity.paragraphId &&
                numInParagraph == entity.numInParagraph;


    }

    @Override
    public int hashCode() {
        return Objects.hash(id, paragraphId, numInParagraph);
    }
}
