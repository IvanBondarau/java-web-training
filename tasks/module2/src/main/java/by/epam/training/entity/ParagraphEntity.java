package by.epam.training.entity;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class ParagraphEntity implements Entity {

    private long id;
    private long textId;
    private long numInText;


    public ParagraphEntity(long textId, long numInText) {
        this.textId = textId;
        this.numInText = numInText;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getTextId() {
        return textId;
    }

    public void setTextId(long textId) {
        this.textId = textId;
    }

    public long getNumInText() {
        return numInText;
    }

    public void setNumInText(long numInText) {
        this.numInText = numInText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParagraphEntity that = (ParagraphEntity) o;
        return id == that.id &&
                textId == that.textId &&
                numInText == that.numInText;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, textId, numInText);
    }
}
