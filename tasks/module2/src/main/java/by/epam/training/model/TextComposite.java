package by.epam.training.model;

import java.util.List;

public interface TextComposite extends TextLeaf {

    public void addTextLeaf(TextLeaf textLeaf);

    public List<TextLeaf> getAllLeafs();
}

