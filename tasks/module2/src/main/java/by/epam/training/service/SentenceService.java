package by.epam.training.service;

import by.epam.training.entity.SentenceEntity;
import by.epam.training.model.Lexeme;
import by.epam.training.model.Sentence;
import by.epam.training.repository.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SentenceService implements Service<Sentence> {

    private Repository<SentenceEntity> repository;
    private LexemeService lexemeService;

    public SentenceService(Repository<SentenceEntity> repository, LexemeService service) {
        this.repository = repository;
        this.lexemeService = service;
    }

    public long save(long parentId, long numInParagraph, Sentence item) {
        SentenceEntity entity = new SentenceEntity(parentId, numInParagraph);
        long id = repository.create(entity);

        for (int num = 0; num < item.getAllLeafs().size(); num++) {
            Lexeme lexeme = (Lexeme)item.getAllLeafs().get(num);
            lexemeService.save(id, num, lexeme);
        }

        return id;
    }

    @Override
    public Sentence load(long id) {
        Sentence sentence = new Sentence();
        lexemeService.filterBySentenceId(id).forEach(sentence::addTextLeaf);
        return sentence;
    }

    public List<Sentence> filterByParagraphId(long paragraphId) {
        List<SentenceEntity> entities = repository.filter((SentenceEntity entity) -> {
            return entity.getParagraphId() == paragraphId;
        });

        return entities.stream()
                .sorted(Comparator.comparingLong(SentenceEntity::getNumInParagraph))
                .map((SentenceEntity entity) -> load(entity.getId()))
                .collect(Collectors.toList());
    }
}
