package by.epam.training.service;

import by.epam.training.entity.ParagraphEntity;
import by.epam.training.model.Paragraph;
import by.epam.training.model.Sentence;
import by.epam.training.repository.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ParagraphService implements Service<Paragraph> {

    private Repository<ParagraphEntity> repository;
    private SentenceService sentenceService;

    public ParagraphService(Repository<ParagraphEntity> repository, SentenceService service) {
        this.repository = repository;
        this.sentenceService = service;
    }

    public long save(long parentId, long numInText, Paragraph item) {
        ParagraphEntity entity = new ParagraphEntity(parentId, numInText);
        long id = repository.create(entity);

        for (int num = 0; num < item.getAllLeafs().size(); num++) {
            Sentence sentence = (Sentence) item.getAllLeafs().get(num);
            sentenceService.save(id, num, sentence);
        }

        return id;
    }

    @Override
    public Paragraph load(long id) {
        Paragraph paragraph = new Paragraph();
        sentenceService.filterByParagraphId(id).forEach(paragraph::addTextLeaf);
        return paragraph;
    }

    public List<Paragraph> filterByTextId(long textId) {
        List<ParagraphEntity> entities = repository.filter((ParagraphEntity entity) -> entity.getTextId() == textId);
        return entities.stream()
                .sorted(Comparator.comparingLong(ParagraphEntity::getNumInText))
                .map((ParagraphEntity entity) -> load(entity.getId()))
                .collect(Collectors.toList());
    }
}
