package by.epam.training.parser;

import by.epam.training.model.Sentence;
import by.epam.training.model.TextLeaf;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser extends TextLeafParser {

    private static final Pattern LEXEME_PATTERN = Pattern.compile("(['\"(]+)?(\\w|\\d)+([-'](\\w|\\d)+)*([\".,'):?!\n]+)?");

    @Override
    public Optional<TextLeaf> parseLine(String line) {

        Matcher matcher = LEXEME_PATTERN.matcher(line);

        Sentence sentence = new Sentence();

        while (matcher.find()) {
            Optional<TextLeaf> parseResult = parseNext(matcher.group());
            parseResult.ifPresent(sentence::addTextLeaf);
        }

        return Optional.of(sentence);
    }

    @Override
    public TextLeaf finish() {
        return null;
    }
}
