package by.epam.training.service;

public interface Service<T> {

    T load(long id);
}
