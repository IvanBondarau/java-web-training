package by.epam.training.repository;

@FunctionalInterface
public interface Specification<T> {

    boolean match(T item);

    default Specification<T> and(Specification<T> other) {
        return (item) -> this.match(item) && other.match(item);
    }

    default Specification<T> or(Specification<T> other) {
        return (item) -> this.match(item) || other.match(item);

    }
}
