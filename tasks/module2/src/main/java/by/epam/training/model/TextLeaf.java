package by.epam.training.model;

public interface TextLeaf {

    public String getText();
}
