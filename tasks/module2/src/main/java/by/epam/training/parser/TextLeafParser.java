package by.epam.training.parser;

import by.epam.training.model.TextLeaf;

import java.util.Optional;

public abstract class TextLeafParser implements ParserChain<TextLeaf> {

    private TextLeafParser next;

    @Override
    public ParserChain<TextLeaf> linkWith(ParserChain<TextLeaf> next) {
        ((TextLeafParser) next).next = this;
        return next;
    }

    protected Optional<TextLeaf> parseNext(String line) {
        if (next != null) {
            return next.parseLine(line);
        } else {
            return Optional.empty();
        }
    }

    public abstract TextLeaf finish();

    public TextLeaf finishNext() {
        return next.finish();
    }
}
