package by.epam.training.repository;

import by.epam.training.entity.Entity;
import by.epam.training.entity.LexemeEntity;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class EntityRepository<T extends Entity> implements Repository<T> {

    private static final AtomicLong idCounter = new AtomicLong(0);

    protected Map<Long, T> items;

    public EntityRepository() {
        items = new HashMap<>();
    }

    @Override
    public long create(T item) {
        item.setId(idCounter.getAndAdd(1));
        items.put(item.getId(), item);
        return item.getId();
    }

    @Override
    public T read(long id) {
        if (items.containsKey(id)) {
            return items.get(id);
        } else {
            throw new IllegalArgumentException("Invalid id");
        }
    }

    @Override
    public void delete(long id) {
        items.remove(id);
    }


    @Override
    public void update(T item) {
        if (items.containsKey(item.getId())) {
            items.put(item.getId(), item);
        }
    }


    @Override
    public List<T> getAll() {
        return new ArrayList<>(items.values());
    }


    @Override
    public List<T> filter(Specification<T> spec) {
        return items.values().stream()
                .filter(spec::match)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<T> find(Specification<T> spec) {
        return items.values().stream().filter(spec::match).findFirst();
    }
}
