package by.epam.training.parser;

import by.epam.training.model.Lexeme;
import by.epam.training.model.TextLeaf;
import org.apache.log4j.Logger;

import java.util.Optional;

public class LexemeParser extends TextLeafParser implements Cloneable {

    private static final Logger LOGGER = Logger.getLogger(LexemeParser.class);

    private int prefixEnd;
    private int suffixStart;

    @Override
    public Optional<TextLeaf> parseLine(String line) {

        Lexeme lexeme = new Lexeme();

        setPrefix(lexeme, line);
        setSuffix(lexeme, line);

        setWord(lexeme, line);

        return Optional.of(lexeme);

    }

    public void setWord(Lexeme lexeme, String text) {
        lexeme.setWord(text.substring(prefixEnd, suffixStart));
    }

    private void setPrefix(Lexeme lexeme, String text) {

        prefixEnd = 0;

        while (prefixEnd < (text.length() - 1) &&
                !Character.isAlphabetic(text.charAt(prefixEnd))) {
            prefixEnd++;
        }

        lexeme.setPrefix(text.substring(0, prefixEnd));
    }

    private void setSuffix(Lexeme lexeme, String text) {

        suffixStart = text.length() - 1;

        while (suffixStart > prefixEnd &&
                !Character.isAlphabetic(text.charAt(suffixStart))) {
            suffixStart--;
        }

        suffixStart++;

        lexeme.setSuffix(text.substring(suffixStart));
    }

    @Override
    public TextLeaf finish() {
        return null;
    }


}
