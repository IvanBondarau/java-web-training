package by.epam.training.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T> {

    long create(T item);
    T read(long id);
    void update(T item);
    void delete(long id);

    List<T> getAll();
    List<T> filter(Specification<T> spec);
    Optional<T> find(Specification<T> spec);
}
