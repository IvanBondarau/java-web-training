package by.epam.training.service;

import by.epam.training.entity.TextEntity;
import by.epam.training.model.Paragraph;
import by.epam.training.model.Sentence;
import by.epam.training.model.Text;
import by.epam.training.model.TextLeaf;
import by.epam.training.repository.Repository;

import java.util.Optional;

public class TextService implements Service<Text> {

    private Repository<TextEntity> repository;
    private ParagraphService paragraphService;

    public TextService(Repository<TextEntity> repository, ParagraphService service) {
        this.repository = repository;
        this.paragraphService = service;
    }

    public long save(long number, Text item) {
        TextEntity entity = new TextEntity(number);
        long id = repository.create(entity);

        for (int num = 0; num < item.getAllLeafs().size(); num++) {
            Paragraph paragraph = (Paragraph) item.getAllLeafs().get(num);
            paragraphService.save(id, num, paragraph);
        }

        return id;
    }

    @Override
    public Text load(long id) {
        Text text = new Text();
        paragraphService.filterByTextId(id).forEach(text::addTextLeaf);
        return text;
    }

    public Optional<Text> findTextByNumber(int number) {
        Optional<TextEntity> searchResult = repository.find((TextEntity textEntity)->textEntity.getNumber() == number);
        return searchResult.map(textEntity -> load(textEntity.getId()));
    }

    public void sortWordsByLength(Text text) {
        text.getAllLeafs().stream()
                .flatMap((TextLeaf paragraph) -> ((Paragraph)paragraph).getAllLeafs().stream())
                .forEach((TextLeaf sentence) -> ((Sentence)sentence).sortLexemes(new WordLengthComparator()));
    }


}
