package by.epam.training.parser;

import java.util.Optional;

public interface ParserChain<T> {

    Optional<T> parseLine(String line);

    ParserChain<T> linkWith(ParserChain<T> next);

    T finish();
}
