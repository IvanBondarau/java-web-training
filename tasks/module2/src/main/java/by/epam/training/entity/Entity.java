package by.epam.training.entity;

public interface Entity {

    void setId(long id);
    long getId();
}
