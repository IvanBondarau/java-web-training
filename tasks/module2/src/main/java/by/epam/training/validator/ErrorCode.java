package by.epam.training.validator;

public enum ErrorCode {
    INVALID_PATH,
    FILE_DOESNT_EXIST,
    NOT_A_FILE,
    FILE_NOT_READABLE
}
