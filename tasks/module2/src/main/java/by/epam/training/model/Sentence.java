package by.epam.training.model;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Sentence implements TextComposite{

    private static final Logger LOGGER = Logger.getLogger(Sentence.class);

    private List<TextLeaf> words;

    public Sentence() {
        words = new ArrayList<>();
    }

    @Override
    public void addTextLeaf(TextLeaf textLeaf) {
        words.add(textLeaf);
    }

    @Override
    public List<TextLeaf> getAllLeafs() {
        return words;
    }

    @Override
    public String getText() {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < words.size() - 1; i++) {
            result.append(words.get(i).getText());
            if (!words.get(i).getText().endsWith("\n")) {
                result.append(" ");
            }
        }

        result.append(words.get(words.size() - 1).getText());

        return result.toString();
    }

    public void sortLexemes(Comparator<TextLeaf> comparator) {
        words.sort(comparator);
    }
}
