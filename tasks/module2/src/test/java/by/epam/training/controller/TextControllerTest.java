package by.epam.training.controller;

import by.epam.training.entity.LexemeEntity;
import by.epam.training.entity.ParagraphEntity;
import by.epam.training.entity.SentenceEntity;
import by.epam.training.entity.TextEntity;
import by.epam.training.model.Text;
import by.epam.training.repository.EntityRepository;
import by.epam.training.repository.Repository;
import by.epam.training.service.LexemeService;
import by.epam.training.service.ParagraphService;
import by.epam.training.service.SentenceService;
import by.epam.training.service.TextService;
import by.epam.training.validator.FileValidator;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TextControllerTest {

    private static final Logger LOGGER = Logger.getLogger(TextControllerTest.class);

    private Repository<TextEntity> textRepository;
    private Repository<ParagraphEntity> paragraphRepository;
    private Repository<SentenceEntity> sentenceRepository;
    private Repository<LexemeEntity> lexemeRepository;

    private TextService textService;

    private TextController textController;

    @Before
    public void init() {
        initRepository();
        initService();
        initController();
    }

    @Test
    public void loadFromFileValid() throws IOException {
        String path = TextControllerTest.class.getResource("/sample.txt").getPath();

        textController.loadTextFromFile(path, 1);
        assertTrue(textService.findTextByNumber(1).isPresent());
    }

    @Test(expected = IOException.class)
    public void invalidPathIOException() throws IOException {
        String path = "Invalid";
        textController.loadTextFromFile(path, 1);
    }

    @Test
    public void loadFindAndSortValid() throws IOException {
        String path = TextControllerTest.class.getResource("/sample.txt").getPath();

        textController.loadTextFromFile(path, 1);
        assertTrue(textService.findTextByNumber(1).isPresent());

        Optional<Text> searchResult;
        if ((searchResult = textController.findTextByNumber(1)).isPresent()) {
            textController.sortWordsInSentences(searchResult.get());
        }

        assertTrue(searchResult.isPresent());
    }


    private void initRepository() {
        textRepository = new EntityRepository<>();
        paragraphRepository = new EntityRepository<>();
        sentenceRepository = new EntityRepository<>();
        lexemeRepository = new EntityRepository<>();
    }

    private void initService() {

        LexemeService lexemeService = new LexemeService(lexemeRepository);
        SentenceService sentenceService = new SentenceService(sentenceRepository, lexemeService);
        ParagraphService paragraphService = new ParagraphService(paragraphRepository, sentenceService);
        textService = new TextService(textRepository, paragraphService);
    }

    private void initController() {

        FileValidator validator = new FileValidator();
        textController = new TextController(validator, textService);
    }

}