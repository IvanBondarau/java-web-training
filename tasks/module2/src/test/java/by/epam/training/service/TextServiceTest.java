package by.epam.training.service;

import by.epam.training.entity.LexemeEntity;
import by.epam.training.entity.ParagraphEntity;
import by.epam.training.entity.SentenceEntity;
import by.epam.training.entity.TextEntity;
import by.epam.training.model.*;
import by.epam.training.repository.EntityRepository;
import by.epam.training.repository.Repository;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith(JUnit4.class)
public class TextServiceTest {

    private static final Logger LOGGER = Logger.getLogger(TextService.class);

    private Repository<TextEntity> textRepository;
    private Repository<ParagraphEntity> paragraphRepository;
    private Repository<SentenceEntity> sentenceRepository;
    private Repository<LexemeEntity> lexemeRepository;

    private TextService textService;

    private Text text;



    @Before
    public void init() {

        initRepository();
        initService();
        initText();
    }

    @Test
    public void saveLoadTestOk() {

        long savedID = textService.save(1, text);
        Text loaded = textService.load(savedID);

        LOGGER.info(loaded.getText());

        Assert.assertEquals(text.getText(), loaded.getText());
    }


    private void initRepository() {
        textRepository = new EntityRepository<>();
        paragraphRepository = new EntityRepository<>();
        sentenceRepository = new EntityRepository<>();
        lexemeRepository = new EntityRepository<>();
    }

    private void initService() {

        LexemeService lexemeService = new LexemeService(lexemeRepository);
        SentenceService sentenceService = new SentenceService(sentenceRepository, lexemeService);
        ParagraphService paragraphService = new ParagraphService(paragraphRepository, sentenceService);
        textService = new TextService(textRepository, paragraphService);
    }


    private void initText() {
        Lexeme lexeme1 = new Lexeme();
        lexeme1.setPrefix("");
        lexeme1.setSuffix("");
        lexeme1.setWord("test");

        Lexeme lexeme2 = new Lexeme();
        lexeme2.setPrefix("");
        lexeme2.setSuffix(".");
        lexeme2.setWord("check1");

        Sentence sentence1 = new Sentence();
        sentence1.addTextLeaf(lexeme1);
        sentence1.addTextLeaf(lexeme2);

        Paragraph paragraph1 = new Paragraph();
        paragraph1.addTextLeaf(sentence1);

        text = new Text();
        text.addTextLeaf(paragraph1);

    }

}
