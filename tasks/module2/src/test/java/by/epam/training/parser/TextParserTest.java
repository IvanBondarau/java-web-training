package by.epam.training.parser;

import by.epam.training.model.Text;
import by.epam.training.model.TextLeaf;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@RunWith(JUnit4.class)
public class TextParserTest {

    private static final Logger LOGGER = Logger.getLogger(TextParserTest.class);

    private ParserChain<TextLeaf> parser;

    @Before
    public void init() {
        LexemeParser lexemeParser = new LexemeParser();
        SentenceParser sentenceParser = new SentenceParser();
        ParagraphParser paragraphParser = new ParagraphParser();
        TextParser textParser = new TextParser();

        parser = lexemeParser.linkWith(sentenceParser).linkWith(paragraphParser).linkWith(textParser);
    }


    @Test
    public void simpleSentenceOk() {
        String test = "\tJust simple sentence.";

        parser.parseLine(test);
        TextLeaf text = parser.finish();
        Assert.assertEquals(text.getText(), test + "\n");

        LOGGER.info("***\n" + text.getText() + "\n***");
    }

    @Test
    public void complexSentenceOk() {
        String test = "\tIt's much, \"MUCH\" more-more complicated sentence!!";

        parser.parseLine(test);
        TextLeaf text = parser.finish();

        LOGGER.info("***\n" + text.getText() + "\n***");

        Assert.assertEquals(text.getText(), test + "\n");

    }

    @Test
    public void multilineSentenceOk() {
        String line1 = "\tIt's the beginning";
        String line2 = "and the end.";

        parser.parseLine(line1);
        parser.parseLine(line2);
        TextLeaf text = parser.finish();


        LOGGER.info("***\n" + text.getText() + "\n***");

        Assert.assertEquals(text.getText(), line1 + "\n" + line2 + "\n");

    }

    @Test
    public void multipleSentencesOk() {
        String line1 = "\tSentence 1. Then ";
        String line2 = "sentence 2. Next 3 on one line.";
        String line3 = "Sentence 4: the end!";

        parser.parseLine(line1);
        parser.parseLine(line2);
        parser.parseLine(line3);

        Text text = (Text)parser.finish();

        LOGGER.info("***\n" + text.getText() + "\n***");

        Assert.assertEquals("\t" + line1.trim() + "\n" + line2 + "\n" + line3 + "\n", text.getText());
    }

    @Test
    public void multiParagraphsOk() {
        String line1 = "\tParagraph 1. ";
        String line2 = "\tParagraph 2!";

        parser.parseLine(line1);
        parser.parseLine(line2);

        Text text = (Text)parser.finish();

        LOGGER.info("***\n" + text.getText() + "\n***");
    }

    @Test
    public void oneWordOk() {
        String line = "check";

        parser.parseLine(line);

        Text text = (Text)parser.finish();

        Assert.assertEquals("\tCheck\n", text.getText());

        LOGGER.info("***\n" + text.getText() + "\n***");
    }

    @Test
    public void oneWordAndSentenceOk() {
        String line = "check. Check!";

        parser.parseLine(line);

        Text text = (Text)parser.finish();

        Assert.assertEquals("\tCheck. Check!\n", text.getText());

        LOGGER.info("***\n" + text.getText() + "\n***");
    }


    @Test
    public void bigTextFromFileOk() throws URISyntaxException, IOException {
        URL sample = TextParserTest.class.getResource("/sample.txt");
        List<String> lines = Files.readAllLines(Paths.get(sample.toURI()));

        StringBuilder ans = new StringBuilder();
        for (String str: lines) {
            parser.parseLine(str);
            ans.append(str);
            ans.append("\n");
        }

        TextLeaf text = parser.finish();

        LOGGER.info("***\n" + text.getText() + "\n***");

        Assert.assertEquals(ans.toString(), text.getText());
    }
}