package by.epam.training.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class CrossingTest {

    @Before
    public void init() {

        List<ParkingSpace> parkingSpaces = new ArrayList<>();

        parkingSpaces.add(new ParkingSpace(1, 10, 10));
        parkingSpaces.add(new ParkingSpace(2, 5, 15));
        parkingSpaces.add(new ParkingSpace(3, 20, 20));

        Ferry ferry = new Ferry(70, 100, parkingSpaces);

        Crossing.getInstance().setFerry(ferry);


    }


    @Test
    public void start() throws InterruptedException {
        Car car1 = new Car("First car", 20, 7, 7);
        Car car2 = new Car("Second car",40, 15, 6);
        Car car3 = new Car("Third car", 30, 5, 5);
        Car car4 = new Car("Fourth car", 15, 15, 15);

        car1.start();
        car2.start();
        car3.start();
        car4.start();

        TimeUnit.MILLISECONDS.sleep(100);

        for (int i = 0; i < 10; i++) {
            if (Crossing.getInstance().isQueueEmpty()) {
                Crossing.getInstance().finishFerry();
                break;
            } else {
                Crossing.getInstance().startFerry();
                TimeUnit.MILLISECONDS.sleep(300);
            }
        }
    }


}