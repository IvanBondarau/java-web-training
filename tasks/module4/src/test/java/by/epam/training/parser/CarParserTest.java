package by.epam.training.parser;

import by.epam.training.model.Car;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class CarParserTest {


    private static final Logger LOGGER = Logger.getLogger(CarParserTest.class);

    private Parser<List<Car>> parser;

    @Before
    public void init() {
        parser = new CarParser();
    }

    @Test
    public void testCarListValid() throws ParserException {
        String path = CarParserTest.class.getResource("/testCarList.xml").getPath();

        List<Car> cars = parser.parse(path);

        LOGGER.info(cars);


    }
}