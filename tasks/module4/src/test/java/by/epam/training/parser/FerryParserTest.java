package by.epam.training.parser;

import by.epam.training.model.Ferry;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FerryParserTest {

    private static final Logger LOGGER = Logger.getLogger(FerryParserTest.class);

    private Parser<Ferry> parser;

    @Before
    public void init() {
        parser = new FerryParser();
    }

    @Test
    public void testFerryValid() throws ParserException {
        String path = FerryParser.class.getResource("/testFerry.xml").getPath();
        Ferry ferry = parser.parse(path);

        LOGGER.info(ferry.toString());

    }

}