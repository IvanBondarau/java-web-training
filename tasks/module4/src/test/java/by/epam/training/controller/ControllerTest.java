package by.epam.training.controller;

import by.epam.training.model.Crossing;
import by.epam.training.parser.CarParser;
import by.epam.training.parser.CarParserTest;
import by.epam.training.parser.FerryParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.concurrent.TimeUnit;

@RunWith(JUnit4.class)
public class ControllerTest {

    private CarController carController;
    private FerryController ferryController;

    @Before
    public void init() {
        CarParser carParser = new CarParser();
        FerryParser ferryParser = new FerryParser();

        carController = new CarController(carParser);
        ferryController = new FerryController(ferryParser);
    }

    @Test
    public void loadAndStart() throws InterruptedException {
        String carPath = CarParserTest.class.getResource("/testCarList.xml").getPath();
        String ferryPath = FerryParser.class.getResource("/testFerry.xml").getPath();

        ferryController.loadFerryFromFile(ferryPath);
        carController.loadCarsFromFile(carPath);

        TimeUnit.MILLISECONDS.sleep(100);

        while (!Crossing.getInstance().isQueueEmpty()) {
            Crossing.getInstance().startFerry();
            TimeUnit.MILLISECONDS.sleep(1000);
        }

        /*while (!Crossing.getInstance().isQueueEmpty()) {
            Crossing.getInstance().startFerry();
            TimeUnit.SECONDS.sleep( 1);
        }*/
    }
}
