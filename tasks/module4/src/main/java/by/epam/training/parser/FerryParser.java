package by.epam.training.parser;

import by.epam.training.builder.FerryBuilder;
import by.epam.training.builder.ParkingSpaceBuilder;
import by.epam.training.model.Ferry;
import by.epam.training.model.ParkingSpace;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FerryParser implements Parser<Ferry> {

    public Ferry parse(String path) throws ParserException {

        Ferry resultFerry = null;

        Map<String, String> ferryParams = new HashMap<>();
        Map<String, String> parkingSpaceParams = new HashMap<>();

        ParkingSpaceBuilder parkingSpaceBuilder = new ParkingSpaceBuilder(parkingSpaceParams);
        List<ParkingSpace> parkingSpaces = new LinkedList<>();


        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader reader =
                    factory.createXMLStreamReader(new BufferedReader(new FileReader(path)));

            while (reader.hasNext()) {
                int event = reader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        switch (reader.getLocalName()) {
                            case "Ferry":
                                String maxWeight = reader.getAttributeValue(null, "maxWeight");
                                String runDuration = reader.getAttributeValue(null, "runDuration");
                                ferryParams.put("MaximumWeight", maxWeight);
                                ferryParams.put("RunDuration", runDuration);
                                break;
                            case "ParkingSpace":
                                String number = reader.getAttributeValue(null, "number");
                                String length = reader.getAttributeValue(null, "length");
                                String width = reader.getAttributeValue(null, "width");
                                parkingSpaceParams.put("Number", number);
                                parkingSpaceParams.put("Length", length);
                                parkingSpaceParams.put("Width", width);

                                parkingSpaces.add(parkingSpaceBuilder.build());
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        if ("Ferry".equals(reader.getLocalName())) {
                            FerryBuilder ferryBuilder = new FerryBuilder(ferryParams);
                            ferryBuilder.setParkingSpaces(parkingSpaces);
                            resultFerry = ferryBuilder.build();
                        }
                        break;
                }
            }
            
            return resultFerry;
        } catch (FileNotFoundException | XMLStreamException e) {
            throw new ParserException(e);
        }

    }

}
