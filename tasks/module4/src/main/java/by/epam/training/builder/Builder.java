package by.epam.training.builder;

public interface Builder<T> {
    T build();
}
