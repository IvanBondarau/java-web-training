package by.epam.training.model;

import org.apache.log4j.Logger;

import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ParkingSpace {

    private static final Logger LOGGER = Logger.getLogger(ParkingSpace.class);

    private final Lock lock = new ReentrantLock();

    private Ferry parentFerry;

    private int number;

    private double length;
    private double width;

    public boolean available = true;

    private Car locatedCar;

    public ParkingSpace(int number, double length, double width) {
        this.number = number;
        this.length = length;
        this.width = width;
    }

    public void setFerry(Ferry ferry) {
        lock.lock();
        this.parentFerry = ferry;
        lock.unlock();
    }

    public int getNumber() {
        return number;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public boolean isAvailable() {
        lock.lock();
        try {
            return available;
        } finally {
            lock.unlock();
        }
    }

    public Optional<Car> getLocatedCar() {
        lock.lock();
        try {
            return Optional.ofNullable(locatedCar);
        } finally {
            lock.unlock();
        }
    }

    public boolean apply(Car car) {
        lock.lock();
        try {
            if (!available) {
                return false;
            }
            if (car.getLength() <= length && car.getWidth() <= width) {
                available = false;
                LOGGER.info("Space " + number + " applied by " + car);
                return true;
            } else {
                return false;
            }

        } finally {
            lock.unlock();
        }
    }

    public void allocate(Car car) {
        lock.lock();
        try {
            this.locatedCar = car;
            parentFerry.decNumOfCarsShouldUpload();
        } finally {
            lock.unlock();
        }
    }

    public void release() {
        lock.lock();
        try {
            locatedCar = null;
            available = true;
            parentFerry.decNumOfCarsShouldUnload();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public String toString() {
        return "ParkingSpace{" +
                "number=" + number +
                ", length=" + length +
                ", width=" + width +
                '}';
    }
}
