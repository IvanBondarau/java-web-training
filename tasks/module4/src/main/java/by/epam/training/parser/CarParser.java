package by.epam.training.parser;

import by.epam.training.builder.CarBuilder;
import by.epam.training.model.Car;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CarParser implements Parser<List<Car>> {

    @Override
    public List<Car> parse(String path) throws ParserException {
        List<Car> cars = new LinkedList<>();

        Map<String, String> params = new HashMap<>();
        CarBuilder carBuilder = new CarBuilder(params);


        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader reader =
                    factory.createXMLStreamReader(new BufferedReader(new FileReader(path)));

            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLStreamConstants.START_ELEMENT) {
                    if ("Car".equals(reader.getLocalName())) {
                        String number = reader.getAttributeValue(null, "number");
                        String weight = reader.getAttributeValue(null, "weight");
                        String length = reader.getAttributeValue(null, "length");
                        String width = reader.getAttributeValue(null, "width");

                        params.put("Number", number);
                        params.put("Weight", weight);
                        params.put("Length", length);
                        params.put("Width", width);

                        cars.add(carBuilder.build());
                    }
                }
            }

            return cars;

        } catch (FileNotFoundException | XMLStreamException e) {
            throw new ParserException(e);
        }
    }
}
