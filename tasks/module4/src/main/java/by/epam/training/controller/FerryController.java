package by.epam.training.controller;

import by.epam.training.model.Crossing;
import by.epam.training.model.Ferry;
import by.epam.training.parser.Parser;
import by.epam.training.parser.ParserException;
import org.apache.log4j.Logger;

public class FerryController {

    private static final Logger LOGGER = Logger.getLogger(FerryController.class);

    private Parser<Ferry> parser;

    public FerryController(Parser<Ferry> parser) {
        this.parser = parser;
    }

    public void loadFerryFromFile(String filePath) {
        try{
            Ferry ferry = parser.parse(filePath);
            LOGGER.info(ferry + " loaded from " + filePath);
            Crossing.getInstance().setFerry(ferry);
        } catch (ParserException e) {
            LOGGER.error(e.getMessage());
        }



    }
}
