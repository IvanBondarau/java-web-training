package by.epam.training.parser;

public interface Parser<T> {
    T parse(String path) throws ParserException;
}
