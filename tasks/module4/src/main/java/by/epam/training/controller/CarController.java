package by.epam.training.controller;

import by.epam.training.model.Car;
import by.epam.training.parser.CarParser;
import by.epam.training.parser.Parser;
import by.epam.training.parser.ParserException;
import org.apache.log4j.Logger;

import java.util.List;

public class CarController {

    private static final Logger LOGGER = Logger.getLogger(CarController.class);

    private Parser<List<Car>> carParser;

    public CarController(Parser<List<Car>> carParser) {
        this.carParser = carParser;
    }

    public void loadCarsFromFile(String filePath) {
        try {
            List<Car> cars = carParser.parse(filePath);
            for (Car car:cars) {
                car.start();
            }

        } catch (ParserException e) {
            LOGGER.error(e.getMessage());
        }
    }

}
