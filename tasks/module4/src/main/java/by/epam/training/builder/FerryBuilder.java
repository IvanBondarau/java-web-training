package by.epam.training.builder;

import by.epam.training.model.Ferry;
import by.epam.training.model.ParkingSpace;

import java.util.List;
import java.util.Map;

public class FerryBuilder implements Builder<Ferry> {

    private Map<String, String> params;
    private List<ParkingSpace> parkingSpaces;

    public FerryBuilder(Map<String, String> params) {
        this.params = params;
    }

    public void setParkingSpaces(List<ParkingSpace> parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
    }

    @Override
    public Ferry build() {

        if (parkingSpaces == null) {
            throw new IllegalStateException("Parking spaces list isn't set");
        }

        double maximumWeight = Double.parseDouble(params.get("MaximumWeight"));
        int runDuration = Integer.parseInt(params.get("RunDuration"));

        return new Ferry(maximumWeight, runDuration, parkingSpaces);



    }
}
