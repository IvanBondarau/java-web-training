package by.epam.training.builder;

import by.epam.training.model.Car;

import java.util.Map;

public class CarBuilder implements Builder<Car> {

    private Map<String, String> params;

    public CarBuilder(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public Car build() {
        String number = params.get("Number");
        double weight = Double.parseDouble(params.get("Weight"));
        double length = Double.parseDouble(params.get("Length"));
        double width = Double.parseDouble(params.get("Width"));

        return new Car(number, weight, length, width);
    }
}
