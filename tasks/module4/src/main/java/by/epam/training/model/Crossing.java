package by.epam.training.model;

import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Crossing  {

    private static final Logger LOGGER  = Logger.getLogger(Crossing.class);

    private static Crossing crossing;

    private Ferry ferry;

    private final static Lock crossingLock = new ReentrantLock();
    private final static Lock ferryLock = new ReentrantLock();
    private final static Lock queueLock = new ReentrantLock();

    private Queue<Car> queue;

    private Crossing() {
        queueLock.lock();
        queue = new LinkedList<>();
        queueLock.unlock();
    }

    public void setFerry(Ferry ferry) {
        ferryLock.lock();
        try {
            if (ferry == null) {
                throw new IllegalArgumentException("Ferry must be not null");
            }
            if (this.ferry == null) {
                this.ferry = ferry;
                ferry.start();
            } else {
                this.ferry.finish();
                ferry.join();
                this.ferry = ferry;
                ferry.start();
            }
        } catch (InterruptedException e) {
            throw new IllegalStateException("Interrupted");
        } finally {
            ferryLock.unlock();
        }
    }


    public void addCarToQueue(Car car) {
        queueLock.lock();
        try {
            queue.add(car);
            LOGGER.info("Added " + car + " to queue");
        } finally {
            queueLock.unlock();
        }
    }


    public void startFerry() {
        ferryLock.lock();
        if (ferry.getFerryState() != FerryState.UPLOADING) {
            throw new IllegalStateException("Ferry is not ready");
        }
        ferryLock.unlock();
        LOGGER.info("Ferry started");
        queueLock.lock();
        ferryLock.lock();
        try {

            Map<Car, ParkingSpace> uploadedCars = new HashMap<>();

            int queueSize = queue.size();

            for (int it = 0; it < queueSize; it++) {
                Car currentCar = queue.remove();
                Optional<ParkingSpace> parking = ferry.offerCar(currentCar);
                if (parking.isPresent()) {
                    uploadedCars.put(currentCar, parking.get());
                } else {
                    queue.add(currentCar);
                }
            }

            LOGGER.info("Uploading cars: " + uploadedCars.size());

            ferry.setNumOfUploadingCars(uploadedCars.size());
            for (Map.Entry<Car, ParkingSpace> pair: uploadedCars.entrySet()) {
                LOGGER.info("Allowed uploading of " + pair.getKey() + " on space " + pair.getValue().getNumber());
                pair.getKey().allowUploading(pair.getValue());
            }
            ferry.allowRunning();

            LOGGER.info("Ferry: running allowed");

        } finally {
            ferryLock.unlock();
            queueLock.unlock();
        }
    }

    public boolean isQueueEmpty() {
        return queue.isEmpty();
    }

    public void finishFerry() throws InterruptedException {
        ferry.finish();
        ferry.join();
    }


    public static Crossing getInstance() {
        if (crossing == null) {
            crossingLock.lock();
            if (crossing == null) {
                crossing = new Crossing();
            }
            crossingLock.unlock();
        }

        return crossing;
    }
}
