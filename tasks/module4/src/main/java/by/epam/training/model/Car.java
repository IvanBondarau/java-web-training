package by.epam.training.model;

import org.apache.log4j.Logger;

import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Car extends Thread {

    private static final Logger LOGGER = Logger.getLogger(Car.class);

    private String number;

    private double weight;
    private double length;
    private double width;

    private ParkingSpace parkingSpace;

    private CountDownLatch uploadingAllowed = new CountDownLatch(1);
    private CountDownLatch unloadingAllowed = new CountDownLatch(1);


    public Car(String number, double weight, double length, double width) {
        this.number = number;
        this.weight = weight;
        this.length = length;
        this.width = width;

        this.setDaemon(true);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void allowUploading(ParkingSpace space) {
        this.parkingSpace = space;
        uploadingAllowed.countDown();
        LOGGER.info(this + ": uploading on space " + space.getNumber());
    }

    public void allowUnloading() {
        unloadingAllowed.countDown();
    }

    @Override
    public void run() {


        Crossing.getInstance().addCarToQueue(this);

        try {
            uploadingAllowed.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        parkingSpace.allocate(this);

        LOGGER.info(this + " uploaded on parking space num " + parkingSpace.getNumber());


        try {
            unloadingAllowed.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        }

        parkingSpace.release();
        this.parkingSpace = null;

        LOGGER.info(this + " unloaded");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.weight, weight) == 0 &&
                Double.compare(car.length, length) == 0 &&
                Double.compare(car.width, width) == 0 &&
                number.equals(car.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, weight, length, width);
    }

    @Override
    public String toString() {
        return "Car{" +
                "number='" + number + '\'' +
                ", weight=" + weight +
                ", length=" + length +
                ", width=" + width +
                '}';
    }
}
