package by.epam.training.model;

public enum FerryState {
    UPLOADING,
    RUNNING,
    UNLOADING
}
