package by.epam.training.model;

import org.apache.log4j.Logger;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Ferry extends Thread {

    private static final Logger LOGGER = Logger.getLogger(Ferry.class);

    private Lock lock = new ReentrantLock();

    private CountDownLatch runningAllowed = new CountDownLatch(1);

    private boolean finished = false;

    private double maximumWeight;
    private double availableWeight;
    private int runDuration;
    private List<ParkingSpace> parkingSpaces;



    private FerryState state;
    private Lock stateLock = new ReentrantLock();

    private CountDownLatch numOfCarsShouldUpload;
    private CountDownLatch numOfCarsShouldUnload;


    public Ferry(double maximumWeight, int runDuration, List<ParkingSpace> parkingSpaces) {
        this.maximumWeight = maximumWeight;
        this.runDuration = runDuration;
        this.parkingSpaces = parkingSpaces;
        for (ParkingSpace parkingSpace: parkingSpaces) {
            parkingSpace.setFerry(this);
        }
        this.availableWeight = maximumWeight;
    }

    public int getRunDuration() {
        return runDuration;
    }

    public void allowRunning() {
        try {
            numOfCarsShouldUpload.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setFerryState(FerryState.RUNNING);
        runningAllowed.countDown();
    }


    public FerryState getFerryState() {
        stateLock.lock();
        try {
            return state;
        } finally {
            stateLock.unlock();
        }
    }

    public void finish() {
        if (finished) {
            return;
        }
        finished = true;
        if (state == FerryState.UPLOADING) {
            runningAllowed.countDown();
        }
    }

    public void setNumOfUploadingCars(int num) {
        numOfCarsShouldUpload = new CountDownLatch(num);
        setNumOfUnloadingCars(num);
    }

    public void decNumOfCarsShouldUpload() {
        numOfCarsShouldUpload.countDown();
    }

    public void setNumOfUnloadingCars(int num) {
        numOfCarsShouldUnload = new CountDownLatch(num);
    }

    public void decNumOfCarsShouldUnload() {
        numOfCarsShouldUnload.countDown();
    }

    public void setFerryState(FerryState state) {
        stateLock.lock();
        this.state = state;
        stateLock.unlock();
    }

    @Override
    public void run() {
        while (!finished) {

            setFerryState(FerryState.UPLOADING);
            LOGGER.info("Uploading");

            try {
                runningAllowed.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            runningAllowed = new CountDownLatch(1);

            if (finished) {
                LOGGER.info("Finishing");
                break;
            }

            LOGGER.info("Running");
            try {
                TimeUnit.MILLISECONDS.sleep(runDuration);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            setFerryState(FerryState.UNLOADING);
            LOGGER.info("Unloading");

            for (ParkingSpace space: parkingSpaces) {

                Optional<Car> car = space.getLocatedCar();
                car.ifPresent(value -> LOGGER.info("Allowed unloading " + value));
                car.ifPresent(Car::allowUnloading);
            }

            try {
                numOfCarsShouldUnload.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

            availableWeight = maximumWeight;

            setFerryState(FerryState.RUNNING);
            LOGGER.info("Returning");

            try {
                TimeUnit.MILLISECONDS.sleep(runDuration);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }


        }
    }

    public Optional<ParkingSpace> offerCar(Car car) {

        for (ParkingSpace space: parkingSpaces) {
            lock.lock();
            try {
                if (car.getWeight() > availableWeight) {
                    return Optional.empty();
                }

                boolean applyingResult = space.apply(car);

                if (applyingResult) {
                    availableWeight -= car.getWeight();
                    return Optional.of(space);
                }
            } finally {
                lock.unlock();
            }
        }
        return Optional.empty();
    }

    @Override
    public String toString() {
        return "Ferry{" +
                "maximumWeight=" + maximumWeight +
                ", parkingSpaces=" + parkingSpaces +
                '}';
    }
}
