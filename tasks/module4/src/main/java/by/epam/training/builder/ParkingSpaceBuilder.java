package by.epam.training.builder;

import by.epam.training.model.ParkingSpace;

import java.util.Map;

public class ParkingSpaceBuilder implements Builder<ParkingSpace> {

    private Map<String, String > params;

    public ParkingSpaceBuilder(Map<String, String > params) {
        this.params = params;
    }

    @Override
    public ParkingSpace build() {
        int number = Integer.parseInt(params.get("Number"));
        double length = Double.parseDouble(params.get("Length"));
        double width = Double.parseDouble(params.get("Width"));
        return new ParkingSpace(number, length, width);
    }
}
