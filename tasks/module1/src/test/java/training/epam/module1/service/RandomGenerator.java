package training.epam.module1.service;

import training.epam.module1.entity.ClassicTariff;
import training.epam.module1.entity.InternetOnlyTariff;
import training.epam.module1.entity.Tariff;
import training.epam.module1.entity.UnlimitedTariff;

import java.util.Optional;
import java.util.Random;

public class RandomGenerator {

    private long seed;
    private Random random;

    public RandomGenerator(long seed) {
        this.seed = seed;
        this.random = new Random(seed);
    }

    public long getSeed() {
        return seed;
    }

    private long getRandomLong(long minValue, long maxValue) {
        long rnd = Math.abs(random.nextLong());
        rnd = Long.MIN_VALUE == rnd ? minValue : rnd;

        return rnd % (maxValue - minValue + 1) + minValue;
    }

    private double getRandomDouble(double minValue, double maxValue) {
        return (random.nextDouble() * (maxValue - minValue) + minValue);
    }

    private Tariff getRandomTariff(double minPayment, double maxPayment, int minSubscribers, int maxSubscribers) {
        int rnd = Math.abs(random.nextInt());
        int type = rnd == Integer.MIN_VALUE ? 0 : rnd % 3;

        Tariff ans;

        switch (type) {
            case 0:
                ans = new ClassicTariff("Random",
                                        getRandomDouble(minPayment, maxPayment),
                                        getRandomLong(minSubscribers, maxSubscribers),
                                        random.nextInt(),
                                        random.nextInt()
                                        );
                break;
            case 1:
                ans = new InternetOnlyTariff("Random",
                        getRandomDouble(minPayment, maxPayment),
                        getRandomLong(minSubscribers, maxSubscribers),
                        random.nextInt()
                );
                break;
            case 2:
                ans = new UnlimitedTariff("Random",
                        getRandomDouble(minPayment, maxPayment),
                        getRandomLong(minSubscribers, maxSubscribers),
                        random.nextInt()
                );
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        return ans;
    }

    public Tariff[] getRandomTariffArray(int arrayLength,
                                         double minPayment, double maxPayment,
                                         int minSubscribers, int maxSubscribers) {
        Tariff[] tariffArray = new Tariff[arrayLength];

        for (int i = 0; i < tariffArray.length; i++) {
            tariffArray[i] = getRandomTariff(minPayment, maxPayment, minSubscribers, maxSubscribers);
            tariffArray[i].setName("Random " + (i + 1));
        }

        return tariffArray;
    }
};