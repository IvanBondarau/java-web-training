package training.epam.module1.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import training.epam.module1.entity.ClassicTariff;
import training.epam.module1.entity.Tariff;
import training.epam.module1.entity.TariffType;
import training.epam.module1.entity.UnlimitedTariff;
import training.epam.module1.repository.TariffSpecification;

@RunWith(JUnit4.class)
public class TariffSpecificationTest {

    @Test
    public void matchesShouldBeValid() {

        Tariff testTariff1 = new UnlimitedTariff("Check 1", 11.73, 120, 20);
        Tariff testTariff2 = new ClassicTariff("Check 2", 20.19, 70, 200, 1000);
        Tariff testTariff3 = new UnlimitedTariff("Check 3", 40, 500, 40);

        TariffSpecification spec = new TariffSpecification("Check",
                                                10, 30,
                                              1, 1000,
                                                            TariffType.UNLIMITED);

        Assert.assertTrue(spec.match(testTariff1));
        Assert.assertFalse(spec.match(testTariff2));
        Assert.assertFalse(spec.match(testTariff3));
    }
}
