package training.epam.module1.controller;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import training.epam.module1.entity.ClassicTariff;
import training.epam.module1.entity.Tariff;
import training.epam.module1.entity.UnlimitedTariff;
import training.epam.module1.parser.LineParser;
import training.epam.module1.reader.FileDataReader;
import training.epam.module1.repository.TariffRepository;
import training.epam.module1.service.TariffService;
import training.epam.module1.validator.ErrorCode;
import training.epam.module1.validator.FileValidator;
import training.epam.module1.validator.LineValidator;
import training.epam.module1.validator.ValidationResult;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RunWith(JUnit4.class)
public class LoadFileControllerTest {

    private Logger log = Logger.getLogger(LoadFileControllerTest.class);

    private FileValidator fileValidator;
    private FileDataReader fileReader;
    private LineValidator lineValidator;
    private TariffService tariffService;
    private LineParser lineParser;
    private LoadFileController loadFileController;

    @Before
    public void init() {
        fileValidator = new FileValidator();
        fileReader = new FileDataReader();
        lineValidator = new LineValidator();
        tariffService = new TariffService(new TariffRepository());
        lineParser = new LineParser();
        loadFileController = new LoadFileController();
    }

    @Test
    public void invalidUri() {

        String testPath = "/any??invalid!!//\\PATH";

        FileLoadingResult fileLoadingResult;

        try {
            fileLoadingResult = loadFileController.loadFile(testPath,
                                                          fileValidator,
                                                          fileReader,
                                                          lineValidator,
                                                          lineParser,
                                                          tariffService);
        } catch (IOException e) {
            Assert.fail();
            return;
        }

        Assert.assertFalse(fileLoadingResult.isSuccessful());
        Assert.assertFalse(fileLoadingResult.fileIsFound());
        ValidationResult loadingErrors = fileLoadingResult.getValidationResult(FileLoadingResult.LOADING_ERRORS).get();
        Assert.assertTrue(loadingErrors.getErrors().containsKey(ErrorCode.INVALID_PATH));

    }

    @Test
    public void fileDoesntExist() {
        String testPath = "nosuchfile.txt";

        FileLoadingResult fileLoadingResult;

        try {
            fileLoadingResult = loadFileController.loadFile(testPath,
                                                            fileValidator,
                                                            fileReader,
                                                            lineValidator,
                                                            lineParser,
                                                            tariffService);
        } catch (IOException e) {
            Assert.fail();
            return;
        }


        Assert.assertFalse(fileLoadingResult.isSuccessful());
        Assert.assertFalse(fileLoadingResult.fileIsFound());

        ValidationResult loadingErrors = fileLoadingResult.getValidationResult(FileLoadingResult.LOADING_ERRORS).get();
        Assert.assertTrue(loadingErrors.getErrors().containsKey(ErrorCode.FILE_DOESNT_EXIST));
    }

    @Test
    public void invalidFormatAll() {
        String testPath = this.getClass().getClassLoader().getResource("invalidFormatAll.txt").getPath();

        FileLoadingResult fileLoadingResult;

        try {
            fileLoadingResult = loadFileController.loadFile(testPath,
                                                fileValidator,
                                                fileReader,
                                                lineValidator,
                                                lineParser,
                                                tariffService);
        } catch (IOException e) {
            Assert.fail();
            return;
        }


        Assert.assertFalse(fileLoadingResult.isSuccessful());
        Assert.assertTrue(fileLoadingResult.fileIsFound());

        for (Map.Entry<Integer, ValidationResult> lineError: fileLoadingResult.getAll().entrySet()) {
            Assert.assertTrue(lineError.getValue().getErrors().containsKey(ErrorCode.INVALID_LINE_FORMAT));
        }
    }

    @Test
    public void invalidParamsLine2() {
        String testPath = this.getClass().getClassLoader().getResource("invalidParamsLine2.txt").getPath();

        FileLoadingResult fileLoadingResult;

        try {
            fileLoadingResult = loadFileController.loadFile(testPath,
                                                            fileValidator,
                                                            fileReader,
                                                            lineValidator,
                                                            lineParser,
                                                            tariffService);
        } catch (IOException e) {
            Assert.fail();
            return;
        }

        Assert.assertFalse(fileLoadingResult.isSuccessful());
        Assert.assertTrue(fileLoadingResult.fileIsFound());

        Assert.assertTrue(fileLoadingResult.getValidationResult(2).isPresent());
        ValidationResult loadingErrors = fileLoadingResult.getValidationResult(2).get();
        Assert.assertTrue(loadingErrors.getErrors().containsKey(ErrorCode.INVALID_LINE_FORMAT));

        Tariff testTariff1 = new ClassicTariff("Light 1", 12.73,20000,100,1000);
        Tariff testTariff3 = new UnlimitedTariff("Unlim 1", 17.73, 30000, 2);
        List<Tariff> response = tariffService.getAll();

        Assert.assertTrue(response.contains(testTariff1));
        Assert.assertTrue(response.contains(testTariff3));
    }

    @Test
    public void invalidUndefinedTariffType() {

        String testPath = this.getClass().getClassLoader().getResource("invalidUndefinedTariffType.txt").getPath();

        FileLoadingResult fileLoadingResult;

        try {
            fileLoadingResult = loadFileController.loadFile(testPath,
                                                            fileValidator,
                                                            fileReader,
                                                            lineValidator,
                                                            lineParser,
                                                            tariffService);
        } catch (IOException e) {
            Assert.fail();
            return;
        }

        Assert.assertFalse(fileLoadingResult.isSuccessful());
        Assert.assertTrue(fileLoadingResult.fileIsFound());

        for (Map.Entry<Integer, ValidationResult> lineError: fileLoadingResult.getAll().entrySet()) {
            Assert.assertTrue(lineError.getValue().getErrors().containsKey(ErrorCode.UNDEFINED_TARIFF_TYPE));
        }
    }

    @Test
    public void invalidFormat1Params3() {

        String testPath = this.getClass().getClassLoader().getResource("invalidFormat1Params3.txt").getPath();

        FileLoadingResult fileLoadingResult;

        try {
            fileLoadingResult = loadFileController.loadFile(testPath,
                                                            fileValidator,
                                                            fileReader,
                                                            lineValidator,
                                                            lineParser,
                                                            tariffService);
        } catch (IOException e) {
            Assert.fail();
            return;
        }

        Assert.assertFalse(fileLoadingResult.isSuccessful());
        Assert.assertTrue(fileLoadingResult.fileIsFound());

        Assert.assertTrue(fileLoadingResult.getValidationResult(1).isPresent());
        ValidationResult loadingErrors = fileLoadingResult.getValidationResult(1).get();
        Assert.assertTrue(loadingErrors.getErrors().containsKey(ErrorCode.INVALID_LINE_FORMAT));


        Assert.assertTrue(fileLoadingResult.getValidationResult(3).isPresent());
        loadingErrors = fileLoadingResult.getValidationResult(3).get();
        Assert.assertTrue(loadingErrors.getErrors().containsKey(ErrorCode.INVALID_LINE_PARAMS));
    }

    @Test
    public void allValid() {
        String testPath = this.getClass().getClassLoader().getResource("allValid.txt").getPath();

        FileLoadingResult fileLoadingResult;

        try {
            fileLoadingResult = loadFileController.loadFile(testPath,
                                                            fileValidator,
                                                            fileReader,
                                                            lineValidator,
                                                            lineParser,
                                                            tariffService);
        } catch (IOException e) {
            Assert.fail();
            return;
        }

        Assert.assertTrue(fileLoadingResult.isSuccessful());

        Tariff testTariff1 = new ClassicTariff("Light 1", 12.73,20000,100,1000);
        Tariff testTariff2 = new ClassicTariff("Light 2", 15.33, 20000, 200, 2000);
        Tariff testTariff3 = new UnlimitedTariff("Unlim 1", 17.73, 30000, 2);
        List<Tariff> response = tariffService.getAll();

        Assert.assertTrue(response.contains(testTariff1));
        Assert.assertTrue(response.contains(testTariff2));
        Assert.assertTrue(response.contains(testTariff3));
    }
}