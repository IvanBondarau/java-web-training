package training.epam.module1.service;

import org.apache.log4j.Logger;
import org.junit.*;
import training.epam.module1.entity.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import training.epam.module1.repository.Repository;
import training.epam.module1.repository.TariffRepository;
import training.epam.module1.repository.TariffSpecification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


@RunWith(JUnit4.class)
public class TariffServiceTest {

    private static final Logger LOGGER = Logger.getLogger(TariffServiceTest.class);

    private static final long SEED = 123456789;

    private static final RandomGenerator random = new RandomGenerator(SEED);

    private TariffService service;
    private Repository<Tariff> tariffRepository;

    @Before
    public void init() {
        tariffRepository = new TariffRepository();
        service = new TariffService(tariffRepository);
    }

    @Test
    public void getAllTariffsShouldBeValid() {

        Tariff[] arr = random.getRandomTariffArray(100, 1, 100, 1, 100000);

        for (Tariff tariff: arr) {
            service.addTariff(tariff);
        }

        Tariff[] response = service.getAll().toArray(new Tariff[arr.length]);

        Assert.assertArrayEquals(arr, response);
    }

    @Test
    public void filterShouldBeValid() {

        Tariff[] arr = random.getRandomTariffArray(100, 1, 100, 1, 100000);

        for (Tariff tariff: arr) {
            service.addTariff(tariff);
        }

        TariffSpecification spec = new TariffSpecification();

        spec.setName("1");
        spec.setPayment(1, 5000);
        spec.setSubscribers(1, 500000);
        spec.setTariffType(TariffType.CLASSIC);

        List<Tariff> ans = new ArrayList<>();

        for (Tariff tariff: arr) {
            if (spec.match(tariff)) {
                ans.add(tariff);
            }
        }

        Tariff[] correctAns = ans.toArray(new Tariff[0]);

        Tariff[] response = service.filterTariffs(spec).toArray(new Tariff[0]);

        Assert.assertArrayEquals(correctAns, response);
    }

    @Test
    public void sortShouldBeValid() {

        Tariff[] arr = random.getRandomTariffArray(100, 1, 100, 1, 100000);

        for (Tariff tariff: arr) {
            service.addTariff(tariff);
        }

        Comparator<Tariff> comparator = Comparator.comparingDouble(Tariff::getPayment);

        Tariff[] response = service.sortTariffs(comparator).toArray(new Tariff[0]);
        Arrays.sort(arr, comparator);

        Assert.assertArrayEquals(arr, response);

    }

    @Test
    public void countShouldBeValid() {

        Tariff[] arr = random.getRandomTariffArray(100, 1, 100, 1, 100000);

        for (Tariff tariff: arr) {
            service.addTariff(tariff);
        }

        TariffSpecification spec = new TariffSpecification();

        spec.setName("1");
        spec.setPayment(1, 5000);
        spec.setSubscribers(1, 500000);
        spec.setTariffType(TariffType.CLASSIC);

        int ans = 0;
        for (Tariff tariff: arr) {
            if (spec.match(tariff)) {
                ans++;
            }
        }

        int response = service.countTariffs(spec);

        Assert.assertEquals(ans, response);
    }

    @Test
    public void countSubscribers() {

        long subscribers = 0;

        Tariff[] arr = random.getRandomTariffArray(100, 1, 100, 1, 100000);

        for (Tariff tariff: arr) {
            subscribers += tariff.getSubscribers();
            service.addTariff(tariff);
        }

        Assert.assertEquals(subscribers, service.countSubscribers());
    }


}
