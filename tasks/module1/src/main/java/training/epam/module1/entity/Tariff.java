package training.epam.module1.entity;

import java.util.Objects;

public class Tariff {

    protected String name;
    protected double payment;
    protected long subscribers;
    protected TariffType type;

    public Tariff(String name, double payment, long subscribers, TariffType type) {
        this.name = name;
        this.payment = payment;
        this.subscribers = subscribers;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPayment() {
        return payment;
    }

    public void setPayment(double payment) {
        this.payment = payment;
    }

    public long getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(long clients) {
        this.subscribers = clients;
    }

    public TariffType getType() {
        return this.type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariff tariff = (Tariff) o;
        return Double.compare(tariff.getPayment(), getPayment()) == 0 &&
                getSubscribers() == tariff.getSubscribers() &&
                Objects.equals(getName(), tariff.getName()) &&
                getType() == tariff.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPayment(), getSubscribers(), getType());
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "name='" + name + '\'' +
                ", payment=" + payment +
                ", subscribers=" + subscribers +
                ", type=" + type +
                '}';
    }
}
