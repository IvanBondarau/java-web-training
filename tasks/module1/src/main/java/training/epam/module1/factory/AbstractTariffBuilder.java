package training.epam.module1.factory;

import training.epam.module1.entity.Tariff;

import java.util.Map;

public abstract class AbstractTariffBuilder {

    protected String name;
    protected long subs;
    protected double payment;

    public AbstractTariffBuilder(Map<String, String> params) {
        name = params.get("name");
        subs = Long.parseLong(params.get("subs"));
        payment = Double.parseDouble(params.get("payment"));
    }

    public abstract Tariff build();
}
