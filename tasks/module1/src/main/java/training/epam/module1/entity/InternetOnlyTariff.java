package training.epam.module1.entity;

import java.util.Objects;

public final class InternetOnlyTariff extends Tariff {

    private int trafficIncluded;

    public InternetOnlyTariff(String name, double payment, long subscribers, int trafficIncluded) {
        super(name, payment, subscribers, TariffType.INTERNET_ONLY);
        this.trafficIncluded = trafficIncluded;
    }

    public int getTrafficIncluded() {
        return trafficIncluded;
    }

    public void setTrafficIncluded(int trafficIncluded) {
        this.trafficIncluded = trafficIncluded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        InternetOnlyTariff that = (InternetOnlyTariff) o;
        return getTrafficIncluded() == that.getTrafficIncluded();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getTrafficIncluded());
    }

    @Override
    public String toString() {
        return "InternetOnlyTariff{" +
                "trafficIncluded=" + trafficIncluded +
                ", name='" + name + '\'' +
                ", payment=" + payment +
                ", subscribers=" + subscribers +
                ", type=" + type +
                '}';
    }
}
