package training.epam.module1.controller;

import training.epam.module1.validator.ValidationResult;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class FileLoadingResult {

    public static final Integer LOADING_ERRORS = -1;

    private Map<Integer, ValidationResult> resultMap;

    public FileLoadingResult() {
        resultMap = new HashMap<>();
    }

    public void addValidationResult(int line, ValidationResult validationResult) {
        if (resultMap.containsKey(line)) {
            resultMap.get(line).addValidationResult(validationResult);
        } else {
            resultMap.put(line, validationResult);
        }
    }

    public Map<Integer, ValidationResult> getAll() {
        return resultMap;
    }

    public Optional<ValidationResult> getValidationResult(int lineNum) {
        return resultMap.containsKey(lineNum) ? Optional.of(resultMap.get(lineNum)) : Optional.empty();
    }

    public boolean isSuccessful() {
        return resultMap.isEmpty();
    }

    public boolean fileIsFound() {
        return !getValidationResult(LOADING_ERRORS).isPresent();
    }



}
