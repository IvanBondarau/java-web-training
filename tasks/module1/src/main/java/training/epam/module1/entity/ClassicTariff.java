package training.epam.module1.entity;

import java.util.Objects;

public class ClassicTariff extends Tariff {

    private int minutesIncluded;
    private int trafficIncluded;

    public ClassicTariff(String name, double payment, long subscribers, int minutes, int traffic) {
        super(name, payment, subscribers, TariffType.CLASSIC);
        this.minutesIncluded = minutes;
        this.trafficIncluded = traffic;
    }

    public int getMinutesIncluded() {
        return minutesIncluded;
    }

    public void setMinutesIncluded(int minutesIncluded) {
        this.minutesIncluded = minutesIncluded;
    }

    public int getTrafficIncluded() {
        return trafficIncluded;
    }

    public void setTrafficIncluded(int trafficIncluded) {
        this.trafficIncluded = trafficIncluded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ClassicTariff that = (ClassicTariff) o;
        return getMinutesIncluded() == that.getMinutesIncluded() &&
                getTrafficIncluded() == that.getTrafficIncluded();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getMinutesIncluded(), getTrafficIncluded());
    }

    @Override
    public String toString() {
        return "ClassicTariff{" +
                "minutesIncluded=" + minutesIncluded +
                ", trafficIncluded=" + trafficIncluded +
                ", name='" + name + '\'' +
                ", payment=" + payment +
                ", subscribers=" + subscribers +
                ", type=" + type +
                '}';
    }
}
