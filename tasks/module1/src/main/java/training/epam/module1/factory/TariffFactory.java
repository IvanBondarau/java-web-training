package training.epam.module1.factory;

import training.epam.module1.entity.*;

import java.util.Map;
import java.util.Optional;

public class TariffFactory {

    private Map<String, String> params;

    public Tariff getTariff(Map<String, String> params) {

        String tariffTypeStr = params.get("type");
        Optional<TariffType> tariffTypeOptional = TariffType.getFromString(tariffTypeStr);
        TariffType tariffType = tariffTypeOptional.orElseThrow(IllegalArgumentException::new);

        AbstractTariffBuilder tariffBuilder;
        switch (tariffType) {
            case CLASSIC:
                tariffBuilder = new ClassicTariffBuilder(params);
                break;
            case UNLIMITED:
                tariffBuilder = new UnlimitedTariffBuilder(params);
                break;
            case INTERNET_ONLY:
                tariffBuilder = new InternetTariffBuilder(params);
                break;
            default:
                throw new IllegalArgumentException("Invalid tariff type");
        }

        return tariffBuilder.build();

    }
}
