package training.epam.module1.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationResult {

    private Map<ErrorCode, List<String>> errors;

    public ValidationResult() {
        errors = new HashMap<>();
    }

    public void addError(ErrorCode code, String message) {
        errors.computeIfAbsent(code, k -> new ArrayList<>());
        errors.get(code).add(message);
    }

    public void addValidationResult(ValidationResult validationResult) {
        for (ErrorCode errorCode: validationResult.getErrors().keySet()) {
            if (errors.containsKey(errorCode)) {
                errors.get(errorCode).addAll(validationResult.getErrors().get(errorCode));
            } else {
                errors.put(errorCode, validationResult.getErrors().get(errorCode));
            }
        }
    }

    public boolean isValid() {
        return errors.isEmpty();

    }

    public Map<ErrorCode, List<String>> getErrors() {
        return errors;
    }
}
