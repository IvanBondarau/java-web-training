package training.epam.module1.validator;

import java.util.Map;

public class ClassicTariffParamsValidator extends TariffParamsValidator {

    @Override
    public ValidationResult validate(Map<String, String> params) {
        ValidationResult validationResult =  super.validate(params);

        try {
            Integer.parseInt(params.get("minutes"));
        } catch (NumberFormatException e) {
            validationResult.addError(ErrorCode.INVALID_LINE_PARAMS, "minutes: " + e.getMessage());
        }

        try {
            Integer.parseInt(params.get("internet"));
        } catch (NumberFormatException e) {
            validationResult.addError(ErrorCode.INVALID_LINE_PARAMS, "internet: " + e.getMessage());
        }

        return validationResult;
    }
}
