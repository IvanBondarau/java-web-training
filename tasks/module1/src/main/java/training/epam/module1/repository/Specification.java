package training.epam.module1.repository;

public interface Specification<T> {
    public boolean match(T item);
}
