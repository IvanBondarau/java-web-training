package training.epam.module1.controller;


import org.apache.log4j.Logger;
import training.epam.module1.factory.TariffFactory;
import training.epam.module1.entity.Tariff;
import training.epam.module1.parser.LineParser;
import training.epam.module1.reader.FileDataReader;
import training.epam.module1.service.TariffService;
import training.epam.module1.validator.ErrorCode;
import training.epam.module1.validator.FileValidator;
import training.epam.module1.validator.LineValidator;
import training.epam.module1.validator.ValidationResult;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class LoadFileController {

    private static final Logger log = Logger.getLogger(LoadFileController.class);

    public FileLoadingResult loadFile(String pathToFile,
                                     FileValidator fileValidator,
                                     FileDataReader fileReader,
                                     LineValidator lineValidator,
                                     LineParser lineParser,
                                     TariffService tariffService)
            throws IOException {

        FileLoadingResult fileLoadingResult = new FileLoadingResult();

        ValidationResult fileValidationResult = fileValidator.validate(pathToFile);

        if (!fileValidationResult.isValid()) {
            fileLoadingResult.addValidationResult(FileLoadingResult.LOADING_ERRORS, fileValidationResult);

            return fileLoadingResult;
        }

        List<String> lines = fileReader.readAll(pathToFile);

        log.info("Reading " + pathToFile);

        for (int lineNum = 0; lineNum < lines.size(); lineNum++) {

            ValidationResult lineValidationResult = lineValidator.validate(lines.get(lineNum), lineParser);

            if (lineValidationResult.isValid()) {
                Map<String, String> params = lineParser.parse(lines.get(lineNum));

                TariffFactory factory = new TariffFactory();
                Tariff tariff = factory.getTariff(params);
                tariffService.addTariff(tariff);

            } else {
                log.warn("Error in line " + (lineNum + 1) + ":");
                for (Map.Entry<ErrorCode, List<String>> error: lineValidationResult.getErrors().entrySet()) {
                    log.warn(error.getKey() + " : " + error.getValue());
                }

                fileLoadingResult.addValidationResult(lineNum+1, lineValidationResult);
            }
        }

        return fileLoadingResult;
    }
}
