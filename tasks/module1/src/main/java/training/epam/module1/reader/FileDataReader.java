package training.epam.module1.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileDataReader {

    public List<String> readAll(String pathToFile)
        throws IOException {
        return Files.readAllLines(Paths.get(pathToFile));
    }
}
