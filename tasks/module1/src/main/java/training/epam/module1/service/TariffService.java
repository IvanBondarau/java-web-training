package training.epam.module1.service;

import training.epam.module1.entity.Tariff;
import training.epam.module1.repository.Repository;
import training.epam.module1.repository.TariffSpecification;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TariffService {

    private Repository<Tariff> tariffRepository;

    public TariffService(Repository<Tariff> tariffRepository) {
        this.tariffRepository = tariffRepository;
    }

    public void addTariff(Tariff tariff) {
        if (tariff != null) {
            tariffRepository.save(tariff);
        } else {
            throw new IllegalArgumentException("Tariff must be not null");
        }
    }

    public void removeTariff(Tariff tariff) {
        if (tariff != null) {
            tariffRepository.remove(tariff);
        } else {
            throw new IllegalArgumentException("Tariff must be not null");
        }
    }

    public List<Tariff> filterTariffs(TariffSpecification tariffSpecification) {
        return tariffRepository.filter(tariffSpecification);
    }

    public List<Tariff> sortTariffs(Comparator<Tariff> tariffComparator) {
        return tariffRepository.sort(tariffComparator);
    }

    public int countTariffs(TariffSpecification tariffSpecification) {
        return tariffRepository.count(tariffSpecification);
    }

    public List<Tariff> getAll() {
        return tariffRepository.getAll();
    }

    public long countSubscribers() {
        List<Tariff> tariffs = tariffRepository.getAll();

        long totalSubscribers = 0;
        for (int i = 0; i < tariffs.size(); i++) {
            Tariff tariff = tariffs.get(i);
            totalSubscribers += tariff.getSubscribers();
        }

        return totalSubscribers;
    }

    public double calculateTotalEarnings() {

        List<Tariff> tariffs = tariffRepository.getAll();

        double totalEarnings = 0.d;
        for (int i = 0; i < tariffs.size(); i++) {
            Tariff tariff = tariffs.get(i);
            totalEarnings += tariff.getPayment() * tariff.getSubscribers();
        }

        return totalEarnings;
    }

}
