package training.epam.module1.parser;

import java.util.HashMap;
import java.util.Map;

public class LineParser {

   public  Map<String, String> parse(String line) {
        String[] lineSplit = line.split(",");
        Map<String, String> params = new HashMap<>();

        for (String strParam:lineSplit) {
            String[] param = strParam.split(":");
            params.put(param[0],param[1]);
        }

        return params;
    }
}
