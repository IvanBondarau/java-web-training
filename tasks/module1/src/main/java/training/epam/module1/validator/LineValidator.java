package training.epam.module1.validator;

import org.apache.log4j.Logger;
import training.epam.module1.parser.LineParser;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LineValidator {
    private static final Logger log = Logger.getLogger(LineValidator.class);

    private final Pattern commonTariffPattern = Pattern.compile(
            "type:([a-z]+)(,[A-Za-z]+:[A-Za-z0-9 .]+)+");

    private final Pattern classicTariffPattern = Pattern.compile(
            "type:classic,name:[A-Za-z][A-Za-z0-9 ]*,subs:\\d+,payment:\\d+\\.\\d+,minutes:\\d+,internet:\\d+");

    private final Pattern internetTariffPattern = Pattern.compile(
            "type:internet,name:[A-Za-z][A-Za-z0-9 ]*,subs:\\d+,payment:\\d+\\.\\d+,internet:\\d+");

    private final Pattern unlimitedTariffPattern = Pattern.compile(
            "type:unlim,name:[A-Za-z][A-Za-z0-9 ]*,subs:\\d+,payment:\\d+.\\d+,maxSpeed:\\d+");

    public LineValidator() {

    }

    public ValidationResult validate(String line, LineParser lineParser) {
        ValidationResult validationResult = new ValidationResult();

        Matcher commonMatcher = commonTariffPattern.matcher(line);

        if (!commonMatcher.matches()) {
            validationResult.addError(ErrorCode.INVALID_LINE_FORMAT, "Doesn't match common line format");
            return validationResult;
        }

        Pattern specificPattern;
        TariffParamsValidator tariffParamsValidator;
        if (line.startsWith("type:classic")) {
            specificPattern = classicTariffPattern;
            tariffParamsValidator = new ClassicTariffParamsValidator();
        } else if (line.startsWith("type:internet")) {
            specificPattern = internetTariffPattern;
            tariffParamsValidator = new InternetTariffParamsValidator();
        } else if (line.startsWith("type:unlim")) {
            specificPattern = unlimitedTariffPattern;
            tariffParamsValidator = new UnlimitedTariffParamsValidator();
        } else {
            validationResult.addError(ErrorCode.UNDEFINED_TARIFF_TYPE, "Undefined tariff type");
            return validationResult;
        }

        Matcher specificMatcher = specificPattern.matcher(line);
        if (!specificMatcher.matches()) {
            validationResult.addError(ErrorCode.INVALID_LINE_FORMAT, "Doesn't match specific line format");
        }

        Map<String, String> params = lineParser.parse(line);

        validationResult.addValidationResult(tariffParamsValidator.validate(params));

        return validationResult;
    }
}
