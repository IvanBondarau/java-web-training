package training.epam.module1.validator;

public enum ErrorCode {
    INVALID_PATH,
    FILE_DOESNT_EXIST,
    NOT_A_FILE,
    FILE_NOT_READABLE,
    INVALID_LINE_FORMAT,
    INVALID_LINE_PARAMS,
    UNDEFINED_TARIFF_TYPE
}
