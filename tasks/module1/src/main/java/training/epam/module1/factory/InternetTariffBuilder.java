package training.epam.module1.factory;

import training.epam.module1.entity.InternetOnlyTariff;
import training.epam.module1.entity.Tariff;

import java.util.Map;

public final class InternetTariffBuilder extends AbstractTariffBuilder {

    private int internet;

    public InternetTariffBuilder(Map<String, String> params) {
        super(params);
        internet = Integer.parseInt(params.get("internet"));
    }

    @Override
    public Tariff build() {
        return new InternetOnlyTariff(name, payment, subs, internet);
    }
}
