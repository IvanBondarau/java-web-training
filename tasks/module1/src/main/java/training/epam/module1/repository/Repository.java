package training.epam.module1.repository;

import java.util.Comparator;
import java.util.List;

public interface Repository<T> {

    public void save(T item);
    public void remove(T item);

    public List<T> getAll();
    public List<T> sort(Comparator<T> comparator);
    public List<T> filter(Specification<T> specification);
    public int count(Specification<T> specification);
}
