package training.epam.module1.entity;

import java.util.Optional;
import java.util.regex.Pattern;

public enum TariffType {
    CLASSIC,
    INTERNET_ONLY,
    UNLIMITED;

    public static Optional<TariffType> getFromString(String str) {
        if (str.equals("classic")) {
            return Optional.of(CLASSIC);
        } else if (str.equals("internet")) {
            return Optional.of(INTERNET_ONLY);
        } else if (str.equals("unlim")) {
            return Optional.of(UNLIMITED);
        } else {
            return Optional.empty();
        }
    }
}
