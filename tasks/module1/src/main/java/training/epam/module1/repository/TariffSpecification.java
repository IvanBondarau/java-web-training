package training.epam.module1.repository;

import training.epam.module1.entity.Tariff;
import training.epam.module1.entity.TariffType;


public class TariffSpecification implements Specification<Tariff> {

    private String name;
    private double minPayment;
    private double maxPayment;
    private long minSubscribers;
    private long maxSubscribers;
    private TariffType tariffType;

    private boolean nameSet;
    private boolean paymentSet;
    private boolean subscribersSet;
    private boolean tariffTypeSet;

    public TariffSpecification() {
        name = null;
        minPayment = Double.NaN;
        maxPayment = Double.NaN;
        minSubscribers = Integer.MIN_VALUE;
        maxSubscribers = Integer.MAX_VALUE;

        nameSet = false;
        paymentSet = false;
        subscribersSet = false;
    }

    public TariffSpecification(String name,
                               double minPayment, double maxPayment,
                               long minSubscribers, long maxSubscribers,
                               TariffType tariffType) {
        this.name = name;
        this.minPayment = minPayment;
        this.maxPayment = maxPayment;
        this.minSubscribers = minSubscribers;
        this.maxSubscribers = maxSubscribers;
        this.tariffType = tariffType;

        nameSet = true;
        paymentSet = true;
        subscribersSet = true;
        tariffTypeSet = true;
    }

    public void setName(String name) {
        this.name = name;
        nameSet = true;
    }

    public void resetName() {
        name = null;
        nameSet = false;
    }

    public void setPayment(double minPayment, double maxPayment) {
        this.minPayment = minPayment;
        this.maxPayment = maxPayment;
        paymentSet = true;
    }

    public void resetPayment() {
        minPayment = Double.NaN;
        maxPayment = Double.NaN;
        paymentSet = false;
    }

    public void setSubscribers(long minSubscribers, long maxSubscribers) {
        this.minSubscribers = minSubscribers;
        this.maxSubscribers = maxSubscribers;
        subscribersSet = true;
    }

    public void resetSubscribers() {
        minSubscribers = Integer.MIN_VALUE;
        maxSubscribers = Integer.MAX_VALUE;
        subscribersSet = false;
    }

    public void setTariffType(TariffType tariffType) {
        this.tariffType = tariffType;
        tariffTypeSet = true;
    }

    public void resetTariffType() {
        this.tariffType = null;
        this.tariffTypeSet = false;
    }

    public boolean match(Tariff tariff) {
        boolean match = true;
        if (nameSet) {
            match = match && tariff.getName().contains(name);
        }
        if (paymentSet) {
            match = match && (tariff.getPayment() >= minPayment) && (tariff.getPayment() <= maxPayment);
        }
        if (subscribersSet) {
            match = match && (tariff.getSubscribers() >= minSubscribers)
                    && (tariff.getSubscribers() <= maxSubscribers);
        }

        if (tariffTypeSet) {
            match = match && tariff.getType() == tariffType;
        }

        return match;
    }
}
