package training.epam.module1.validator;

import java.util.Map;

public class TariffParamsValidator {

    public ValidationResult validate(Map<String, String> params) {

        ValidationResult validationResult = new ValidationResult();

        try {
            Double.parseDouble(params.get("payment"));
        } catch (NumberFormatException e) {
            validationResult.addError(ErrorCode.INVALID_LINE_PARAMS, "payment: " + e.getMessage());
        }

        try {
            Long.parseLong(params.get("subs"));
        } catch (NumberFormatException e) {
            validationResult.addError(ErrorCode.INVALID_LINE_PARAMS, "subs: " + e.getMessage());
        }

        return validationResult;

    }
}
