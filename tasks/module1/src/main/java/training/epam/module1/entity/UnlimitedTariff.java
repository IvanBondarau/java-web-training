package training.epam.module1.entity;

import java.util.Objects;

public class UnlimitedTariff extends Tariff {

    private double maximumSpeed;

    public UnlimitedTariff(String name, double payment, long subscribers, double maximumSpeed) {
        super(name, payment, subscribers, TariffType.UNLIMITED);
        this.maximumSpeed = maximumSpeed;
    }

    public double getMaximumSpeed() {
        return maximumSpeed;
    }

    public void setMaximumSpeed(double maximumSpeed) {
        this.maximumSpeed = maximumSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UnlimitedTariff that = (UnlimitedTariff) o;
        return Double.compare(that.getMaximumSpeed(), getMaximumSpeed()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getMaximumSpeed());
    }

    @Override
    public String toString() {
        return "UnlimitedTariff{" +
                "maximumSpeed=" + maximumSpeed +
                ", name='" + name + '\'' +
                ", payment=" + payment +
                ", subscribers=" + subscribers +
                ", type=" + type +
                '}';
    }
}
