package training.epam.module1.repository;

import training.epam.module1.entity.Tariff;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TariffRepository implements Repository<Tariff> {

    private List<Tariff> tariffs;

    public TariffRepository() {
        tariffs = new ArrayList<Tariff>();
    }

    public void save(Tariff tariff) {
        if (tariff != null) {
            tariffs.add(tariff);
        } else {
            throw new NullPointerException("TariffService::addTariff - tariff must be not null");
        }
    }

    public void remove(Tariff tariff) {
        tariffs.remove(tariff);
    }

    public List<Tariff> getAll() {
        return new ArrayList<>(tariffs);
    }


    public List<Tariff> filter(Specification<Tariff> tariffSpecification) {

        List<Tariff> resultTariffs = new ArrayList<>();

        for (Tariff tariff : tariffs) {
            if (tariffSpecification.match(tariff)) {
                resultTariffs.add(tariff);
            }
        }

        return resultTariffs;
    }

    public List<Tariff> sort(Comparator<Tariff> tariffComparator) {
        List<Tariff> tariffsCopy = new ArrayList<>(tariffs);

        tariffsCopy.sort(tariffComparator);

        return tariffsCopy;
    }

    public int count(Specification<Tariff> tariffSpecification) {

        int ans = 0;

        for (Tariff tariff : tariffs) {
            if (tariffSpecification.match(tariff)) {
                ans++;
            }
        }

        return ans;
    }

}
