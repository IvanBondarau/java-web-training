package training.epam.module1.factory;

import training.epam.module1.entity.Tariff;
import training.epam.module1.entity.UnlimitedTariff;

import java.util.Map;

public class UnlimitedTariffBuilder extends AbstractTariffBuilder {

    private int maxSpeed;

    public UnlimitedTariffBuilder(Map<String, String> params) {
        super(params);
        maxSpeed = Integer.parseInt(params.get("maxSpeed"));
    }

    public Tariff build() {
        return new UnlimitedTariff(name, payment, subs, maxSpeed);
    }
}
