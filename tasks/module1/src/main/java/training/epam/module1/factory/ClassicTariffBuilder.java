package training.epam.module1.factory;

import training.epam.module1.entity.ClassicTariff;
import training.epam.module1.entity.Tariff;

import java.util.Map;

public class ClassicTariffBuilder extends AbstractTariffBuilder {

    private int minutes;
    private int internet;

    public ClassicTariffBuilder(Map<String, String> params) {
        super(params);
        minutes = Integer.parseInt(params.get("minutes"));
        internet = Integer.parseInt(params.get("internet"));
    }


    @Override
    public Tariff build() {
        return new ClassicTariff(name, payment, subs, minutes, internet);
    }

}
