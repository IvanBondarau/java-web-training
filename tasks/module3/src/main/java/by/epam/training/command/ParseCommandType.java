package by.epam.training.command;

public enum ParseCommandType {
    PARSE_STAX,
    PARSE_DOM
}
