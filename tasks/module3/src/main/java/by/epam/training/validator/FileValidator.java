package by.epam.training.validator;

import org.apache.log4j.Logger;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidator {

    private Logger LOGGER = Logger.getLogger(FileValidator.class);

    private XmlValidator xmlValidator;

    public FileValidator(XmlValidator xmlValidator) {
        this.xmlValidator = xmlValidator;
    }

    public ValidationResult validate(String pathToFIle) {

        ValidationResult result = new ValidationResult();

        try {
            URI fileUri = new URI("file:///" + pathToFIle);
            Path filePath = Paths.get(fileUri.getPath());

            if (!pathToFIle.endsWith(".xml")) {
                result.addError(ErrorCode.INVALID_FILE_EXTENSION, "File" + filePath + " has invalid extension");
                return result;
            }

            if (!Files.exists(filePath)) {
                result.addError(ErrorCode.FILE_DOESNT_EXIST, "File" + filePath + " doesn't exist");
                return result;
            }

            if (!Files.isRegularFile(filePath)) {
                result.addError(ErrorCode.NOT_A_FILE, filePath + " is not a file");
                return result;
            }

            if (!Files.isReadable(filePath)) {
                result.addError(ErrorCode.FILE_NOT_READABLE, filePath + " not is not readable");
                return result;
            }

            File file = new File(pathToFIle);

            if (!xmlValidator.validateAgainstXsd(file)) {
                result.addError(ErrorCode.NOT_MATCH_SCHEME, "File" + filePath + "doesn't match scheme");
            }


        } catch (URISyntaxException e) {
            result.addError(ErrorCode.INVALID_PATH, e.getMessage());
            return result;
        }

        return result;
    }
}
