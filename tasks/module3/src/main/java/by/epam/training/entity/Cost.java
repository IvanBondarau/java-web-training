package by.epam.training.entity;

import java.util.List;
import java.util.Objects;

public class Cost {

    private double value;
    private String currency;
    private List<String> details;

    public Cost(double value, String currency, List<String> details) {
        this.value = value;
        this.currency = currency;
        this.details = details;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cost cost = (Cost) o;
        return Double.compare(cost.value, value) == 0 &&
                currency.equals(cost.currency) &&
                Objects.equals(details, cost.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, currency, details);
    }

    @Override
    public String toString() {
        return "Cost{" +
                "value=" + value +
                ", currency='" + currency + '\'' +
                ", details=" + details +
                '}';
    }
}
