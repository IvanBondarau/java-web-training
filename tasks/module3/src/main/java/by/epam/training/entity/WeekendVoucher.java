package by.epam.training.entity;

import java.util.List;

public class WeekendVoucher extends Voucher {

    public WeekendVoucher(String country, int duration, String transport, List<String> hotelCharacteristics, Cost cost) {
        super(VoucherType.WEEKEND, country, duration, transport, hotelCharacteristics, cost);
    }
}
