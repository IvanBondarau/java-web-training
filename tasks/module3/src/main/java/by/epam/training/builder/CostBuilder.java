package by.epam.training.builder;


import by.epam.training.entity.Cost;

import java.util.List;
import java.util.Map;

public class CostBuilder {

    private Map<String, List<String>> params;

    public CostBuilder(Map<String, List<String>> params) {
        this.params = params;
    }

    public Cost build() {

        double value = Double.parseDouble(params.get("CostValue").get(0));
        String currency = params.get("CostCurrency").get(0);
        List<String> details = params.get("CostDetails");

        return new Cost(value, currency, details);
    }
}
