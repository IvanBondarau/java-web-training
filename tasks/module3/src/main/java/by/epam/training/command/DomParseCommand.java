package by.epam.training.command;

import by.epam.training.entity.Voucher;
import by.epam.training.parser.DomParser;
import by.epam.training.parser.ParserException;

import java.util.List;

public class DomParseCommand implements ParseCommand {

    private DomParser parser;

    public DomParseCommand(DomParser parser) {
        this.parser = parser;
    }

    @Override
    public List<Voucher> parse(String path) throws CommandException {
        try {
            return parser.parse(path);
        } catch (ParserException e) {
            throw new CommandException(e);
        }
    }
}
