package by.epam.training.builder;

import by.epam.training.entity.Cost;
import by.epam.training.entity.TourVoucher;
import by.epam.training.entity.Voucher;

import java.util.List;
import java.util.Map;

public class TourVoucherBuilder extends VoucherBuilder {

    public TourVoucherBuilder(Map<String, List<String>> params) {
        super(params);
    }

    @Override
    public Voucher build() {
        buildBase();
        return new TourVoucher(country, duration, transport, hotelCharacteristics, cost);
    }
}
