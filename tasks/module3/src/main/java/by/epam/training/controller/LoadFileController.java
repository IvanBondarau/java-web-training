package by.epam.training.controller;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandProvider;
import by.epam.training.command.ParseCommand;
import by.epam.training.command.ParseCommandType;
import by.epam.training.entity.Voucher;
import by.epam.training.service.Service;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.ValidationResult;
import org.apache.log4j.Logger;

import java.util.List;


public class LoadFileController {

    private static final Logger LOGGER = Logger.getLogger(LoadFileController.class);

    private FileValidator fileValidator;
    private CommandProvider commandProvider;
    private Service<Voucher> voucherService;

    public LoadFileController(FileValidator fileValidator, CommandProvider commandProvider, Service<Voucher> voucherService) {
        this.fileValidator = fileValidator;
        this.commandProvider = commandProvider;
        this.voucherService = voucherService;
    }

    public String loadFile(String path, ParseCommandType parseCommandType) {

        ValidationResult validationResult = fileValidator.validate(path);

        if (!validationResult.isValid()) {

            LOGGER.error(validationResult.getErrors());
            return "File is not valid";
        }

        ParseCommand command = commandProvider.getCommandByType(parseCommandType);

        List<Voucher> voucherList;
        try {
            voucherList = command.parse(path);
        } catch (CommandException e) {

            LOGGER.error(e.getMessage());
            return "Error while parsing file";
        }

        for (Voucher voucher: voucherList) {
            voucherService.save(voucher);
        }

        return "OK";

    }
}
