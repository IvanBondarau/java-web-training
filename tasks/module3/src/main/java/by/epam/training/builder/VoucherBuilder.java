package by.epam.training.builder;

import by.epam.training.entity.Cost;
import by.epam.training.entity.Voucher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class VoucherBuilder {

    protected Map<String, List<String>> params;

    protected String country;
    protected int duration;
    protected String transport;
    protected List<String> hotelCharacteristics;
    protected Cost cost;


    public VoucherBuilder(Map<String, List<String>> params) {
        this.params = params;
    }

    protected void buildBase() {
        country = params.get("Country").get(0);
        duration = Integer.parseInt(params.get("Days").get(0));
        transport = params.get("Transport").get(0);
        hotelCharacteristics = params.get("HotelCharacteristics");
        CostBuilder costBuilder = new CostBuilder(params);
        cost = costBuilder.build();
    }

    public abstract Voucher build();
}
