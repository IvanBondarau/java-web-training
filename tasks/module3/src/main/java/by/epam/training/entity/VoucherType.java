package by.epam.training.entity;

public enum VoucherType {
    TOUR,
    VACATION,
    WEEKEND,
    PILGRIMAGE;

    public static VoucherType getFromString(String string) {
        if (string.equals("PilgrimageVoucher")) {
            return VoucherType.PILGRIMAGE;
        } else if (string.equals("TourVoucher")) {
            return VoucherType.TOUR;
        } if (string.equals("WeekendVoucher")) {
            return VoucherType.WEEKEND;
        } else if (string.equals("VacationVoucher")) {
            return VoucherType.VACATION;
        } else {
            throw new IllegalArgumentException("Undefined voucher type");
        }
    }
}
