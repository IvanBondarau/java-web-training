package by.epam.training.builder;

import by.epam.training.entity.Voucher;
import by.epam.training.entity.WeekendVoucher;

import java.util.List;
import java.util.Map;

public class WeekendVoucherBuilder extends VoucherBuilder {

    public WeekendVoucherBuilder(Map<String, List<String>> params) {
        super(params);
    }

    @Override
    public Voucher build() {
        buildBase();
        return new WeekendVoucher(country, duration, transport, hotelCharacteristics, cost);
    }
}
