package by.epam.training.parser;

import by.epam.training.entity.Voucher;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

public interface VoucherParser {
    List<Voucher> parse(String path) throws ParserException;
}
