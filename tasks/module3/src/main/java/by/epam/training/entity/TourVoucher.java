package by.epam.training.entity;

import java.util.List;

public class TourVoucher extends Voucher {

    public TourVoucher(String country, int duration, String transport, List<String> hotelCharacteristics, Cost cost) {
        super(VoucherType.TOUR, country, duration, transport, hotelCharacteristics, cost);
    }
}
