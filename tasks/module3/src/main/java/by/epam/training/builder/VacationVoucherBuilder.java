package by.epam.training.builder;

import by.epam.training.entity.VacationVoucher;
import by.epam.training.entity.Voucher;

import java.util.List;
import java.util.Map;

public class VacationVoucherBuilder extends VoucherBuilder {


    public VacationVoucherBuilder(Map<String, List<String>> params) {
        super(params);
    }

    @Override
    public Voucher build() {
        buildBase();
        return new VacationVoucher(country, duration, transport, hotelCharacteristics, cost);
    }
}
