package by.epam.training.parser;

import by.epam.training.builder.VoucherBuilder;
import by.epam.training.builder.VoucherBuilderFactory;
import by.epam.training.entity.Cost;
import by.epam.training.entity.Voucher;
import by.epam.training.entity.VoucherType;
import by.epam.training.entity.WeekendVoucher;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StaxParser implements VoucherParser {

    public List<Voucher> parse(String path) throws ParserException {
        List<Voucher> voucherList = null;

        String id = null;
        VoucherType type = null;
        List<String> costDetails = null;
        List<String> hotelCharacteristics = null;
        String tagContent = null;

        Map<String, List<String>> params = new HashMap<>();

        try {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader =
                factory.createXMLStreamReader(new BufferedReader(new FileReader(path)));

            while (reader.hasNext()) {
                int event = reader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        if ("TouristVouchers".equals(reader.getLocalName())) {
                            voucherList = new ArrayList<>();
                        } else if ("HotelCharacteristics".equals(reader.getLocalName())) {
                            hotelCharacteristics = new ArrayList<>();
                        } else if ("Details".equals(reader.getLocalName())) {
                            costDetails = new ArrayList<>();
                        } else if ("Cost".equals(reader.getLocalName())) {

                            String value = reader.getAttributeValue(0);
                            List<String> valueAsList = new ArrayList<>();
                            valueAsList.add(value);
                            params.put("CostValue", valueAsList);

                            String currency = reader.getAttributeValue(1);
                            List<String> currencyAsList = new ArrayList<>();
                            currencyAsList.add(currency);
                            params.put("CostCurrency", currencyAsList);
                        }

                        if (reader.getLocalName().endsWith("Voucher")) {
                            id = reader.getAttributeValue(0);
                            type = VoucherType.getFromString(reader.getLocalName());
                        }

                        break;
                    case XMLStreamConstants.CHARACTERS:
                        tagContent = reader.getText().trim();
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        if (reader.getLocalName().endsWith("Voucher")) {
                            VoucherBuilderFactory builderFactory = new VoucherBuilderFactory();
                            VoucherBuilder voucherBuilder = builderFactory.getBuilder(type, params);
                            Voucher voucher = voucherBuilder.build();
                            voucher.setId(id);
                            voucherList.add(voucher);
                            break;
                        }

                        switch (reader.getLocalName()) {
                            case "Country":
                            case "Transport":
                            case "Days":
                                String fieldName = reader.getLocalName();
                                List<String> fieldValue = new ArrayList<>();
                                fieldValue.add(tagContent);
                                params.put(fieldName, fieldValue);

                                break;
                            case "Detail":
                                costDetails.add(tagContent);
                                break;
                            case "Details":
                                params.put("CostDetails", costDetails);
                                break;
                            case "HotelCharacteristic":
                                hotelCharacteristics.add(tagContent);
                                break;
                            case "HotelCharacteristics":
                                params.put("HotelCharacteristics", hotelCharacteristics);
                                break;


                        }
                        break;
                    case XMLStreamConstants.START_DOCUMENT:
                        voucherList = new ArrayList<>();
                        break;
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            throw new ParserException(e.getMessage());
        }

        return voucherList;
    }
}