package by.epam.training.parser;

import by.epam.training.builder.VoucherBuilder;
import by.epam.training.builder.VoucherBuilderFactory;
import by.epam.training.entity.Cost;
import by.epam.training.entity.Voucher;
import by.epam.training.entity.VoucherType;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DomParser implements VoucherParser {

    private DocumentBuilderFactory factory;
    private VoucherBuilderFactory builderFactory;

    public DomParser() {
        factory = DocumentBuilderFactory.newInstance();
        builderFactory = new VoucherBuilderFactory();
    }

    public List<Voucher> parse(String path) throws ParserException {
        List<Voucher> loadedVouchers = new ArrayList<>();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(path);
            Node root = document.getDocumentElement();


            NodeList childrenList = root.getChildNodes();

            String id;
            Map<String, List<String>> params = new HashMap<>();

            for (int nodeNum = 0; nodeNum < childrenList.getLength(); nodeNum++) {
                Node item = childrenList.item(nodeNum);

                if (item.getNodeType() == Node.ELEMENT_NODE) {
                    id = item.getAttributes().item(0).getTextContent();

                    NodeList fields = item.getChildNodes();
                    for (int fieldNum = 0; fieldNum < fields.getLength(); fieldNum++) {
                        Node field = fields.item(fieldNum);

                        if (field.getNodeType() == Node.ELEMENT_NODE) {

                            String fieldName = field.getNodeName();
                            List<String> fieldValue = new ArrayList<>();

                            if (fieldName.equals("Country")
                                    || fieldName.equals("Transport")
                                    || fieldName.equals("Days")) {
                                fieldValue.add(field.getTextContent());
                                params.put(fieldName, fieldValue);

                            } else if (fieldName.equals("HotelCharacteristics")) {
                                NodeList characteristics = field.getChildNodes();
                                for (int charNum = 0; charNum < characteristics.getLength(); charNum++) {
                                    if (characteristics.item(charNum).getNodeType() == Node.ELEMENT_NODE) {
                                        fieldValue.add(characteristics.item(charNum).getTextContent());
                                    }
                                }
                                params.put(fieldName, fieldValue);

                            } else if (fieldName.equals("Cost")) {

                                NamedNodeMap attributes = field.getAttributes();

                                String value = attributes.getNamedItem("value").getTextContent();
                                List<String> valueAsList = new ArrayList<>();
                                valueAsList.add(value);
                                params.put("CostValue", valueAsList);


                                String currency = attributes.getNamedItem("currency").getTextContent();
                                List<String> currencyAsList = new ArrayList<>();
                                currencyAsList.add(currency);
                                params.put("CostCurrency", currencyAsList);

                                NodeList detailsNode = null;

                                for (int childNum = 0; childNum < field.getChildNodes().getLength(); childNum++) {
                                    if (field.getChildNodes().item(childNum).getNodeType() == Node.ELEMENT_NODE) {
                                        detailsNode = field.getChildNodes().item(childNum).getChildNodes();
                                    }
                                }

                                List<String> details = new ArrayList<>();
                                for (int i = 0; i < detailsNode.getLength(); i++) {
                                    if (detailsNode.item(i).getNodeType() == Node.ELEMENT_NODE) {
                                        details.add(detailsNode.item(i).getTextContent());
                                    }
                                }

                                params.put("CostDetails", details);

                            } else {
                                throw new IllegalArgumentException();
                            }
                        }
                    }

                    VoucherType type = VoucherType.getFromString(item.getNodeName());
                    VoucherBuilder voucherBuilder = builderFactory.getBuilder(type, params);
                    Voucher voucher = voucherBuilder.build();
                    voucher.setId(id);
                    loadedVouchers.add(voucher);
                }

            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            throw new ParserException(e.getMessage());
        }
        return loadedVouchers;
    }
}
