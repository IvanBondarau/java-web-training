package by.epam.training.validator;

public enum ErrorCode {
    INVALID_FILE_EXTENSION,
    INVALID_PATH,
    FILE_DOESNT_EXIST,
    NOT_A_FILE,
    FILE_NOT_READABLE,
    NOT_MATCH_SCHEME
}
