package by.epam.training.entity;

import java.util.List;

public class PilgrimageVoucher extends Voucher {

    public PilgrimageVoucher(String country, int duration, String transport, List<String> hotelCharacteristics, Cost cost) {
        super(VoucherType.PILGRIMAGE, country, duration, transport, hotelCharacteristics, cost);
    }
}
