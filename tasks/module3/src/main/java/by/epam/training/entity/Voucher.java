package by.epam.training.entity;

import java.util.List;
import java.util.Objects;

public class Voucher {

    private String id;
    private VoucherType type;
    private String country;
    private int duration;
    private String transport;
    private List<String> hotelCharacteristics;
    private Cost cost;

    protected Voucher(VoucherType type,
                      String country,
                      int duration,
                      String transport,
                      List<String> hotelCharacteristics,
                      Cost cost) {

        this.type = type;
        this.country = country;
        this.duration = duration;
        this.transport = transport;
        this.hotelCharacteristics = hotelCharacteristics;
        this.cost = cost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VoucherType getType() {
        return type;
    }

    public void setType(VoucherType type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public List<String> getHotelCharacteristics() {
        return hotelCharacteristics;
    }

    public void setHotelCharacteristics(List<String> hotelCharacteristics) {
        this.hotelCharacteristics = hotelCharacteristics;
    }

    public Cost getCost() {
        return cost;
    }

    public void setCost(Cost cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voucher voucher = (Voucher) o;
        return duration == voucher.duration &&
                Objects.equals(id, voucher.id) &&
                type == voucher.type &&
                Objects.equals(country, voucher.country) &&
                Objects.equals(transport, voucher.transport) &&
                Objects.equals(hotelCharacteristics, voucher.hotelCharacteristics) &&
                Objects.equals(cost, voucher.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, country, duration, transport, hotelCharacteristics, cost);
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", country='" + country + '\'' +
                ", duration=" + duration +
                ", transport='" + transport + '\'' +
                ", hotelCharacteristics=" + hotelCharacteristics +
                ", cost=" + cost +
                '}';
    }
}
