package by.epam.training.entity;

import java.util.List;

public class VacationVoucher extends Voucher {

    public VacationVoucher(String country, int duration, String transport, List<String> hotelCharacteristics, Cost cost) {
        super(VoucherType.VACATION, country, duration, transport, hotelCharacteristics, cost);
    }
}
