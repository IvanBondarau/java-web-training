package by.epam.training.command;

import by.epam.training.entity.Voucher;
import by.epam.training.parser.ParserException;
import by.epam.training.parser.StaxParser;

import java.util.List;

public class StaxParseCommand implements ParseCommand  {

    private StaxParser parser;

    public StaxParseCommand(StaxParser parser) {
        this.parser = parser;
    }

    @Override
    public List<Voucher> parse(String path) throws CommandException {
        try {
            return parser.parse(path);
        } catch (ParserException e) {
            throw new CommandException(e);
        }
    }
}
