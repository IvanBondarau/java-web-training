package by.epam.training.command;

import by.epam.training.entity.Voucher;
import by.epam.training.parser.ParserException;

import java.util.List;

public interface ParseCommand {
    List<Voucher> parse(String path) throws CommandException;
}
