package by.epam.training.repository;

import java.util.List;

public interface Repository<T> {
    String create(T item);
    T read(String id);
    void update(T item);
    void delete(String id);

    List<T> getAll();
}
