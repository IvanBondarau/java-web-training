package by.epam.training.builder;

import by.epam.training.entity.Cost;
import by.epam.training.entity.PilgrimageVoucher;
import by.epam.training.entity.TourVoucher;
import by.epam.training.entity.Voucher;

import java.util.List;
import java.util.Map;

public class PilgrimageVoucherBuilder extends VoucherBuilder {

    public PilgrimageVoucherBuilder(Map<String, List<String>> params) {
        super(params);
    }

    @Override
    public Voucher build() {
        buildBase();
        return new PilgrimageVoucher(country, duration, transport, hotelCharacteristics, cost);
    }
}
