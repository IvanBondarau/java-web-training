package by.epam.training.builder;

import by.epam.training.entity.Cost;
import by.epam.training.entity.VoucherType;

import java.util.Map;
import java.util.List;

public class VoucherBuilderFactory {

    public VoucherBuilder getBuilder(VoucherType type, Map<String, List<String>>params) {
        switch (type) {
            case TOUR:
                return new TourVoucherBuilder(params);

            case VACATION:
                return new VacationVoucherBuilder(params);

            case WEEKEND:
                return new WeekendVoucherBuilder(params);

            case PILGRIMAGE:
                return new PilgrimageVoucherBuilder(params);

            default:
                throw new IllegalArgumentException();
        }
    }
}
