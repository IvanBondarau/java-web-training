package by.epam.training.repository;

import by.epam.training.entity.Voucher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class VoucherRepository implements Repository<Voucher> {

    private static final AtomicLong idCounter = new AtomicLong(0);

    private Map<String, Voucher> items;

    public VoucherRepository() {
        items = new HashMap<>();
    }

    @Override
    public String create(Voucher item) {
        long idLong = idCounter.getAndAdd(1);
        String id = Long.toString(idLong);
        item.setId(id);
        items.put(id, item);
        return id;
    }

    @Override
    public Voucher read(String id) {
        if (items.containsKey(id)) {
            return items.get(id);
        } else {
            throw new IllegalArgumentException("Id is not found");
        }
    }

    @Override
    public void update(Voucher item) {
        String id = item.getId();
        if (items.containsKey(id)) {
            items.put(id, item);
        } else {
            throw new IllegalArgumentException("Item is not found");
        }
    }

    @Override
    public void delete(String id) {
        if (items.containsKey(id)) {
            items.remove(id);
        } else {
            throw new IllegalArgumentException("Id is not found");
        }
    }

    @Override
    public List<Voucher> getAll() {
        return new ArrayList<>(items.values());
    }
}
