package by.epam.training.service;

import by.epam.training.entity.Voucher;
import by.epam.training.repository.Repository;

import java.util.List;

public class VoucherService implements Service<Voucher> {

    private Repository<Voucher> voucherRepository;

    public VoucherService(Repository<Voucher> voucherRepository) {
        this.voucherRepository = voucherRepository;
    }

    @Override
    public void save(Voucher item) {
        voucherRepository.create(item);
    }

    @Override
    public List<Voucher> getAll() {
        return voucherRepository.getAll();
    }
}
