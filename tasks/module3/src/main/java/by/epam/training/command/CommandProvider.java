package by.epam.training.command;

import java.util.EnumMap;

public class CommandProvider {

    private EnumMap<ParseCommandType, ParseCommand> commands;

    public CommandProvider() {
        commands = new EnumMap<ParseCommandType, ParseCommand>(ParseCommandType.class);
    }

    public void registerCommand(ParseCommandType type, ParseCommand command) {
        commands.put(type, command);
    }

    public ParseCommand getCommandByType(ParseCommandType type) {
        return commands.get(type);
    }
}
