package by.epam.training.controller;

import by.epam.training.command.CommandProvider;
import by.epam.training.command.DomParseCommand;
import by.epam.training.command.ParseCommandType;
import by.epam.training.command.StaxParseCommand;
import by.epam.training.entity.Voucher;
import by.epam.training.parser.DomParser;
import by.epam.training.parser.ParserException;
import by.epam.training.parser.StaxParser;
import by.epam.training.parser.VoucherParser;
import by.epam.training.repository.VoucherRepository;
import by.epam.training.service.Service;
import by.epam.training.service.VoucherService;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.FileValidatorTest;
import by.epam.training.validator.XmlValidator;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class LoadFileControllerTest {

    private static final Logger LOGGER = Logger.getLogger(LoadFileControllerTest.class);

    private LoadFileController controller;
    private Service<Voucher> voucherService;

    @Before
    public void init() {
        File xmlScheme = new File(FileValidatorTest.class.getResource("/touristVouchers.xsd").getPath());
        XmlValidator xmlValidator = new XmlValidator(xmlScheme);
        FileValidator validator = new FileValidator(xmlValidator);
        VoucherRepository repository = new VoucherRepository();
        voucherService = new VoucherService(repository);

        CommandProvider commandProvider = new CommandProvider();

        StaxParser staxParser = new StaxParser();
        DomParser domParser = new DomParser();

        StaxParseCommand staxParseCommand = new StaxParseCommand(staxParser);
        DomParseCommand domParseCommand = new DomParseCommand(domParser);

        commandProvider.registerCommand(ParseCommandType.PARSE_STAX, staxParseCommand);
        commandProvider.registerCommand(ParseCommandType.PARSE_DOM, domParseCommand);

        controller = new LoadFileController(validator, commandProvider, voucherService);
    }

    @Test
    public void invalidPath() {

        String xml = "/StrangePath";

        String result = controller.loadFile(xml, ParseCommandType.PARSE_DOM);

        assertEquals("File is not valid", result);
    }

    @Test
    public void invalidFileExtension() {

        String xml =  FileValidatorTest.class.getResource("/invalidExtension.txt").getPath();

        String result = controller.loadFile(xml, ParseCommandType.PARSE_DOM);

        assertEquals("File is not valid", result);
    }

    @Test
    public void badFormed() {

        String xml = FileValidatorTest.class.getResource("/invalid.xml").getPath();

        String result = controller.loadFile(xml, ParseCommandType.PARSE_DOM);

        assertEquals("File is not valid", result);
    }

    @Test
    public void invalidXML() {

        String xml = FileValidatorTest.class.getResource("/invalid.xml").getPath();

        String result = controller.loadFile(xml, ParseCommandType.PARSE_DOM);

        assertEquals("File is not valid", result);
    }

    @Test
    public void staxParserAllValid() {

        String xml = FileValidatorTest.class.getResource("/allValid.xml").getPath();

        String result = controller.loadFile(xml, ParseCommandType.PARSE_STAX);

        assertEquals("OK", result);
        assertEquals(4, voucherService.getAll().size());
    }

    @Test
    public void domParserAllValid() {


        String xml = FileValidatorTest.class.getResource("/allValid.xml").getPath();

        String result = controller.loadFile(xml, ParseCommandType.PARSE_DOM);

        assertEquals("OK", result);
        assertEquals(4, voucherService.getAll().size());
    }


}