package by.epam.training.validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FileValidatorTest {

    FileValidator validator;

    @Before
    public void init() {
        File xmlScheme = new File(FileValidatorTest.class.getResource("/touristVouchers.xsd").getPath());
        XmlValidator xmlValidator = new XmlValidator(xmlScheme);
        validator = new FileValidator(xmlValidator);
    }



    @Test
    public void invalidPath() {
        String path = "/some/strange/path";
        ValidationResult validationResult = validator.validate(path);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void invalidExtension() {
        String path = FileValidatorTest.class.getResource("/invalidExtension.txt").getPath();
        ValidationResult validationResult = validator.validate(path);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void badFormed() {
        String path = FileValidatorTest.class.getResource("/badFormed.xml").getPath();
        ValidationResult validationResult = validator.validate(path);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void invalid() {
        String path = FileValidatorTest.class.getResource("/invalid.xml").getPath();
        ValidationResult validationResult = validator.validate(path);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void shouldBeValid() {
        String xml = FileValidatorTest.class.getResource("/allValid.xml").getPath();

        ValidationResult validationResult = validator.validate(xml);
        assertTrue(validationResult.isValid());
    }


}