package by.epam.training.parser;

import by.epam.training.entity.Voucher;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.FileValidatorTest;
import by.epam.training.validator.ValidationResult;
import by.epam.training.validator.XmlValidator;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class DomParserTest {

    private static final Logger LOGGER = Logger.getLogger(DomParserTest.class);

    private FileValidator validator;
    private DomParser parser;

    @Before
    public void init() {
        File xmlScheme = new File(FileValidatorTest.class.getResource("/touristVouchers.xsd").getPath());
        XmlValidator xmlValidator = new XmlValidator(xmlScheme);
        validator = new FileValidator(xmlValidator);
        parser = new DomParser();
    }


    @Test
    public void allValid() throws ParserException {
        String xml = FileValidatorTest.class.getResource("/allValid.xml").getPath();

        ValidationResult validationResult = validator.validate(xml);
        assertTrue(validationResult.isValid());

        List<Voucher> vouchers = parser.parse(xml);
        LOGGER.info(vouchers.size());

        for (Voucher voucher: vouchers) {
            LOGGER.info(voucher);
        }

        assertEquals(4 , vouchers.size());
    }

}