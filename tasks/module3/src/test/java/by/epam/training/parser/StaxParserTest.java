package by.epam.training.parser;

import by.epam.training.entity.Voucher;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.FileValidatorTest;
import by.epam.training.validator.ValidationResult;
import by.epam.training.validator.XmlValidator;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.Assert.*;

public class StaxParserTest {

    private static final Logger LOGGER = Logger.getLogger(StaxParser.class);

    private FileValidator validator;
    private StaxParser parser;

    @Before
    public void init() {
        File xmlScheme = new File(FileValidatorTest.class.getResource("/touristVouchers.xsd").getPath());
        XmlValidator xmlValidator = new XmlValidator(xmlScheme);
        validator = new FileValidator(xmlValidator);
        parser = new StaxParser();
    }

    @Test
    public void allValid() throws ParserException {
        String xml = FileValidatorTest.class.getResource("/allValid.xml").getPath();

        ValidationResult validationResult = validator.validate(xml);
        assertTrue(validationResult.isValid());

        List<Voucher> vouchers = parser.parse(xml);
        LOGGER.info(vouchers.size());

        for (Voucher voucher: vouchers) {
            LOGGER.info(voucher);
        }

        assertEquals(4 , vouchers.size());
    }

}