package by.epam.training.bet;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.TransactionManager;
import by.epam.training.entity.Bet;
import by.epam.training.entity.Lot;
import by.epam.training.entity.UserAccount;
import by.epam.training.lot.LotDAO;
import by.epam.training.user.UserAccountDAO;
import by.epam.training.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(JUnit4.class)
public class BetServiceImplTest {

    private BetService betService;

    @Mock
    private TransactionManager transactionManager;

    @Mock
    private BetDAO betDAO;

    @Mock
    private UserAccountDAO userAccountDAO;

    @Mock
    private LotDAO lotDAO;

    @Mock
    private UserService userService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        betService = new BetServiceImpl(transactionManager, betDAO, userAccountDAO, userService, lotDAO);
    }


    @Test
    public void getByLotId() throws DAOException, BetServiceException {

        betService.getByLotId(1L);

        Mockito.verify(betDAO).getByLotId(1L);

    }

    @Test
    public void placeBet() throws DAOException, BetServiceException {

        UserAccount userAccount = UserAccount.builder()
                                        .id(1L)
                                        .login("test")
                                        .password("password")
                                        .email("email")
                                        .userContactId(1L).build();

        Lot lot = Lot.builder().name("test lot").id(1L).build();

        BigDecimal value = new BigDecimal(100);

        Mockito.when(betDAO.create(any())).thenAnswer(
                (args) -> {
                    Bet bet = args.getArgument(0);
                    assertEquals(userAccount.getId(), bet.getUserAccountId());
                    assertEquals(lot.getId(), bet.getLotId());
                    assertEquals(value, bet.getValue());
                    bet.setId(1L);
                    return 1L;
                }
        );

        betService.placeBet(userAccount, lot, value);

    }

    @Test
    public void findLastBet() throws DAOException, BetServiceException {

        betService.findLastBet(2L, 1L);

        Mockito.verify(betDAO).findByUserIdAndLotId(2L, 1L);
    }

    @Test
    public void removeFromLotByUserId() throws DAOException, BetServiceException {

        betService.removeFromLotByUserId(1L, 2L);

        Mockito.verify(betDAO).deleteByUserIdAndLotId(2L, 1L);
    }


}