package by.epam.training.user;

import by.epam.training.dao.*;
import by.epam.training.entity.UserContact;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserContactDAOImplTest {

    private UserContactDAO userContactDAO;

    @Before
    public void init() {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionProvider connectionProvider = new ConnectionProviderImpl(transactionManager, connectionPool);

        userContactDAO = new UserContactDAOImpl(connectionProvider);
    }

    @Test
    public void createAndRead() throws DAOException {

        UserContact contact = new UserContact();
        contact.setContactEmail("123");
        contact.setAddress("132");
        contact.setPhone("213");
        contact.setFirstName("231");
        contact.setLastName("312");

        Long id = userContactDAO.create(contact);
        UserContact loaded = userContactDAO.read(id);

        assertEquals(contact, loaded);
    }
    @Test
    public void update() throws DAOException {

        UserContact contact = new UserContact();
        contact.setContactEmail("123");
        contact.setAddress("132");
        contact.setPhone("213");
        contact.setFirstName("231");
        contact.setLastName("312");

        Long id = userContactDAO.create(contact);

        contact.setContactEmail("0123");
        contact.setAddress("0132");
        contact.setPhone("0213");
        contact.setFirstName("0231");
        contact.setLastName("0312");

        userContactDAO.update(contact);

        UserContact loaded = userContactDAO.read(id);

        assertEquals(contact, loaded);
    }

    @Test(expected = ContactNotFoundException.class)
    public void delete() throws DAOException{

        UserContact contact = new UserContact();
        contact.setContactEmail("123");
        contact.setAddress("132");
        contact.setPhone("213");
        contact.setFirstName("231");
        contact.setLastName("312");

        Long id = userContactDAO.create(contact);
        UserContact loaded = userContactDAO.read(id);

        assertEquals(contact, loaded);

        userContactDAO.delete(id);
        userContactDAO.read(id);

    }
}