package by.epam.training.user;

import by.epam.training.dao.*;
import by.epam.training.entity.UserAccount;
import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class UserAccountDAOImplTest {

    private static final Logger LOGGER = Logger.getLogger(UserAccountDAOImplTest.class);

    private UserAccountDAO userAccountDAO;

    private UserAccount userAccount;

    @Before
    public void clear() throws SQLException {
        Connection connection = ConnectionPoolImpl.getInstance().getConnection();
        Statement statement = connection.createStatement();
        statement.execute("TRUNCATE table user_account;");
        connection.commit();
        ConnectionPoolImpl.getInstance().releaseConnection(connection);
    }

    @Before
    public void init() {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionProvider connectionProvider = new ConnectionProviderImpl(transactionManager, connectionPool);

        userAccountDAO = new UserAccountDAOImpl(connectionProvider);
    }

    @Test
    public void testCreateReadValid() throws DAOException {

        userAccount = new UserAccount("check", "check", "check", 0L);

        Long id = userAccountDAO.create(userAccount);

        UserAccount loaded = userAccountDAO.read(id);

        assertEquals(userAccount, loaded);
    }

    @Test
    public void testUpdateValid() throws DAOException {
        userAccount = new UserAccount("check", "check", "check", 0L);
        Long id = userAccountDAO.create(userAccount);

        userAccount.setLogin("1");
        userAccount.setEmail("2");
        userAccount.setPassword("3");

        userAccountDAO.update(userAccount);

        UserAccount loaded = userAccountDAO.read(id);

        assertEquals(userAccount, loaded);
    }

    @Test
    public void testDeleteValid() throws DAOException {
        UserAccount userAccount = new UserAccount("check", "check", "check", 0L);
        Long id = userAccountDAO.create(userAccount);

        userAccountDAO.delete(id);

        try {
            userAccountDAO.read(id);
        } catch (UserNotFoundException e) {
            return;
        }
    }

    @Test
    public void testIsLoginAndEmailAvailableValid() throws DAOException {
        String login = "13w2144fqjirfmcfoiarsmsc";
        String email = "fmuamjfzifjuqerojmoikrsosiv";

        assertTrue(userAccountDAO.isLoginAvailable(login));
        assertTrue(userAccountDAO.isEmailAvailable(email));

        userAccount = new UserAccount(login, email, "123", 0l);

        Long id = userAccountDAO.create(userAccount);

        assertFalse(userAccountDAO.isLoginAvailable(login));
        assertFalse(userAccountDAO.isEmailAvailable(email));

        userAccountDAO.delete(id);

        assertTrue(userAccountDAO.isLoginAvailable(login));
        assertTrue(userAccountDAO.isEmailAvailable(email));
    }

    @Test(expected = UserNotFoundException.class)
    public void readUserNotFound() throws DAOException {
        userAccountDAO.read(100000L);
    }

    @Test
    public void testGetByLoginPasswordValid() throws DAOException {
        String login = "13w2144fqjirfmcfoiarsmsc";
        String email = "fmuamjfzifjuqerojmoikrsosiv";
        String password = "1278133jednwcrmflkrflmlsfkk";

        userAccount = new UserAccount(login, email, password, 0L);
        userAccountDAO.create(userAccount);
        UserAccount loaded = userAccountDAO.getByLoginPassword(login, password);

        assertEquals(userAccount, loaded);
    }




}
