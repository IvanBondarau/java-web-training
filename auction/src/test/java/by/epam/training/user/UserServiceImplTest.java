package by.epam.training.user;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.TransactionManager;
import by.epam.training.entity.UserAccount;
import by.epam.training.entity.UserContact;
import by.epam.training.entity.UserRole;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(JUnit4.class)
public class UserServiceImplTest {

    private static final UserRole defaultUser = new UserRole(1L, "default_user");
    private static final UserRole admin = new UserRole(2L, "admin");
    @Mock
    private TransactionManager transactionManager;
    @Mock
    private UserContactDAO userContactDAO;
    @Mock
    private UserAccountDAO userAccountDAO;
    @Mock
    private UserRoleDAO userRoleDAO;
    private UserService userService;

    @Before
    public void init() throws DAOException {
        MockitoAnnotations.initMocks(this);

        userService = new UserServiceImpl(userAccountDAO, userContactDAO, userRoleDAO, transactionManager);

        Mockito.when(userRoleDAO.getByName("admin")).thenReturn(admin);
        Mockito.when(userRoleDAO.getByName("default_user")).thenReturn(defaultUser);

    }

    @Test
    public void signUpValid() throws DAOException, UserServiceException {

        String login = "login";
        String email = "email";
        String password = "password";

        Mockito.when(userAccountDAO.create(any())).thenAnswer(
                invocationOnMock -> {
                    UserAccount userAccount = invocationOnMock.getArgument(0);
                    assertEquals(login, userAccount.getLogin());
                    assertEquals(email, userAccount.getEmail());
                    assertEquals(password, userAccount.getPassword());
                    userAccount.setId(111L);
                    return 111L;
                }
        );

        UserDTO userDTO = userService.signUpUser(login, email, password);

        Mockito.verify(transactionManager).beginTransaction();
        Mockito.verify(transactionManager).commitTransaction();

        Mockito.verify(userContactDAO).create(any());

        assertEquals(111L, (long) userDTO.getUserAccount().getId());
        assertEquals(login, userDTO.getUserAccount().getLogin());
        assertEquals(email, userDTO.getUserAccount().getEmail());
        assertEquals(password, userDTO.getUserAccount().getPassword());
        assertNotNull(userDTO.getUserContact());
        assertEquals(1, userDTO.getRoles().size());
        assertTrue(userDTO.getRoles().contains(defaultUser));

    }

    @Test
    public void logInValid() throws DAOException, UserServiceException {
        String login = "login";
        String password = "password";

        Mockito.when(userAccountDAO.getByLoginPassword(any(), any())).thenAnswer(
                invocationOnMock -> {
                    String loginArg = invocationOnMock.getArgument(0);
                    String passwordArg = invocationOnMock.getArgument(1);
                    assertEquals(login, loginArg);
                    assertEquals(password, passwordArg);
                    return UserAccount.builder().login(loginArg).password(passwordArg).userContactId(1L).build();
                }
        );

        Optional<UserDTO> userDTO = userService.logInUser(login, password);

        Mockito.verify(userContactDAO).read(1L);

        assertTrue(userDTO.isPresent());

        UserAccount userAccount = userDTO.get().getUserAccount();
        assertEquals(login, userAccount.getLogin());
        assertEquals(password, userAccount.getPassword());

    }

    @Test
    public void updateUserContact() throws UserServiceException, DAOException {
        UserDTO userDTO = new UserDTO();
        UserContact contact = new UserContact();
        contact.setFirstName("Test");
        contact.setId(1L);
        userDTO.setUserContact(contact);

        userService.updateUserContact(userDTO);

        Mockito.verify(userContactDAO).update(contact);
    }

    @Test
    public void fillPurseValid() throws UserServiceException, DAOException {

        UserAccount userAccount = UserAccount.builder().purse(new BigDecimal(1000L)).build();

        userService.fillPurse(userAccount, new BigDecimal(100));

        BigDecimal expected = new BigDecimal(1100);

        Mockito.verify(userAccountDAO).update(any());

        assertEquals(expected, userAccount.getPurse());
    }

    @Test
    public void blockMoneyValid() throws UserServiceException, DAOException {

        UserAccount userAccount = UserAccount.builder()
                .purse(new BigDecimal(1000L))
                .frozenPurse(new BigDecimal(100))
                .build();

        userService.blockMoney(userAccount, new BigDecimal(100));

        BigDecimal expectedPurse = new BigDecimal(900);
        BigDecimal frozenPurse = new BigDecimal(200);

        Mockito.verify(userAccountDAO).update(any());

        assertEquals(expectedPurse, userAccount.getPurse());
        assertEquals(frozenPurse, userAccount.getFrozenPurse());
    }

    @Test
    public void unblockMoneyValid() throws UserServiceException, DAOException {

        UserAccount userAccount = UserAccount.builder()
                .purse(new BigDecimal(1000L))
                .frozenPurse(new BigDecimal(100))
                .build();

        userService.unlockMoney(userAccount, new BigDecimal(100));

        BigDecimal expectedPurse = new BigDecimal(1100);
        BigDecimal frozenPurse = new BigDecimal(0);

        Mockito.verify(userAccountDAO).update(any());

        assertEquals(expectedPurse, userAccount.getPurse());
        assertEquals(frozenPurse, userAccount.getFrozenPurse());
    }
}