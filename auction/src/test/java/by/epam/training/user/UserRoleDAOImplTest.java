package by.epam.training.user;

import by.epam.training.ApplicationConstant;
import by.epam.training.dao.*;
import by.epam.training.entity.UserRole;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class UserRoleDAOImplTest {

    private static final Logger LOGGER = Logger.getLogger(UserRoleDAOImplTest.class);
    private UserRoleDAO userRoleDAO;

    @Before
    public void init() {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionProvider connectionProvider = new ConnectionProviderImpl(transactionManager, connectionPool);

        userRoleDAO = new UserRoleDAOImpl(connectionProvider);
    }


}