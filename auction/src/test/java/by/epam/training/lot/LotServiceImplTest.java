package by.epam.training.lot;

import by.epam.training.bet.BetDAO;
import by.epam.training.bet.BetService;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.TransactionManager;
import by.epam.training.entity.Lot;
import by.epam.training.entity.LotStatus;
import by.epam.training.entity.UserAccount;
import by.epam.training.user.UserAccountDAO;
import by.epam.training.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

public class LotServiceImplTest {

    @Mock
    private LotDAO lotDAO;
    @Mock
    private LotStatusDAO lotStatusDAO;
    @Mock
    private BetDAO betDAO;
    @Mock
    private BetService betService;
    @Mock
    private UserAccountDAO userAccountDAO;
    @Mock
    private UserService userService;
    @Mock
    private TransactionManager transactionManager;

    private LotService lotService;

    @Before
    public void init() {

        MockitoAnnotations.initMocks(this);

        lotService = new LotServiceImpl(lotDAO, lotStatusDAO, betDAO, userAccountDAO,
                betService, userService, transactionManager);
    }

    @Test
    public void getLotById() throws DAOException, LotServiceException {
        Lot lot = Lot.builder().name("Test").build();

        Mockito.when(lotDAO.read(1L)).thenReturn(lot);

        LotDTO loaded = lotService.getLotById(1L);

        assertEquals(lot, loaded.getLot());
    }

    @Test
    public void createLotValid() throws LotServiceException, DAOException {

        Mockito.when(lotStatusDAO.getByName(any())).thenReturn(new LotStatus(1L, "Test status"));

        UserAccount userAccount = UserAccount.builder().id(10L).build();

        String name = "Test";
        String description = "Description";

        Timestamp start = new Timestamp(0);
        Timestamp end = new Timestamp(10);

        BigDecimal startingPrice = new BigDecimal(100);
        BigDecimal step = new BigDecimal(10);

        LotDTO lotDTO = lotService.createLot(userAccount, name, description, start, end, startingPrice, step);

        Mockito.verify(lotDAO).create(any());

        assertEquals(10L, (long) lotDTO.getLot().getOwnerId());
        assertEquals(name, lotDTO.getLot().getName());
        assertEquals(description, lotDTO.getLot().getDescription());
        assertEquals(start, lotDTO.getLot().getStart());
        assertEquals(end, lotDTO.getLot().getEnd());
        assertEquals(startingPrice, lotDTO.getLot().getStartingPrice());
        assertEquals(step, lotDTO.getLot().getStep());
    }

    @Test(expected = LotServiceException.class)
    public void getLotByIdDAOException() throws DAOException, LotServiceException {
        Mockito.when(lotDAO.read(any())).thenThrow(DAOException.class);

        lotService.getLotById(1L);
    }
}