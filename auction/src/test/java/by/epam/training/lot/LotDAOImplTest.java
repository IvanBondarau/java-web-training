package by.epam.training.lot;

import by.epam.training.dao.*;
import by.epam.training.entity.Lot;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class LotDAOImplTest {


    private static final Logger LOGGER = Logger.getLogger(LotDAOImplTest.class);

    private LotDAO lotDAO;

    @Before
    public void clear() throws SQLException {
        Connection connection = ConnectionPoolImpl.getInstance().getConnection();
        Statement statement = connection.createStatement();
        statement.execute("TRUNCATE table lot;");
        connection.commit();
        ConnectionPoolImpl.getInstance().releaseConnection(connection);

    }

    @Before
    public void init() {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionProvider connectionProvider = new ConnectionProviderImpl(transactionManager, connectionPool);

        lotDAO = new LotDAOImpl(connectionProvider);
    }

    @Test
    public void testCreateAndReadValid() throws DAOException {

        Lot lot = Lot.builder()
                .name("Test lot")
                .startingPrice(new BigDecimal(0))
                .step(new BigDecimal(0))
                .start(new Timestamp(1))
                .end(new Timestamp(2))
                .ownerId(1L)
                .statusId(1L)
                .build();

        Long id = lotDAO.create(lot);
        Lot loaded = lotDAO.read(id);

        assertEquals(lot, loaded);
    }

    @Test
    public void testUpdateValid() throws DAOException {

        Lot lot = Lot.builder()
                .name("Test lot")
                .startingPrice(new BigDecimal(0))
                .step(new BigDecimal(0))
                .start(new Timestamp(1))
                .end(new Timestamp(2))
                .ownerId(1L)
                .statusId(1L)
                .build();
        Long id = lotDAO.create(lot);
        lot.setId(id);
        lot.setName("New Name");
        lot.setCurrentPrice(new BigDecimal(3000));
        lot.setDescription("Abacaba");

        lotDAO.update(lot);

        Lot loaded = lotDAO.read(id);

        assertEquals(lot, loaded);
    }

    @Test(expected = LotNotFoundException.class)
    public void testDeleteValid() throws DAOException {

        Lot lot = Lot.builder()
                .name("Test lot")
                .startingPrice(new BigDecimal(0))
                .step(new BigDecimal(0))
                .start(new Timestamp(1))
                .end(new Timestamp(2))
                .ownerId(1L)
                .statusId(1L)
                .build();

        Long id = lotDAO.create(lot);
        lotDAO.delete(id);

        Lot loaded = lotDAO.read(id);

    }

}