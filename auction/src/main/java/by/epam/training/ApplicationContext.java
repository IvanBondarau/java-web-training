package by.epam.training;

import by.epam.training.bet.BetDAO;
import by.epam.training.bet.BetDAOImpl;
import by.epam.training.bet.BetService;
import by.epam.training.bet.BetServiceImpl;
import by.epam.training.command.*;
import by.epam.training.dao.*;
import by.epam.training.lot.*;
import by.epam.training.security.CryptographyManager;
import by.epam.training.security.CryptographyManagerImpl;
import by.epam.training.user.*;
import by.epam.training.validation.RequestValidator;
import by.epam.training.validation.SignUpDataValidator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class ApplicationContext {

    private static ApplicationContext instance = null;
    private static final Lock instanceLock = new ReentrantLock();

    private ConnectionPool connectionPool;
    private CommandProvider commandProvider;
    private ScheduledExecutorService scheduledExecutorService;

    private ApplicationContext() {
        //ConnectionPool
        connectionPool = ConnectionPoolImpl.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionProvider connectionProvider = new ConnectionProviderImpl(transactionManager, connectionPool);

        //DAO
        UserAccountDAO userAccountDAO = new UserAccountDAOImpl(connectionProvider);
        UserRoleDAO userRoleDAO = new UserRoleDAOImpl(connectionProvider);
        UserContactDAO userContactDAO = new UserContactDAOImpl(connectionProvider);
        LotDAO lotDAO = new LotDAOImpl(connectionProvider);
        LotStatusDAO lotStatusDAO = new LotStatusDAOImpl(connectionProvider);
        BetDAO betDAO = new BetDAOImpl(connectionProvider);

        //Service
        UserServiceImpl userService = new UserServiceImpl(userAccountDAO, userContactDAO, userRoleDAO, transactionManager);
        BetService betService = new BetServiceImpl(transactionManager, betDAO, userAccountDAO, userService, lotDAO);
        LotService lotService = new LotServiceImpl(lotDAO, lotStatusDAO, betDAO, userAccountDAO, betService, userService, transactionManager);
        userService.setLotService(lotService);

        //Validator
        RequestValidator signUpDataValidator = new SignUpDataValidator(userService);

        //Security
        CryptographyManager cryptographyManager = new CryptographyManagerImpl();

        //Lot updater
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(new LotUpdater(lotService), 0, 10, TimeUnit.SECONDS);

        //Command
        ServletCommand forwardLogIn = new ForwardLogInPageCommand();
        ServletCommand forwardSignUp = new ForwardSignUpCommand();
        ServletCommand forwardUserPage = new ForwardUserPageCommand();
        ServletCommand signUp = new SignUpCommand(userService, signUpDataValidator, cryptographyManager);
        ServletCommand logIn = new LogInCommand(userService, cryptographyManager);
        ServletCommand updateUserContact = new UpdateUserContactCommand(userService);
        ServletCommand logOut = new LogOutCommand();
        ServletCommand forwardUserList = new ForwardUserListCommand(userService);
        ServletCommand removeUserById = new RemoveUserCommand(userService);
        ServletCommand promoteToAdmin = new PromoteToAdminCommand(userService);
        ServletCommand setLanguageCommand = new SetLanguageCommand();
        ServletCommand forwardCreateLotPage = new ForwardCommand("createLotPage");
        ServletCommand forwardHomePage = new ForwardCommand("homePage");
        ServletCommand forwardLotListPage = new ForwardLotListCommand(lotService);
        ServletCommand createLot = new CreateLotCommand(lotService);
        ServletCommand forwardLotPage = new ForwardLotPageCommand(lotService, betService);
        ServletCommand placeBetCommand = new PlaceBetCommand(betService, lotService);
        ServletCommand fillPurseCommand = new FillPurseCommand(userService);
        ServletCommand removeLot = new RemoveLotCommand(lotService);

        commandProvider = new CommandProviderImpl();
        commandProvider.registerCommand(ApplicationConstant.FORWARD_USER_PAGE, forwardUserPage);
        commandProvider.registerCommand(ApplicationConstant.LOG_OUT_COMMAND_NAME, logOut);
        commandProvider.registerCommand(ApplicationConstant.UPDATE_USER_CONTACT, updateUserContact);
        commandProvider.registerCommand(ApplicationConstant.FORWARD_LOG_IN_FORM, forwardLogIn);
        commandProvider.registerCommand(ApplicationConstant.FORWARD_SIGN_UP_FORM, forwardSignUp);
        commandProvider.registerCommand(ApplicationConstant.SIGN_UP_COMMAND_NAME, signUp);
        commandProvider.registerCommand(ApplicationConstant.LOG_IN_COMMAND_NAME, logIn);
        commandProvider.registerCommand(ApplicationConstant.FORWARD_USER_LIST, forwardUserList);
        commandProvider.registerCommand(ApplicationConstant.REMOVE_USER_BY_ID, removeUserById);
        commandProvider.registerCommand(ApplicationConstant.PROMOTE_TO_ADMIN, promoteToAdmin);
        commandProvider.registerCommand(ApplicationConstant.SET_LANGUAGE_COMMAND, setLanguageCommand);
        commandProvider.registerCommand(ApplicationConstant.FORWARD_CREATE_LOT_PAGE, forwardCreateLotPage);
        commandProvider.registerCommand(ApplicationConstant.FORWARD_LOT_LIST, forwardLotListPage);
        commandProvider.registerCommand(ApplicationConstant.CREATE_LOT, createLot);
        commandProvider.registerCommand(ApplicationConstant.FORWARD_LOT_PAGE, forwardLotPage);
        commandProvider.registerCommand(ApplicationConstant.PLACE_BET_COMMAND, placeBetCommand);
        commandProvider.registerCommand(ApplicationConstant.FILL_PURSE, fillPurseCommand);
        commandProvider.registerCommand(ApplicationConstant.FORWARD_HOME_PAGE, forwardHomePage);
        commandProvider.registerCommand(ApplicationConstant.REMOVE_LOT, removeLot);
    }

    public CommandProvider getCommandProvider() {
        return commandProvider;
    }

    public void destroy() {
        scheduledExecutorService.shutdown();
        connectionPool.close();

    }

    public static ApplicationContext getInstance() {
        if (instance == null) {
            instanceLock.lock();
            if (instance == null) {
                instance = new ApplicationContext();
            }
            instanceLock.unlock();
        }
        return instance;
    }
}
