package by.epam.training.entity;

import by.epam.training.dao.IdentifiedEntity;
import lombok.*;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Lot implements IdentifiedEntity {

    private Long id;
    private Long ownerId;
    private Long statusId;
    private BigDecimal startingPrice;
    private BigDecimal currentPrice;
    private BigDecimal step;
    private Timestamp start;
    private Timestamp end;
    private String name;
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
