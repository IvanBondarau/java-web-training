package by.epam.training.entity;

import by.epam.training.dao.IdentifiedEntity;
import lombok.*;

import java.math.BigDecimal;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Bet implements IdentifiedEntity {

    private Long id;
    private Long userAccountId;
    private Long lotId;
    private Long lotOwnerId;
    private Timestamp date;
    private BigDecimal value;
}
