package by.epam.training.entity;


import by.epam.training.dao.IdentifiedEntity;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
public class UserAccount implements IdentifiedEntity {

    private Long id;
    private String login;
    private String email;
    private String password;
    private BigDecimal purse;
    private BigDecimal frozenPurse;
    private Long userContactId;

    public UserAccount(String login, String email, String password, Long userContactId) {
        this.login = login;
        this.email = email;
        this.password = password;

        this.purse = new BigDecimal(0);
        this.frozenPurse = new BigDecimal(0);

        id = 0L;
        this.userContactId = userContactId;
    }

}


