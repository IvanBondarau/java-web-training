package by.epam.training.entity;

import by.epam.training.dao.IdentifiedEntity;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class LotStatus implements IdentifiedEntity {
    Long id;
    String name;
}
