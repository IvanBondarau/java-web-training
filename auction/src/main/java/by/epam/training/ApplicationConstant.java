package by.epam.training;

import by.epam.training.entity.UserRole;

import java.math.MathContext;

public class ApplicationConstant {

    public static final String SIGN_UP_COMMAND_NAME = "signUpUser";
    public static final String LOG_IN_COMMAND_NAME = "logInUser";
    public static final String LOG_OUT_COMMAND_NAME = "logOutUser";
    public static final String FORWARD_SIGN_UP_FORM = "forwardSignUpForm";
    public static final String UPDATE_USER_CONTACT = "updateUserContact";
    public static final String FORWARD_LOG_IN_FORM = "forwardLogInForm";
    public static final String FORWARD_USER_PAGE = "forwardUserPage";
    public static final String FORWARD_USER_LIST = "forwardUserList";
    public static final String REMOVE_USER_BY_ID = "removeUserById";
    public static final String PROMOTE_TO_ADMIN = "promoteToAdmin";
    public static final String SET_LANGUAGE_COMMAND = "setLanguageCommand";
    public static final String FORWARD_CREATE_LOT_PAGE = "forwardCreateLotPage";
    public static final String FORWARD_LOT_LIST = "forwardLotList";
    public static final String FORWARD_LOT_PAGE = "forwardLotPage";
    public static final String CREATE_LOT = "createLot";
    public static final String PLACE_BET_COMMAND = "placeBet";
    public static final String FILL_PURSE = "fillPurse";
    public static final String FORWARD_HOME_PAGE = "forwardHomePage";
    public static final String REMOVE_LOT = "removeLot";

    public static final String ANONYMOUS_USER_ROLE_NAME = "anonymous";
    public static final String DEFAULT_USER_ROLE_NAME = "default_user";
    public static final String ADMIN_USER_ROLE_NAME = "admin";


    public static final String NOT_STARTED_STATUS = "not_started";
    public static final String IN_PROGRESS_STATUS = "in_progress";
    public static final String OVER_STATUS = "finished";


    public static final MathContext PRECISION_MATH_CONTEXT = new MathContext(5);
}