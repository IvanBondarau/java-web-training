package by.epam.training.validation;

import by.epam.training.lot.LotDTO;
import by.epam.training.user.UserDTO;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class PlaceBetDataValidator implements RequestValidator {

    private static final Pattern NUMBER_FORMAT_PATTERN = Pattern.compile("\\d{1,9}([.]\\d{1,9})?");

    @Override
    public ValidationResult validate(HttpServletRequest req) {

        ValidationResult validationResult = new ValidationResult();

        UserDTO userDTO = (UserDTO) req.getSession().getAttribute("user");
        LotDTO lotDTO = (LotDTO) req.getSession().getAttribute("lot");
        String valueStr = req.getParameter("value");

        if (userDTO == null) {
            throw new ValidatorException("User is not logged in");
        }

        if (lotDTO == null) {
            throw new ValidatorException("Lot doesn't found");
        }

        if (valueStr == null || valueStr.equals("")) {
            validationResult.addError(new ValidationError("bet.value", "empty"));
            return validationResult;
        }

        if (!NUMBER_FORMAT_PATTERN.matcher(valueStr).matches()) {
            validationResult.addError(new ValidationError("bet.value", "not_valid"));
            return validationResult;
        }

        BigDecimal value = new BigDecimal(valueStr);
        BigDecimal currentPrice = lotDTO.getLot().getCurrentPrice();
        BigDecimal startingPrice = lotDTO.getLot().getStartingPrice();
        BigDecimal step = lotDTO.getLot().getStep();

        if (currentPrice != null) {
            if (value.compareTo(currentPrice.add(step)) < 0) {
                validationResult.addError(new ValidationError("bet.value", "less_than_previous"));
                return validationResult;
            }

            BigDecimal diff = value.subtract(currentPrice);
            if (userDTO.getUserAccount().getPurse().compareTo(diff) < 0) {
                validationResult.addError(new ValidationError("user.purse", "not_enough_money"));
            }
        } else {

            if (value.compareTo(startingPrice) < 0) {
                validationResult.addError(new ValidationError("bet.value", "less_than_starting_price"));
            }

            if (userDTO.getUserAccount().getPurse().compareTo(value) < 0) {
                validationResult.addError(new ValidationError("user.purse", "not_enough_money"));
            }
        }


        return validationResult;
    }
}
