package by.epam.training.validation;

import javax.servlet.http.HttpServletRequest;

public interface RequestValidator {

    ValidationResult validate(HttpServletRequest req);
}
