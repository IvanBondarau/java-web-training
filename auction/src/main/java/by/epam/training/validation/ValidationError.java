package by.epam.training.validation;

import java.util.Objects;

public class ValidationError {

    private String subject;
    private String error;

    public ValidationError(String subject, String error) {
        this.subject = subject;
        this.error = error;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidationError that = (ValidationError) o;
        return subject.equals(that.subject) &&
                error.equals(that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subject, error);
    }

    @Override
    public String toString() {
        return "error." + subject + "." + error;
    }
}
