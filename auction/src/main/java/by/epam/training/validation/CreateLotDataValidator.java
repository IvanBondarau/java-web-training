package by.epam.training.validation;

import by.epam.training.command.CreateLotCommand;
import by.epam.training.time.TimeService;
import by.epam.training.user.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class CreateLotDataValidator implements RequestValidator {


    private static final Logger LOGGER = Logger.getLogger(CreateLotCommand.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private static final Pattern numberPatter = Pattern.compile("\\d{1,9}([.]\\d{1,9})?");

    @Override
    public ValidationResult validate(HttpServletRequest req) {

        ValidationResult result = new ValidationResult();

        UserDTO userDTO = (UserDTO) req.getSession().getAttribute("user");

        if (userDTO == null) {
            throw new ValidatorException("User is empty while creating lot");
        }

        String name = req.getParameter("name");

        if (name == null || name.equals("")) {
            result.addError(new ValidationError("lot.name", "empty"));
        }


        String startDateStr = req.getParameter("start");

        if (startDateStr == null) {
            result.addError(new ValidationError("lot.startDate", "empty"));
            return result;
        }

        String endDateStr = req.getParameter("end");

        if (endDateStr == null) {
            result.addError(new ValidationError("lot.endDate", "empty"));
            return result;
        }

        Timestamp start;
        Timestamp end;
        try {

            Date startDate = dateFormat.parse(startDateStr);
            Date endDate = dateFormat.parse(endDateStr);
            start = new Timestamp(startDate.getTime());
            end = new Timestamp(endDate.getTime());

        } catch (ParseException e) {
            result.addError(new ValidationError("lot.date", "invalid_format"));
            return result;
        }

        if (start.before(TimeService.getCurrentTimestamp())) {
            result.addError(new ValidationError("lot.startDate", "less_than_current"));
            return result;
        }

        if (!start.before(end)) {
            result.addError(new ValidationError("lot.endDate", "less_than_start"));
        }

        String startingPriceStr = req.getParameter("startingPrice");
        if (startingPriceStr == null) {
            result.addError(new ValidationError("lot.startingPrice", "empty"));
            return result;
        }
        if (!numberPatter.matcher(startingPriceStr).matches()) {
            result.addError(new ValidationError("lot.startingPrice", "invalid_format"));
            return result;
        }

        String step = req.getParameter("step");
        if (step == null) {
            result.addError(new ValidationError("lot.step", "empty"));
            return result;
        }
        if (!numberPatter.matcher(step).matches()) {
            result.addError(new ValidationError("lot.step", "invalid_format"));
            return result;
        }

        return result;

    }

}
