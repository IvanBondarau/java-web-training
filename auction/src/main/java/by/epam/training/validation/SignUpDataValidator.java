package by.epam.training.validation;

import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

public class SignUpDataValidator implements RequestValidator {

    private static final Pattern loginPattern = Pattern.compile("[a-zA-Z0-9_#-]{4,}");

    private static final Pattern emailPattern = Pattern.compile("\\b[\\w.-]+@[\\w.-]+\\.\\w{2,4}\\b");

    private static final Pattern passwordPattern = Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$");


    private UserService userService;

    public SignUpDataValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        String login = req.getParameter("login");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String passwordRep = req.getParameter("passwordRepeat");

        ValidationResult result = new ValidationResult();

        if (login == null || !loginPattern.matcher(login).matches()) {
            result.addError(new ValidationError("login", "not_valid"));
        } else {
            try {
                if (!userService.checkLoginAvailable(login)) {
                    result.addError(new ValidationError("login", "already_taken"));
                }
            } catch (UserServiceException e) {
                throw new ValidatorException(e.getMessage(), e);
            }
        }

        if (email == null || !emailPattern.matcher(email).matches()) {
            result.addError(new ValidationError("email", "not_valid"));
        } else {
            try {
                if (!userService.checkEmailAvailable(email)) {
                    result.addError(new ValidationError("email", "already_taken"));
                }
            } catch (UserServiceException e) {
                throw new ValidatorException(e.getMessage(), e);
            }
        }

        /*if (password == null || !passwordPattern.matcher(password).matches()) {
            result.addError(new ValidationError("password", "doesn't match pattern"));
        }*/

        if (password == null || password.equals("")) {
            result.addError(new ValidationError("password", "empty"));
        }

        if (password != null && !password.equals(passwordRep)) {
            result.addError(new ValidationError("passwordRepeat", "doesnt_match"));
        }

        return result;
    }
}
