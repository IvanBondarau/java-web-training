package by.epam.training.validation;

import java.util.LinkedList;
import java.util.List;

public class ValidationResult {

    List<ValidationError> errors;

    public ValidationResult() {
        this.errors = new LinkedList<>();
    }

    public void addError(ValidationError error) {
        errors.add(error);
    }

    public List<ValidationError> getErrors() {
        return errors;
    }

    public boolean isValid() {
        return errors.isEmpty();
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "errors=" + errors +
                '}';
    }
}
