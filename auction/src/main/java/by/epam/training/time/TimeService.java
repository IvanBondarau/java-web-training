package by.epam.training.time;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeService {

    private static final DateFormat dateFormat = new SimpleDateFormat("HH:mm dd.MM.yyyy ");

    public static String getCurrentDateTimeStr() {
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static Timestamp getCurrentTimestamp() {

        Date date= new Date();
        long time = date.getTime();

        return new Timestamp(time);
    }

}
