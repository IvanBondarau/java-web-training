package by.epam.training.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LanguageFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = ((HttpServletRequest)servletRequest);
        boolean languageCookieExists = false;

        Cookie[] cookie = request.getCookies();
        if (cookie != null) {
            for (Cookie cook: cookie) {
                if ("language".equals(cook.getName())) {
                    request.getSession().setAttribute("language", cook.getValue());
                    languageCookieExists = true;
                }
            }
        }

        if (!languageCookieExists) {
            Cookie language = new Cookie("language", "en-US");
            language.setMaxAge(7200);
            ((HttpServletResponse) servletResponse).addCookie(language);
            ((HttpServletRequest) servletRequest).getSession().setAttribute("language", "en-US");
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
