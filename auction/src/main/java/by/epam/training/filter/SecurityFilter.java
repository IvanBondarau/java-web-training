package by.epam.training.filter;

import by.epam.training.entity.UserRole;
import by.epam.training.security.SecurityController;
import by.epam.training.security.SecurityControllerImpl;
import by.epam.training.user.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class SecurityFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(SecurityFilter.class);

    private SecurityController securityController;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest)servletRequest;
        HttpServletResponse resp = (HttpServletResponse)servletResponse;

        String commandName = servletRequest.getParameter("command");
        if (commandName == null) {
            commandName = "forwardHomePage";
        }

        if (!securityController.isCommandRegistered(commandName)) {
            LOGGER.error("undefined command name: " + commandName);
            resp.sendRedirect(req.getContextPath() + "/jsp/errorPage.jsp");
            return;
        }

        UserDTO user = (UserDTO)req.getSession().getAttribute("user");
        boolean authorizationResult = false;
        if (user == null) {
            authorizationResult = securityController.isAllowedToAnonymous(commandName);
        } else {
            authorizationResult = securityController.isAllowed(commandName, user.getRoles());
        }

        if (authorizationResult) {
            LOGGER.info("Allowed " + commandName + " for " + user);
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            LOGGER.error("not enough privileges: commandName = " + commandName + ", user = " + user);
            resp.sendRedirect(req.getContextPath() + "/jsp/errorPage.jsp");
        }

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.securityController = SecurityControllerImpl.getInstance();
    }

    @Override
    public void destroy() {

    }
}
