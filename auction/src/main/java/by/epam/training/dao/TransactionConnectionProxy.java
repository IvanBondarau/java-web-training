package by.epam.training.dao;

import java.sql.Connection;

public class TransactionConnectionProxy implements ConnectionProxy {

    private TransactionManager transactionManager;
    private Connection connection;

    public TransactionConnectionProxy(TransactionManager transactionManager, Connection connection) {
        this.transactionManager = transactionManager;
        this.connection = connection;
    }

    @Override
    public Connection get() {
        return connection;
    }

    @Override
    public void close() {

    }

    public boolean isValid() {
        return connection != null;
    }


}
