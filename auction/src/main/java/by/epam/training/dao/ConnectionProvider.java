package by.epam.training.dao;

@FunctionalInterface
public interface ConnectionProvider {
    ConnectionProxy getConnection();
}
