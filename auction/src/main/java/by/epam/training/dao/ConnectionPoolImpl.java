package by.epam.training.dao;

import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPoolImpl implements ConnectionPool {

    private static final Logger LOGGER = Logger.getLogger(ConnectionPoolImpl.class);

    private static final String DATABASE_PROPERTIES = "datasource.properties";

    private Queue<Connection> availableConnections;
    private Queue<Connection> usingConnections;
    private Condition emptyPool;

    private final Lock connectionLock;

    private static final Lock instanceLock = new ReentrantLock();
    private static ConnectionPoolImpl instance = null;

    private Driver driver;
    private String databaseUrl;
    private String databaseUser;
    private String databasePassword;
    private Integer poolSize;


    private ConnectionPoolImpl() {
        loadProperties();

        try {
            availableConnections = new LinkedList<>();
            for (int i = 0; i < poolSize; i++) {
                Connection connection = DriverManager.getConnection(databaseUrl, databaseUser, databasePassword);
                connection.setAutoCommit(false);
                availableConnections.add(connection);
            }
        } catch (SQLException e) {
            throw new ConnectionPoolException("Error while establishing connection with database: " + e.getSQLState(), e);
        }
        usingConnections = new LinkedList<>();
        connectionLock = new ReentrantLock();
        emptyPool = connectionLock.newCondition();
        LOGGER.info("Connection pool created");
    }

    private void loadProperties() {
        try {
            Properties property = new Properties();
            String path = this.getClass().getClassLoader().getResource(DATABASE_PROPERTIES).getPath();
            FileReader reader = new FileReader(path);
            property.load(reader);
            databaseUrl = property.getProperty("db.url");
            poolSize = Integer.parseInt(property.getProperty("db.pool.size"));
            databaseUser = property.getProperty("db.user");
            databasePassword = property.getProperty("db.password");
            String dbDriverClass = property.getProperty("db.driver");
            Class driverClass = Class.forName(dbDriverClass);
            this.driver = (Driver) driverClass.newInstance();
            DriverManager.registerDriver(this.driver);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new ConnectionPoolException("Unable to create driver: " + e.getMessage(), e);
        } catch (IOException e) {
            throw new ConnectionPoolException("Unable to read property file: " + e.getMessage(), e );
        } catch (SQLException e) {
            throw new ConnectionPoolException("Unable to register driver: " + e.getSQLState(), e);
        }
    }

    public static ConnectionPoolImpl getInstance() {
        if (instance == null) {
            instanceLock.lock();
            if (instance == null) {
                instance = new ConnectionPoolImpl();
            }
            instanceLock.unlock();
        }
        return instance;
    }

    @Override
    public Connection getConnection() {
        connectionLock.lock();
        try {
            while (availableConnections.isEmpty()) {
                emptyPool.await();
            }
            Connection connection = availableConnections.poll();
            usingConnections.add(connection);
            return connection;
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException: " + e.getMessage());
            throw new ConnectionPoolException(e);
        } finally {
            connectionLock.unlock();
        }
    }

    @Override
    public void releaseConnection(Connection connection) {
        connectionLock.lock();
        try {
            if (!usingConnections.contains(connection)) {
                throw new ConnectionPoolException("Releasable connection not from connection pool");
            }
            usingConnections.remove(connection);
            availableConnections.add(connection);
            emptyPool.signal();
        } finally {
            connectionLock.unlock();
        }
    }

    @Override
    public void close() {
        try {
            for (Connection connection : availableConnections) {
                connection.close();
            }
            for (Connection connection : usingConnections) {
                connection.close();
            }
            DriverManager.deregisterDriver(this.driver);
        } catch (SQLException e) {
            throw new ConnectionPoolException("SQLException was thrown during destruction of connection pool: " + e.getMessage(), e);
        }
        LOGGER.info("Connection pool destroyed");
    }

}