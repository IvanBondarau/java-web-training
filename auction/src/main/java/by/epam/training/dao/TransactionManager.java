package by.epam.training.dao;

import java.sql.SQLException;

public interface TransactionManager {

    void beginTransaction();

    void commitTransaction();

    void rollbackTransaction();

    TransactionConnectionProxy getConnection();
}
