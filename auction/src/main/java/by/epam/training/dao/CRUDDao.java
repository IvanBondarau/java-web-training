package by.epam.training.dao;

import java.sql.SQLException;
import java.util.List;

public interface CRUDDao<T extends IdentifiedEntity> {

    Long create(T entity) throws DAOException;

    T read(Long id) throws DAOException;

    void update(T entity) throws DAOException;

    void delete(Long id) throws DAOException;

    List<T> findAll() throws DAOException;
}
