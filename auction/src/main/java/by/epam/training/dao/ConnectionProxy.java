package by.epam.training.dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionProxy extends AutoCloseable {
    Connection get();
    void close() throws SQLException;
}
