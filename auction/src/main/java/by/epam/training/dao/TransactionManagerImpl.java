package by.epam.training.dao;


import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class TransactionManagerImpl implements TransactionManager {

    private static final Logger LOGGER = Logger.getLogger(TransactionManagerImpl.class);

    private ConnectionPool connectionPool;
    private ThreadLocal<Connection> connectionThreadLocal;

    public TransactionManagerImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
        this.connectionThreadLocal = new ThreadLocal<>();
    }

    @Override
    public void beginTransaction() {
        Connection connection = connectionThreadLocal.get();
        if (connection == null) {
            Connection poolConnection = connectionPool.getConnection();
            connectionThreadLocal.set(poolConnection);
            LOGGER.info("Transaction started");
        } else {
            LOGGER.warn("Transaction has already started");
        }
    }

    @Override
    public void commitTransaction() {
        try {
            Connection connection = connectionThreadLocal.get();
            if (connection != null) {
                connection.commit();
                LOGGER.info("Transaction commit: " + connection);
                connectionThreadLocal.remove();
                connectionPool.releaseConnection(connection);
            } else {
                LOGGER.error("Connection is null");
            }
        } catch (SQLException e) {
            throw new TransactionException("SQLException was thrown during transaction commit: " + e.getMessage(), e);
        }
    }

    @Override
    public void rollbackTransaction() {
        try {
            Connection connection = connectionThreadLocal.get();
            if (connection != null) {
                connection.rollback();
                LOGGER.info("Transaction rollback: " + connection);
                connectionThreadLocal.remove();
                connectionPool.releaseConnection(connection);
            } else {
                LOGGER.error("Connection is null");
            }
        } catch (SQLException e) {
            throw new TransactionException("SQLException was thrown during transaction rollback: " + e.getMessage(), e);
        }
    }

    @Override
    public TransactionConnectionProxy getConnection() {
        return new TransactionConnectionProxy(this, connectionThreadLocal.get());
    }
}
