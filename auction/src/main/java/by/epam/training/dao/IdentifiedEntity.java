package by.epam.training.dao;

public interface IdentifiedEntity {
    Long getId();
    void setId(Long id);
}
