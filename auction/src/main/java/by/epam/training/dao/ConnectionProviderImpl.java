package by.epam.training.dao;

public class ConnectionProviderImpl implements ConnectionProvider {

    private TransactionManager transactionManager;
    private ConnectionPool connectionPool;

    public ConnectionProviderImpl(TransactionManager transactionManager, ConnectionPool connectionPool) {
        this.transactionManager = transactionManager;
        this.connectionPool = connectionPool;
    }

    @Override
    public ConnectionProxy getConnection() {

        TransactionConnectionProxy transactionManagerConnection = transactionManager.getConnection();
        if (transactionManagerConnection.isValid()) {
            return transactionManagerConnection;
        } else {
            return new ManagerConnectionProxy(connectionPool, connectionPool.getConnection());
        }
    }

}
