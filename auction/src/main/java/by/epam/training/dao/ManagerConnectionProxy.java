package by.epam.training.dao;

import java.sql.Connection;
import java.sql.SQLException;

public class ManagerConnectionProxy implements ConnectionProxy {

    private ConnectionPool connectionPool;
    private Connection connection;

    public ManagerConnectionProxy(ConnectionPool connectionPool, Connection connection) {
        this.connectionPool = connectionPool;
        this.connection = connection;
    }

    @Override
    public Connection get() {
        return connection;
    }

    @Override
    public void close() throws SQLException {
        connection.commit();
        connectionPool.releaseConnection(connection);
    }
}
