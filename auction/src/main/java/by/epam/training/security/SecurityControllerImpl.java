package by.epam.training.security;

import by.epam.training.ApplicationConstant;
import by.epam.training.entity.UserRole;
import lombok.Builder;
import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class SecurityControllerImpl implements SecurityController {

    private static final Logger LOGGER = Logger.getLogger(SecurityControllerImpl.class);

    private static final String SECURITY_PROPERTIES = "security.properties";

    private static SecurityControllerImpl instance = null;
    private static final Lock INSTANCE_LOCK = new ReentrantLock();

    private Map<String, Set<String>> commandPermissions = new HashMap<>();

    private SecurityControllerImpl() {
        loadProperties();
    }

    @Override
    public boolean isAllowed(String commandName, List<UserRole> roles) {
        for (UserRole role: roles) {
            if (commandPermissions.get(commandName).contains(role.getName())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isAllowedToAnonymous(String commandName) {
        return commandPermissions.get(commandName).contains(ApplicationConstant.ANONYMOUS_USER_ROLE_NAME);

    }

    @Override
    public boolean isCommandRegistered(String commandName) {
        return commandPermissions.containsKey(commandName);
    }

    private void registerPermission(String commandName, String userRoleName) {
        if (!this.commandPermissions.containsKey(commandName)) {
            this.commandPermissions.put(commandName, new HashSet<>());
        }
        this.commandPermissions.get(commandName).add(userRoleName);
    }


    public static SecurityControllerImpl getInstance() {
        if (instance == null) {
            try {
                INSTANCE_LOCK.lock();
                if (instance == null) {
                    instance = new SecurityControllerImpl();
                }
            } finally {
                INSTANCE_LOCK.unlock();
            }

        }
        return instance;
    }


    private void loadProperties() {
        try {
            Properties property = new Properties();
            String path = this.getClass().getClassLoader().getResource(SECURITY_PROPERTIES).getPath();
            FileReader reader = new FileReader(path);
            property.load(reader);
            for (String commandName: property.stringPropertyNames()) {
                String[] roles = ((String)property.get(commandName)).split(",");
                for (String role: roles) {
                    registerPermission(commandName, role);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Unable to ready security properties: " + e.getMessage());
            throw new java.lang.SecurityException(e);
        }

    }
}
