package by.epam.training.security;

import by.epam.training.entity.UserRole;

import java.util.List;

public interface SecurityController {

    boolean isAllowed(String commandName, List<UserRole> roles);
    boolean isAllowedToAnonymous(String commandName);
    boolean isCommandRegistered(String commandName);

}
