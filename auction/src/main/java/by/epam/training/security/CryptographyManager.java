package by.epam.training.security;

public interface CryptographyManager {
    String encrypt(String string);
    String decrypt(String string);
}
