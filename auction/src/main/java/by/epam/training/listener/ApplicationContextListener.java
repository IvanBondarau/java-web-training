package by.epam.training.listener;

import by.epam.training.ApplicationContext;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationContextListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ApplicationContextListener.class);

    private ApplicationContext applicationContext;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        applicationContext = ApplicationContext.getInstance();

        sce.getServletContext().setAttribute("commandProvider", applicationContext.getCommandProvider());

        LOGGER.info("Application context created");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        applicationContext.destroy();
        LOGGER.info("Application context destroyed");
    }
}
