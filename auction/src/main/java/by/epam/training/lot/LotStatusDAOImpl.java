package by.epam.training.lot;

import by.epam.training.dao.ConnectionProvider;
import by.epam.training.dao.ConnectionProxy;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.LotStatus;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class LotStatusDAOImpl implements LotStatusDAO {

    private static final String READ_LOT_STATUS = "SELECT `lot_status`.`id`,\n" +
            "    `lot_status`.`name`\n" +
            "FROM `lot_status` WHERE id = ?;";

    private static final String GET_STATUS_BY_NAME = "SELECT `lot_status`.`id`,\n" +
            "    `lot_status`.`name`\n" +
            "FROM `lot_status` WHERE name = ?;";

    private ConnectionProvider connectionProvider;

    public LotStatusDAOImpl(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    @Override
    public LotStatus read(Long id) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(READ_LOT_STATUS);
            preparedStatement.setLong(1, id);
            ResultSet accountResultSet = preparedStatement.executeQuery();

            LotStatus lotStatus;
            if (accountResultSet.next()) {
                lotStatus = this.buildLotStatus(accountResultSet);
                return lotStatus;
            } else {
                throw new LotStatusNotFoundException("LotStatus with id " + id + " not found");
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading lot status by id: " + e.getMessage(), e);
        }
    }

    @Override
    public LotStatus getByName(String name) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(GET_STATUS_BY_NAME);
            preparedStatement.setString(1, name);
            ResultSet accountResultSet = preparedStatement.executeQuery();

            LotStatus lotStatus;
            if (accountResultSet.next()) {
                lotStatus = this.buildLotStatus(accountResultSet);
                return lotStatus;
            } else {
                throw new LotStatusNotFoundException("LotStatus with name " + name + " not found");
            }
        } catch (SQLException | LotStatusNotFoundException e) {
            throw new DAOException("SQLException was thrown during reading lot status by name: " + e.getMessage(), e);
        }
    }


    private LotStatus buildLotStatus(ResultSet resultSet) throws SQLException {
        int i = 0;
        return LotStatus.builder()
                .id(resultSet.getLong(++i))
                .name(resultSet.getString(++i))
                .build();
    }
}
