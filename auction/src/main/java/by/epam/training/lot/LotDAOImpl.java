package by.epam.training.lot;

import by.epam.training.dao.ConnectionProvider;
import by.epam.training.dao.ConnectionProxy;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.Lot;
import by.epam.training.entity.LotStatus;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LotDAOImpl implements LotDAO {

    private static final Logger LOGGER = Logger.getLogger(LotDAOImpl.class);

    private static final String INSERT_LOT = "INSERT INTO lot" +
            "(starting_price," +
            "current_price," +
            "`owner_id`,\n" +
            "`start`,\n" +
            "`end`,\n" +
            "`step`,\n" +
            "`name`,\n" +
            "`description`,\n" +
            "`lot_status_id`)\n" +
            "VALUES\n" +
            "(?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String READ_LOT = "SELECT `lot`.`id`,\n" +
            "    `lot`.`starting_price`,\n" +
            "    `lot`.`current_price`,\n" +
            "    `lot`.`owner_id`,\n" +
            "    `lot`.`start`,\n" +
            "    `lot`.`end`,\n" +
            "    `lot`.`step`,\n" +
            "    `lot`.`name`,\n" +
            "    `lot`.`description`,\n" +
            "    `lot`.`lot_status_id`\n" +
            "FROM lot WHERE id = ?;\n";

    private static final String UPDATE_LOT = "UPDATE `lot`\n" +
            "SET\n" +
            "`starting_price` = ?,\n" +
            "`current_price` = ?,\n" +
            "`owner_id` = ?,\n" +
            "`start` = ?,\n" +
            "`end` = ?,\n" +
            "`step` = ?,\n" +
            "`name` = ?,\n" +
            "`description` = ?,\n" +
            "`lot_status_id` = ?\n" +
            "WHERE `id` = ?;\n";

    private static final String DELETE_LOT = "DELETE FROM lot\n" +
            "WHERE id = ?;\n";

    private static final String SELECT_ALL = "SELECT `lot`.`id`,\n" +
            "    `lot`.`starting_price`,\n" +
            "    `lot`.`current_price`,\n" +
            "    `lot`.`owner_id`,\n" +
            "    `lot`.`start`,\n" +
            "    `lot`.`end`,\n" +
            "    `lot`.`step`,\n" +
            "    `lot`.`name`,\n" +
            "    `lot`.`description`,\n" +
            "    `lot`.`lot_status_id`\n" +
            "FROM `lot`;\n";

    private static final String SELECT_STARTING_LOTS = "SELECT `lot`.`id`,\n" +
            "    `lot`.`starting_price`,\n" +
            "    `lot`.`current_price`,\n" +
            "    `lot`.`owner_id`,\n" +
            "    `lot`.`start`,\n" +
            "    `lot`.`end`,\n" +
            "    `lot`.`step`,\n" +
            "    `lot`.`name`,\n" +
            "    `lot`.`description`,\n" +
            "    `lot`.`lot_status_id`\n" +
            "FROM `lot` WHERE lot.start <= ? AND ? <= lot.end AND lot.lot_status_id != ?;\n";

    private static final String SELECT_FINISHING_LOTS = "SELECT `lot`.`id`,\n" +
            "    `lot`.`starting_price`,\n" +
            "    `lot`.`current_price`,\n" +
            "    `lot`.`owner_id`,\n" +
            "    `lot`.`start`,\n" +
            "    `lot`.`end`,\n" +
            "    `lot`.`step`,\n" +
            "    `lot`.`name`,\n" +
            "    `lot`.`description`,\n" +
            "    `lot`.`lot_status_id`\n" +
            "FROM `lot` WHERE lot.end < ? AND lot.lot_status_id != ?;\n";

    private ConnectionProvider connectionProvider;

    public LotDAOImpl(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    @Override
    public Long create(Lot entity) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(INSERT_LOT, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            preparedStatement.setBigDecimal(++i, entity.getStartingPrice());
            preparedStatement.setBigDecimal(++i, entity.getCurrentPrice());
            preparedStatement.setLong(++i, entity.getOwnerId());
            preparedStatement.setTimestamp(++i, entity.getStart());
            preparedStatement.setTimestamp(++i, entity.getEnd());
            preparedStatement.setBigDecimal(++i, entity.getStep());
            preparedStatement.setString(++i, entity.getName());
            preparedStatement.setString(++i, entity.getDescription());
            preparedStatement.setLong(++i, entity.getStatusId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            while (resultSet.next()) {
                entity.setId(resultSet.getLong(1));
            }
            return entity.getId();
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred in SQL statement: " + e.getMessage(), e);
        }
    }

    @Override
    public Lot read(Long id) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(READ_LOT);
            preparedStatement.setLong(1, id);
            ResultSet accountResultSet = preparedStatement.executeQuery();

            Lot lot;
            if (accountResultSet.next()) {
                lot = this.buildLot(accountResultSet);
                return lot;
            } else {
                throw new LotNotFoundException("Lot with id " + id + " not found");
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading lot by id: " + e.getMessage(), e);
        }
    }

    @Override
    public void update(Lot entity) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(UPDATE_LOT);
            int i = 0;
            preparedStatement.setBigDecimal(++i, entity.getStartingPrice());
            preparedStatement.setBigDecimal(++i, entity.getCurrentPrice());
            preparedStatement.setLong(++i, entity.getOwnerId());
            preparedStatement.setTimestamp(++i, entity.getStart());
            preparedStatement.setTimestamp(++i, entity.getEnd());
            preparedStatement.setBigDecimal(++i, entity.getStep());
            preparedStatement.setString(++i, entity.getName());
            preparedStatement.setString(++i, entity.getDescription());
            preparedStatement.setLong(++i, entity.getStatusId());
            preparedStatement.setLong(++i, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during updating lot: " + e.getMessage(), e);
        }
    }

    @Override
    public void delete(Long id) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(DELETE_LOT);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during deleting lor: " + e.getMessage(), e);
        }
    }

    @Override
    public List<Lot> findAll() throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            List<Lot> lots = new ArrayList<>();
            try (PreparedStatement preparedStatement = connection.get().prepareStatement(SELECT_ALL)) {
                ResultSet accountResultSet = preparedStatement.executeQuery();
                while (accountResultSet.next()) {
                    Lot lot = buildLot(accountResultSet);
                    lots.add(lot);
                }
            }
            return lots;
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading lots " + e.getMessage(), e);
        }
    }

    @Override
    public List<Lot> selectStartingLots(Timestamp currentTime, Long lotStatusStartingId) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            List<Lot> lots = new ArrayList<>();
            try (PreparedStatement preparedStatement = connection.get().prepareStatement(SELECT_STARTING_LOTS)) {

                int i = 0;
                preparedStatement.setTimestamp(++i, currentTime);
                preparedStatement.setTimestamp(++i, currentTime);
                preparedStatement.setLong(++i, lotStatusStartingId);

                ResultSet accountResultSet = preparedStatement.executeQuery();
                while (accountResultSet.next()) {
                    Lot lot = buildLot(accountResultSet);
                    lots.add(lot);
                }
            }
            return lots;
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading starting lots " + e.getMessage(), e);
        }
    }

    @Override
    public List<Lot> selectFinishingLots(Timestamp currentTime, Long lotStatusFinishingId) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            List<Lot> lots = new ArrayList<>();
            try (PreparedStatement preparedStatement = connection.get().prepareStatement(SELECT_FINISHING_LOTS)) {

                int i = 0;
                preparedStatement.setTimestamp(++i, currentTime);
                preparedStatement.setLong(++i, lotStatusFinishingId);

                ResultSet accountResultSet = preparedStatement.executeQuery();
                while (accountResultSet.next()) {
                    Lot lot = buildLot(accountResultSet);
                    lots.add(lot);
                }
            }
            return lots;
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading finishing lots " + e.getMessage(), e);
        }
    }

    private Lot buildLot(ResultSet resultSet) throws SQLException {
        int i = 0;
        return Lot.builder()
                .id(resultSet.getLong(++i))
                .startingPrice(resultSet.getBigDecimal(++i))
                .currentPrice(resultSet.getBigDecimal(++i))
                .ownerId(resultSet.getLong(++i))
                .start(resultSet.getTimestamp(++i))
                .end(resultSet.getTimestamp(++i))
                .step(resultSet.getBigDecimal(++i))
                .name(resultSet.getString(++i))
                .description(resultSet.getString(++i))
                .statusId(resultSet.getLong(++i))
                .build();
    }

}
