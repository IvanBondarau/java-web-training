package by.epam.training.lot;

import by.epam.training.entity.LotStatus;
import lombok.*;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class LotPreview {

    Long id;
    String name;
    Long ownedId;
    String ownerLogin;
    BigDecimal currentPrice;
    LotStatus lotStatus;

}
