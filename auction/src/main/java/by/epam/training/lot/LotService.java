package by.epam.training.lot;

import by.epam.training.entity.UserAccount;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.List;

public interface LotService {

    LotDTO getLotById(Long id) throws LotServiceException;

    LotDTO createLot(UserAccount creator,
                     String name, String description,
                     Timestamp start, Timestamp end,
                     BigDecimal startingPrice, BigDecimal step) throws LotServiceException;

    List<LotDTO> getAllLots() throws LotServiceException;

    void removeLotById(Long id) throws LotServiceException;

    void clearUserLots(Long userId) throws LotServiceException;

    void startLots(Timestamp updateTime) throws LotServiceException;

    void finishLots(Timestamp updateTime) throws LotServiceException;

    void updateLotCurrentPrice(Long lotId) throws LotServiceException;
}
