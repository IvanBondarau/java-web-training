package by.epam.training.lot;

import by.epam.training.dao.CRUDDao;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.Lot;
import by.epam.training.entity.LotStatus;

import java.sql.Timestamp;
import java.util.List;

public interface LotDAO extends CRUDDao<Lot> {
    List<Lot> selectStartingLots(Timestamp currentTime, Long lotStatusStartingId) throws DAOException;
    List<Lot> selectFinishingLots(Timestamp currentTime, Long lotStatusFinishingId) throws DAOException;
}
