package by.epam.training.lot;

import by.epam.training.time.TimeService;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class LotUpdater extends Thread {

    private static final Logger LOGGER = Logger.getLogger(LotUpdater.class);

    private LotService lotService;

    public LotUpdater(LotService lotService) {
        this.lotService = lotService;
    }

    @Override
    public void run() {
        try {
            Timestamp currentTimestamp = TimeService.getCurrentTimestamp();
            lotService.startLots(currentTimestamp);
            lotService.finishLots(currentTimestamp);
            LOGGER.info("Updated lot status by timestamp: " + currentTimestamp);
        } catch (LotServiceException e) {
            LOGGER.error("Failed to update lot status: " + e.getMessage());
        }
    }
}
