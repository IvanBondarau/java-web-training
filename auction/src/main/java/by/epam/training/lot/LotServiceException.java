package by.epam.training.lot;

public class LotServiceException extends Exception {

    public LotServiceException() {
    }

    public LotServiceException(String message) {
        super(message);
    }

    public LotServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public LotServiceException(Throwable cause) {
        super(cause);
    }
}
