package by.epam.training.lot;

import by.epam.training.ApplicationConstant;
import by.epam.training.bet.BetDAO;
import by.epam.training.bet.BetService;
import by.epam.training.bet.BetServiceException;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.TransactionManager;
import by.epam.training.entity.Bet;
import by.epam.training.entity.Lot;
import by.epam.training.entity.LotStatus;
import by.epam.training.entity.UserAccount;
import by.epam.training.user.UserAccountDAO;
import by.epam.training.user.UserNotFoundException;
import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class LotServiceImpl implements LotService {

    private static final Logger LOGGER = Logger.getLogger(LotServiceImpl.class);

    private LotDAO lotDAO;
    private LotStatusDAO lotStatusDAO;
    private BetDAO betDAO;
    private UserAccountDAO userAccountDAO;

    private BetService betService;
    private UserService userService;

    private TransactionManager transactionManager;

    public LotServiceImpl(LotDAO lotDAO, LotStatusDAO lotStatusDAO, BetDAO betDAO, UserAccountDAO userAccountDAO,
                          BetService betService, UserService userService, TransactionManager transactionManager) {
        this.lotDAO = lotDAO;
        this.lotStatusDAO = lotStatusDAO;
        this.betDAO = betDAO;
        this.userAccountDAO = userAccountDAO;
        this.betService = betService;
        this.userService = userService;
        this.transactionManager = transactionManager;
    }

    @Override
    public LotDTO getLotById(Long id) throws LotServiceException {
        try {
            Lot lot = lotDAO.read(id);
            LotStatus lotStatus = lotStatusDAO.read(lot.getStatusId());
            UserAccount owner = userAccountDAO.read(lot.getOwnerId());
            return new LotDTO(lot, lotStatus, owner);
        } catch (DAOException e) {
            throw new LotServiceException("DAOException occurred: " + e.getMessage(), e);
        }
    }

    @Override
    public LotDTO createLot(UserAccount creator,
                            String name, String description,
                            Timestamp start, Timestamp end,
                            BigDecimal startingPrice, BigDecimal step)
            throws LotServiceException {
        transactionManager.beginTransaction();
        try {
            LotStatus status = lotStatusDAO.getByName(ApplicationConstant.NOT_STARTED_STATUS);
            LOGGER.info(status);
            Lot lot = Lot.builder()
                    .ownerId(creator.getId())
                    .name(name)
                    .description(description)
                    .start(start)
                    .end(end)
                    .startingPrice(startingPrice)
                    .currentPrice(null)
                    .step(step)
                    .statusId(status.getId())
                    .build();

            Long id = lotDAO.create(lot);

            UserAccount owner = userAccountDAO.read(lot.getOwnerId());

            LOGGER.info("Created lot with id " + id);

            transactionManager.commitTransaction();

            return new LotDTO(lot, status, owner);
        } catch (DAOException e) {
            transactionManager.rollbackTransaction();
            throw new LotServiceException("DAO exception occurred: " + e.getMessage(), e);
        }
    }

    @Override
    public List<LotDTO> getAllLots() throws LotServiceException {
        List<LotDTO> dtoList = new LinkedList<>();
        try {
            List<Lot> lots = lotDAO.findAll();
            for (Lot lot : lots) {
                UserAccount owner = userAccountDAO.read(lot.getOwnerId());
                LotStatus status = lotStatusDAO.read(lot.getStatusId());
                dtoList.add(new LotDTO(lot, status, owner));
            }
            return dtoList;
        } catch (DAOException e) {
            throw new LotServiceException(e);
        }
    }



    @Override
    public void removeLotById(Long lotId) throws LotServiceException {
        transactionManager.beginTransaction();
        try {
            LotDTO lotDTO = getLotById(lotId);
            if (!lotDTO.getLotStatus().getName().equals(ApplicationConstant.OVER_STATUS)) {
                List<Bet> bets = betDAO.getByLotId(lotId);
                for (Bet bet : bets) {
                    try {
                        UserAccount userAccount = userAccountDAO.read(bet.getUserAccountId());
                        userAccount.setPurse(userAccount.getPurse().add(bet.getValue()));
                        userAccount.setFrozenPurse(userAccount.getFrozenPurse().subtract(bet.getValue()));
                        userAccountDAO.update(userAccount);
                    } catch (UserNotFoundException e) {
                        LOGGER.warn("Bet creator with id " + bet.getUserAccountId() + " not found");
                        LOGGER.warn("Unable to return bet " + bet);
                    }

                    betDAO.deleteByUserIdAndLotId(bet.getUserAccountId(), lotId);
                }
            }
            lotDAO.delete(lotId);
            transactionManager.commitTransaction();
        } catch (DAOException e) {
            transactionManager.rollbackTransaction();
            LOGGER.error(e);
            throw new LotServiceException(e);
        }
    }

    @Override
    public void clearUserLots(Long userId) throws LotServiceException {
        transactionManager.beginTransaction();
        try {
            List<LotDTO> lots = getAllLots();
            for (LotDTO lotDTO : lots) {
                if (lotDTO.getOwner().getId().equals(userId)) {
                    removeLotById(lotDTO.getLot().getId());
                } else {
                    betService.removeFromLotByUserId(lotDTO.getLot().getId(), userId);
                    updateLotCurrentPrice(lotDTO.getLot().getId());
                }
            }
            transactionManager.commitTransaction();
        } catch (BetServiceException e) {
            LOGGER.error(e);
            transactionManager.rollbackTransaction();
            throw new LotServiceException(e);
        } catch (Throwable e) {
            transactionManager.rollbackTransaction();
            throw e;
        }
    }

    @Override
    public void startLots(Timestamp updateTime) throws LotServiceException {

        try {
            LotStatus inProgress = lotStatusDAO.getByName(ApplicationConstant.IN_PROGRESS_STATUS);
            List<Lot> startingLots = lotDAO.selectStartingLots(updateTime, inProgress.getId());
            for (Lot lot: startingLots) {
                lot.setStatusId(inProgress.getId());
                lotDAO.update(lot);
            }

        } catch (DAOException e) {
            throw new LotServiceException(e);
        }

    }

    @Override
    public void finishLots(Timestamp updateTime) throws LotServiceException {
        transactionManager.beginTransaction();
        try {
            LotStatus over = lotStatusDAO.getByName(ApplicationConstant.OVER_STATUS);
            List<Lot> finishingLots = lotDAO.selectFinishingLots(updateTime, over.getId());
            for (Lot lot: finishingLots) {
                lot.setStatusId(over.getId());
                lotDAO.update(lot);

                List<Bet> bets = betDAO.getByLotId(lot.getId());

                for (Bet bet: bets) {
                    LOGGER.info("Bet " + bet + " released");
                    BigDecimal value = bet.getValue();
                    UserAccount user = userAccountDAO.read(bet.getUserAccountId());
                    user.setFrozenPurse(user.getFrozenPurse().subtract(value));
                    user.setPurse(user.getPurse().add(value));
                    userAccountDAO.update(user);
                }

                if (!bets.isEmpty()) {
                    Bet winningBet = bets.get(0);
                    UserAccount lotOwner = userAccountDAO.read(winningBet.getLotOwnerId());
                    UserAccount betCreator = userAccountDAO.read(winningBet.getUserAccountId());
                    betCreator.setPurse(betCreator.getPurse().subtract(winningBet.getValue()));
                    lotOwner.setPurse(lotOwner.getPurse().add(winningBet.getValue()));
                    userAccountDAO.update(lotOwner);
                    userAccountDAO.update(betCreator);
                }
            }


            transactionManager.commitTransaction();
        } catch (DAOException e) {
            LOGGER.error(e);
            transactionManager.rollbackTransaction();
            throw new LotServiceException(e);
        }
    }

    @Override
    public void updateLotCurrentPrice(Long lotId) throws LotServiceException {
        try {
            Lot lot = lotDAO.read(lotId);
            Optional<Bet> bet = betDAO.getGreatestBet(lotId);
            if (bet.isPresent()) {
                lot.setCurrentPrice(bet.get().getValue());
            } else {
                lot.setCurrentPrice(null);
            }
            lotDAO.update(lot);
        } catch (DAOException e) {
            throw new LotServiceException(e);
        }
    }
}
