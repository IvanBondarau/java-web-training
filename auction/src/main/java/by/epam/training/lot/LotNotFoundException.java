package by.epam.training.lot;

import by.epam.training.dao.DAOException;

public class LotNotFoundException extends DAOException {
    public LotNotFoundException() {
    }

    public LotNotFoundException(String message) {
        super(message);
    }

    public LotNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public LotNotFoundException(Throwable cause) {
        super(cause);
    }
}
