package by.epam.training.lot;

public class LotUpdaterException extends RuntimeException {

    public LotUpdaterException() {
    }

    public LotUpdaterException(String message) {
        super(message);
    }

    public LotUpdaterException(String message, Throwable cause) {
        super(message, cause);
    }

    public LotUpdaterException(Throwable cause) {
        super(cause);
    }
}
