package by.epam.training.lot;

import by.epam.training.dao.DAOException;

public class LotStatusNotFoundException extends DAOException {
    public LotStatusNotFoundException() {
    }

    public LotStatusNotFoundException(String message) {
        super(message);
    }

    public LotStatusNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public LotStatusNotFoundException(Throwable cause) {
        super(cause);
    }
}
