package by.epam.training.lot;

import by.epam.training.entity.Lot;
import by.epam.training.entity.LotStatus;
import by.epam.training.entity.UserAccount;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class LotDTO {
    private Lot lot;
    private LotStatus lotStatus;
    private UserAccount owner;
}
