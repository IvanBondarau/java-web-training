package by.epam.training.lot;

import by.epam.training.dao.DAOException;
import by.epam.training.entity.LotStatus;

public interface LotStatusDAO {

    LotStatus read(Long id) throws DAOException;
    LotStatus getByName(String name) throws DAOException;

}
