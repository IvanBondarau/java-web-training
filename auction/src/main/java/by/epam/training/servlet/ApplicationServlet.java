package by.epam.training.servlet;

import by.epam.training.command.ServletCommand;
import by.epam.training.command.CommandProvider;
import by.epam.training.command.ServletCommandException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "App", urlPatterns = "/", loadOnStartup = 1)
public class ApplicationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        CommandProvider commandProvider = (CommandProvider)getServletContext().getAttribute("commandProvider");

        String commandName = req.getParameter("command");
        if (commandName == null) {
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
            return;
        }

        Optional<ServletCommand> command = commandProvider.getCommand(commandName);

        if (command.isPresent()) {
            try {
                command.get().execute(req, resp);
            } catch (ServletCommandException e) {
                throw new ServletException(e.getMessage(), e);
            }
        } else {
            throw new ServletException("Command not found");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
