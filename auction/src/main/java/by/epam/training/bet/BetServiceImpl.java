package by.epam.training.bet;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.TransactionManager;
import by.epam.training.entity.Bet;
import by.epam.training.entity.Lot;
import by.epam.training.entity.UserAccount;
import by.epam.training.lot.LotDAO;
import by.epam.training.lot.LotDAOImpl;
import by.epam.training.lot.LotServiceException;
import by.epam.training.time.TimeService;
import by.epam.training.user.UserAccountDAO;
import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class BetServiceImpl implements BetService {

    private static final Logger LOGGER = Logger.getLogger(BetServiceImpl.class);

    private TransactionManager transactionManager;

    private BetDAO betDAO;
    private UserAccountDAO userAccountDAO;

    private UserService userService;
    private LotDAO lotDAO;

    public BetServiceImpl(TransactionManager transactionManager, BetDAO betDAO, UserAccountDAO userAccountDAO, UserService userService, LotDAO lotDAO) {
        this.transactionManager = transactionManager;
        this.betDAO = betDAO;
        this.userAccountDAO = userAccountDAO;
        this.userService = userService;
        this.lotDAO = lotDAO;
    }

    @Override
    public List<BetDTO> getByLotId(Long lotId) throws BetServiceException {
        List<BetDTO> betDTOList = new LinkedList<>();
        try {
            List<Bet> bets = betDAO.getByLotId(lotId);
            for (Bet bet: bets) {
                UserAccount creator = userAccountDAO.read(bet.getUserAccountId());
                betDTOList.add(new BetDTO(bet, creator));
            }
            LOGGER.info("Bet list size: " + betDTOList.size()) ;
            return betDTOList;
        } catch (DAOException e) {
            throw new BetServiceException(e);
        }
    }


    @Override
    public Optional<Bet> findLastBet(Long user_account_id, Long lot_id) throws BetServiceException {
        try {
            return betDAO.findByUserIdAndLotId(user_account_id, lot_id);
        } catch (DAOException e) {
            throw new BetServiceException(e);
        }
    }

    @Override
    public void removeFromLotByUserId(Long lotId, Long userId) throws BetServiceException {
        try {
            betDAO.deleteByUserIdAndLotId(userId, lotId);
        } catch (DAOException e) {
            throw new BetServiceException(e);
        }
    }

    @Override
    public void placeBet(UserAccount user, Lot lot, BigDecimal value) throws BetServiceException {
        transactionManager.beginTransaction();
        try {
            Optional<Bet> previousBet = findLastBet(user.getId(), lot.getId());
            if (previousBet.isPresent()) {
                userService.unlockMoney(user, previousBet.get().getValue());
            }
            removeFromLotByUserId(lot.getId(), user.getId());
            createBet(user, lot, value);
            userService.blockMoney(user, value);
            lot.setCurrentPrice(value);
            lotDAO.update(lot);
            transactionManager.commitTransaction();
        } catch (UserServiceException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new BetServiceException(e);
        }
    }

    private void createBet(UserAccount user, Lot lot, BigDecimal value) throws BetServiceException {
        try {
            Bet bet = Bet.builder()
                    .userAccountId(user.getId())
                    .lotId(lot.getId())
                    .lotOwnerId(lot.getOwnerId())
                    .date(TimeService.getCurrentTimestamp())
                    .value(value)
                    .build();
            betDAO.create(bet);
        } catch (DAOException e) {
            throw new BetServiceException(e);
        }
    }



}
