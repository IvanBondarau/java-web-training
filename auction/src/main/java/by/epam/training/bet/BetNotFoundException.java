package by.epam.training.bet;

import by.epam.training.dao.DAOException;

public class BetNotFoundException extends DAOException {

    public BetNotFoundException() {
    }

    public BetNotFoundException(String message) {
        super(message);
    }

    public BetNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public BetNotFoundException(Throwable cause) {
        super(cause);
    }
}
