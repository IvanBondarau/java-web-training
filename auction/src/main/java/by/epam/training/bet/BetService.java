package by.epam.training.bet;

import by.epam.training.entity.Bet;
import by.epam.training.entity.Lot;
import by.epam.training.entity.UserAccount;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface BetService {
    List<BetDTO> getByLotId(Long auctionId) throws BetServiceException;

    Optional<Bet> findLastBet(Long user_account_id, Long lot_id) throws BetServiceException;

    void removeFromLotByUserId(Long lotId, Long userId) throws BetServiceException;

    void placeBet(UserAccount user, Lot lot, BigDecimal value) throws BetServiceException;
}
