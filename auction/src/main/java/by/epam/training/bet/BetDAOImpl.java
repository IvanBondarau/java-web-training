package by.epam.training.bet;

import by.epam.training.dao.ConnectionProvider;
import by.epam.training.dao.ConnectionProxy;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.Bet;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BetDAOImpl implements BetDAO {

    private static final String INSERT_BET = "INSERT INTO bet\n" +
            "(`user_account_id`,\n" +
            "`lot_id`,\n" +
            "`lot_owner_id`,\n" +
            "`value`,\n" +
            "`date`)\n" +
            "VALUES\n" +
            "(?,\n" +
            "?,\n" +
            "?,\n" +
            "?,\n" +
            "?);\n";

    private static final String READ_BET = "SELECT `bet`.`id`,\n" +
            "    `bet`.`user_account_id`,\n" +
            "    `bet`.`lot_id`,\n" +
            "    `bet`.`lot_owner_id`,\n" +
            "    `bet`.`value`,\n" +
            "    `bet`.`date`\n" +
            "FROM `bet` WHERE id = ?;\n";

    private static final String UPDATE_BET = "UPDATE `bet`\n" +
            "SET\n" +
            "`user_account_id` = ?,\n" +
            "`lot_id` = ?,\n" +
            "`lot_owner_id` = ?,\n" +
            "`value` = ?,\n" +
            "`date` = ?\n" +
            "WHERE `id` = ?;\n";

    private static final String DELETE_BET =  "DELETE FROM bet\n" +
            "WHERE id = ?;\n";

    private static final String SELECT_ALL = "SELECT `bet`.`id`,\n" +
            "    `bet`.`user_account_id`,\n" +
            "    `bet`.`lot_id`,\n" +
            "    `bet`.`lot_owner_id`,\n" +
            "    `bet`.`value`,\n" +
            "    `bet`.`date`\n" +
            "FROM `bet` ORDER BY bet.value;\n";

    private static final String SELECT_BY_LOT_ID = "SELECT `bet`.`id`,\n" +
            "    `bet`.`user_account_id`,\n" +
            "    `bet`.`lot_id`,\n" +
            "    `bet`.`lot_owner_id`,\n" +
            "    `bet`.`value`,\n" +
            "    `bet`.`date`\n" +
            "FROM `bet` WHERE bet.lot_id = ? ORDER BY bet.value DESC;\n";

    private static final String FIND_BY_LOT_AND_USER = "SELECT `bet`.`id`,\n" +
            "    `bet`.`user_account_id`,\n" +
            "    `bet`.`lot_id`,\n" +
            "    `bet`.`lot_owner_id`,\n" +
            "    `bet`.`value`,\n" +
            "    `bet`.`date`\n" +
            "FROM `bet` WHERE bet.lot_id = ? and bet.user_account_id = ? ORDER BY bet.value DESC;\n";


    private static final String FIND_LAST_BET = "SELECT `bet`.`id`,\n" +
            "    `bet`.`user_account_id`,\n" +
            "    `bet`.`lot_id`,\n" +
            "    `bet`.`lot_owner_id`,\n" +
            "    `bet`.`value`,\n" +
            "    `bet`.`date`\n" +
            "FROM `bet` WHERE bet.lot_id = ? ORDER BY bet.value DESC LIMIT 1;\n";

    private static final String DELETE_BY_LOT_AND_USER = "DELETE FROM bet\n" +
            "WHERE user_account_id = ? AND lot_id = ?;\n";

    private ConnectionProvider connectionProvider;

    public BetDAOImpl(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    @Override
    public Long create(Bet entity) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(INSERT_BET, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            preparedStatement.setLong(++i, entity.getUserAccountId());
            preparedStatement.setLong(++i, entity.getLotId());
            preparedStatement.setLong(++i, entity.getLotOwnerId());
            preparedStatement.setBigDecimal(++i, entity.getValue());
            preparedStatement.setTimestamp(++i, entity.getDate());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            while (resultSet.next()) {
                entity.setId(resultSet.getLong(1));
            }
            return entity.getId();
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred in SQL statement: " + e.getMessage(), e);
        }
    }

    @Override
    public Bet read(Long id) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(READ_BET);
            preparedStatement.setLong(1, id);
            ResultSet accountResultSet = preparedStatement.executeQuery();

            Bet bet;
            if (accountResultSet.next()) {
                bet = this.buildBet(accountResultSet);
                return bet;
            } else {
                throw new BetNotFoundException("Bet with id " + id + " not found");
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading bet by id: " + e.getMessage(), e);
        }
    }

    @Override
    public void update(Bet entity) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(UPDATE_BET);
            int i = 0;
            preparedStatement.setLong(++i, entity.getUserAccountId());
            preparedStatement.setLong(++i, entity.getLotId());
            preparedStatement.setLong(++i, entity.getLotOwnerId());
            preparedStatement.setBigDecimal(++i, entity.getValue());
            preparedStatement.setTimestamp(++i, entity.getDate());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during updating bet: " + e.getMessage(), e);
        }
    }

    @Override
    public void delete(Long id) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(DELETE_BET);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during deleting bet: " + e.getMessage(), e);
        }
    }

    @Override
    public List<Bet> findAll() throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            List<Bet> bets = new ArrayList<>();
            PreparedStatement preparedStatement = connection.get().prepareStatement(SELECT_ALL);
            ResultSet accountResultSet = preparedStatement.executeQuery();
            while (accountResultSet.next()) {
                Bet lot = buildBet(accountResultSet);
                bets.add(lot);
            }
            return bets;
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during getting bets " + e.getMessage(), e);
        }
    }

    @Override
    public List<Bet> getByLotId(Long lotId) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            List<Bet> bets = new ArrayList<>();
            PreparedStatement preparedStatement = connection.get().prepareStatement(SELECT_BY_LOT_ID);
            preparedStatement.setLong(1, lotId);
            ResultSet accountResultSet = preparedStatement.executeQuery();
            while (accountResultSet.next()) {
                Bet lot = buildBet(accountResultSet);
                bets.add(lot);
            }
            return bets;
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during getting bets by lotId " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteByUserIdAndLotId(Long userId, Long lotId) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(DELETE_BY_LOT_AND_USER);
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, lotId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during deleting bet: " + e.getMessage(), e);
        }
    }

    @Override
    public Optional<Bet> findByUserIdAndLotId(Long userId, Long lotId) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(FIND_BY_LOT_AND_USER);
            preparedStatement.setLong(1, lotId);
            preparedStatement.setLong(2, userId);
            ResultSet accountResultSet = preparedStatement.executeQuery();

            Bet bet;
            if (accountResultSet.next()) {
                bet = this.buildBet(accountResultSet);
                return Optional.of(bet);
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading bet by id: " + e.getMessage(), e);
        }
    }

    @Override
    public Optional<Bet> getGreatestBet(Long lotId) throws DAOException {
        try (ConnectionProxy connectionProxy = connectionProvider.getConnection()){
            PreparedStatement statement = connectionProxy.get().prepareStatement(FIND_LAST_BET);
            statement.setLong(1, lotId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(buildBet(resultSet));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during getting greatest bet " + e.getMessage(), e);
        }
    }

    private Bet buildBet(ResultSet set) throws SQLException {
        int i = 0;
        return Bet.builder()
                .id(set.getLong(++i))
                .userAccountId(set.getLong(++i))
                .lotId(set.getLong(++i))
                .lotOwnerId(set.getLong(++i))
                .value(set.getBigDecimal(++i))
                .date(set.getTimestamp(++i))
                .build();

    }
}
