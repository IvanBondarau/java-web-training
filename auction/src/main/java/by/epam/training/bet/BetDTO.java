package by.epam.training.bet;

import by.epam.training.entity.Bet;
import by.epam.training.entity.UserAccount;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@EqualsAndHashCode
@ToString
public class BetDTO {
    private Bet bet;
    private UserAccount creator;
}
