package by.epam.training.bet;

public class BetServiceException extends Exception {

    public BetServiceException() {
    }

    public BetServiceException(String message) {
        super(message);
    }

    public BetServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public BetServiceException(Throwable cause) {
        super(cause);
    }
}
