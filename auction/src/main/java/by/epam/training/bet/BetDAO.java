package by.epam.training.bet;

import by.epam.training.dao.CRUDDao;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.Bet;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface BetDAO extends CRUDDao<Bet> {
    List<Bet> getByLotId(Long lotId) throws DAOException;
    void deleteByUserIdAndLotId(Long userId, Long lotId) throws DAOException;
    Optional<Bet> findByUserIdAndLotId(Long userId, Long lotId) throws DAOException;
    Optional<Bet> getGreatestBet(Long lotId) throws DAOException;
}
