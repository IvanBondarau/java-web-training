package by.epam.training.user;

import by.epam.training.dao.CRUDDao;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.UserAccount;

import java.math.BigDecimal;
import java.util.List;

public interface UserAccountDAO extends CRUDDao<UserAccount> {
    UserAccount getByLoginPassword(String login, String password) throws DAOException;
    boolean isLoginAvailable(String login) throws DAOException;
    boolean isEmailAvailable(String email) throws DAOException;

    List<Long> getUserIdList() throws DAOException;
}
