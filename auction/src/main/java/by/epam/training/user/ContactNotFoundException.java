package by.epam.training.user;

import by.epam.training.dao.DAOException;

public class ContactNotFoundException extends DAOException {

    public ContactNotFoundException() {
    }

    public ContactNotFoundException(String message) {
        super(message);
    }

    public ContactNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
