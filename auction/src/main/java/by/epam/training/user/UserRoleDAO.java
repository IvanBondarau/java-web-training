package by.epam.training.user;


import by.epam.training.dao.DAOException;
import by.epam.training.entity.UserRole;

import java.util.List;

public interface UserRoleDAO {
    UserRole getByName(String roleName) throws DAOException;
    List<UserRole> getByUserId(Long userId) throws DAOException;
    void assignToUser(Long userId, UserRole role) throws DAOException;
    void removeRoleFromUser(Long userId, UserRole role) throws DAOException;

    List<UserRole> getAll() throws DAOException;
}
