package by.epam.training.user;

import by.epam.training.entity.UserAccount;
import by.epam.training.entity.UserContact;
import by.epam.training.entity.UserRole;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Builder
public class UserDTO {

    private UserAccount userAccount;
    private UserContact userContact;

    private List<UserRole> roles;


}
