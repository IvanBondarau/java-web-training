package by.epam.training.user;

import by.epam.training.dao.DAOException;
import by.epam.training.entity.UserAccount;
import by.epam.training.entity.UserRole;
import lombok.Builder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface UserService {

    UserDTO signUpUser(String login, String email, String password) throws UserServiceException;

    Optional<UserDTO> logInUser(String login, String password) throws UserServiceException;

    void updateUserContact(UserDTO userDTO) throws UserServiceException;

    boolean checkLoginAvailable(String login) throws UserServiceException;

    boolean checkEmailAvailable(String login) throws UserServiceException;

    void removeUserById(Long id) throws UserServiceException;

    void promoteToAdmin(Long id) throws UserServiceException;

    void fillPurse(UserAccount user, BigDecimal value) throws UserServiceException;
    void blockMoney(UserAccount user, BigDecimal value) throws UserServiceException;
    void unlockMoney(UserAccount user, BigDecimal value) throws UserServiceException;

    List<UserDTO> getUserList() throws UserServiceException;


}
