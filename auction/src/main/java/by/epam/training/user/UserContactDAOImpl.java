package by.epam.training.user;

import by.epam.training.dao.ConnectionProvider;
import by.epam.training.dao.ConnectionProxy;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.UserContact;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class UserContactDAOImpl implements UserContactDAO {

    private static final Logger LOGGER = Logger.getLogger(UserContactDAOImpl.class);

    private static final String INSERT_USER_CONTACT =
            "INSERT INTO user_contact (phone, first_name, last_name, address, contact_email)" +
                    " VALUES (?, ?, ?, ?, ?)";

    private static final String SELECT_USER_CONTACT_BY_ID =
            "SELECT id, phone, first_name, last_name, address, contact_email FROM user_contact contact WHERE id = ?";

    private static final String DELETE_USER_BY_ID = "DELETE FROM user_contact WHERE id = ?";

    private static final String UPDATE_CONTACT =
            "UPDATE user_contact contact SET contact.phone=?, contact.first_name=?, contact.last_name=?," +
                    " contact.address = ?, contact.contact_email = ? WHERE contact.id = ?";

    private static final String SELECT_ALL_USER_CONTACTS = "SELECT id, phone, first_name, last_name, address, contact_email FROM user_contact";

    private ConnectionProvider connectionProvider;

    public UserContactDAOImpl(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    private UserContact buildUserContact(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String phone = resultSet.getString("phone");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");
        String address = resultSet.getString("address");
        String contactEmail = resultSet.getString("contact_email");

        return new UserContact(id, phone, firstName, lastName, address, contactEmail);
    }

    @Override
    public Long create(UserContact entity) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(INSERT_USER_CONTACT, Statement.RETURN_GENERATED_KEYS);
            int counter = 0;
            preparedStatement.setString(++counter, entity.getPhone());
            preparedStatement.setString(++counter, entity.getFirstName());
            preparedStatement.setString(++counter, entity.getLastName());
            preparedStatement.setString(++counter, entity.getAddress());
            preparedStatement.setString(++counter, entity.getContactEmail());

            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                entity.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("Create: id has not been generated");
            }
            LOGGER.info("Created user contact with id " + entity.getId().toString());
            return entity.getId();

        } catch (SQLException e) {
            throw new DAOException("SQLException occurred during creation of new user contact: " + e.getMessage(), e);
        }
    }

    @Override
    public UserContact read(Long id) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(SELECT_USER_CONTACT_BY_ID);

            int counter = 0;
            preparedStatement.setLong(++counter, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return this.buildUserContact(resultSet);
            } else {
                throw new ContactNotFoundException("UserContact with id " + id.toString() + " not found");
            }


        } catch (SQLException e) {
            throw new DAOException("SQLException occurred during reading of user contact: " + e.getMessage(), e);
        }
    }

    @Override
    public void update(UserContact entity) throws DAOException {
        try (ConnectionProxy connectionProxy = connectionProvider.getConnection()){
            PreparedStatement updateContactStatement = connectionProxy.get().prepareStatement(UPDATE_CONTACT);
            int counter = 0;
            updateContactStatement.setString(++counter, entity.getPhone());
            updateContactStatement.setString(++counter, entity.getFirstName());
            updateContactStatement.setString(++counter, entity.getLastName());
            updateContactStatement.setString(++counter, entity.getAddress());
            updateContactStatement.setString(++counter, entity.getContactEmail());
            updateContactStatement.setLong(++counter, entity.getId());

            updateContactStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("SQLException in update: " + e.getMessage(), e);
        }
    }

    @Override
    public void delete(Long id) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(DELETE_USER_BY_ID);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred during creation of new user contact: " + e.getMessage(), e);
        }
    }

    @Override
    public List<UserContact> findAll() throws DAOException {
        List<UserContact> contacts = new LinkedList<>();
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(SELECT_ALL_USER_CONTACTS);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                contacts.add(this.buildUserContact(resultSet));
            }

            return contacts;
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred during reading of user contact: " + e.getMessage(), e);
        }
    }


}
