package by.epam.training.user;

import by.epam.training.dao.ConnectionProvider;
import by.epam.training.dao.ConnectionProxy;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.UserRole;
import by.epam.training.user.UserRoleDAO;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class UserRoleDAOImpl implements UserRoleDAO {

    private static final Logger LOGGER = Logger.getLogger(UserRoleDAOImpl.class);

    private static final String GET_BY_ROLE_NAME = "select id, name from auction.user_role " +
            "where name = ?";
    private static final String GET_ALL_ROLES = "select id, name from user_role";
    private static final String GET_BY_USER_ID = "select role.id, role.name " +
            "from user_role role " +
            "join user_account_has_user_role t on role.id = t.user_role_id " +
            "where t.user_account_id = ?";
    private static final String ASSIGN_ROLE_TO_USER = "insert into user_account_has_user_role (user_account_id, user_role_id) " +
            "values (?, ?)";
    private static final String REMOVE_ROLE_FROM_USER = "DELETE FROM user_account_has_user_role WHERE user_account_id = ? AND user_role_id = ?";

    private ConnectionProvider connectionProvider;

    public UserRoleDAOImpl(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    private UserRole buildRole(ResultSet resultSet) throws SQLException {
        long roleId = resultSet.getLong("id");
        String roleName = resultSet.getString("name");
        return new UserRole(roleId, roleName);
    }

    @Override
    public UserRole getByName(String roleName) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()){
            PreparedStatement statement = connection.get().prepareStatement(GET_BY_ROLE_NAME);
            statement.setString(1, roleName);
            ResultSet set = statement.executeQuery();
            UserRole result = null;
            if (set.next()) {
                result = buildRole(set);
            } else {
                throw new UserRoleNotFoundException();
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while getting user roles: " + e.getMessage(), e);
        }
    }

    @Override
    public List<UserRole> getByUserId(Long userId) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()){
            PreparedStatement statement = connection.get().prepareStatement(GET_BY_USER_ID);
            statement.setLong(1, userId);
            ResultSet set = statement.executeQuery();
            List<UserRole> result = new LinkedList<>();
            while (set.next()) {
                result.add(buildRole(set));
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while getting user roles: " + e.getMessage(), e);
        }
    }

    @Override
    public void assignToUser(Long userId, UserRole role) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()){
            PreparedStatement statement = connection.get().prepareStatement(ASSIGN_ROLE_TO_USER);
            statement.setLong(1, userId);
            statement.setLong(2, role.getId());
            statement.executeUpdate();
            LOGGER.info("Role with id " + role.getId() + " assigned to user " + userId);
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while getting user roles: " + e.getMessage(), e);
        }
    }

    @Override
    public void removeRoleFromUser(Long userId, UserRole role) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()){
            PreparedStatement statement = connection.get().prepareStatement(REMOVE_ROLE_FROM_USER);
            statement.setLong(1, userId);
            statement.setLong(2, role.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while getting user roles: " + e.getMessage(), e);
        }
    }

    @Override
    public List<UserRole> getAll() throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()){
            PreparedStatement statement = connection.get().prepareStatement(GET_ALL_ROLES);
            ResultSet set = statement.executeQuery();
            List<UserRole> result = new LinkedList<>();
            while (set.next()) {
                result.add(buildRole(set));
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException("SQLException occurred while getting user roles: " + e.getMessage(), e);
        }
    }
}
