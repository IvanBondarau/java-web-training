package by.epam.training.user;

import by.epam.training.dao.ConnectionProvider;
import by.epam.training.dao.ConnectionProxy;
import by.epam.training.dao.DAOException;
import by.epam.training.entity.UserAccount;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class UserAccountDAOImpl implements UserAccountDAO {

    private static final Logger LOGGER = Logger.getLogger(UserAccountDAOImpl.class);
    private static final String INSERT_USER_ACCOUNT =
            "INSERT INTO user_account (login, email, password, purse, frozen_purse, user_contact_id) VALUES(?, ?, ?, ?, ?, ?)";
    private static final String READ_USER_ACCOUNT =
            "SELECT user.id, user.login, user.email, user.password, user.purse, user.frozen_purse, user.user_contact_id " +
                    "FROM user_account user WHERE user.id = ?";
    private static final String READ_ALL_USER_ACCOUNTS =
            "SELECT user.id, user.login, user.email, user.password, user.purse, user.frozen_purse, user.user_contact_id " +
                    "FROM user_account user";
    private static final String UPDATE_USER =
            "UPDATE user_account user " +
            "SET user.login=?, user.email=?, user.password=?, user.purse = ?, user.frozen_purse = ?, user_contact_id = ? " +
            "WHERE user.id = ?";

    private static final String DELETE_USER = "DELETE FROM user_account WHERE id = ?";
    private static final String SELECT_BY_LOGIN =
            "SELECT user.id, user.login, user.email, user.password, user.purse, user.frozen_purse, user.user_contact_id " +
                    "FROM user_account user WHERE user.login = ? AND user.password = ?";

    private static final String SELECT_ID = "SELECT user.id FROM auction.user_account user";
    private static final String COUNT_USERS_BY_LOGIN = "SELECT COUNT(login) AS total FROM user_account WHERE login = ?";
    private static final String COUNT_USERS_BY_EMAIL = "SELECT COUNT(email) AS total FROM user_account WHERE email = ?";
    private ConnectionProvider connectionProvider;

    public UserAccountDAOImpl(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    @Override
    public Long create(UserAccount entity) throws DAOException {

        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement accountStatement = connection.get().prepareStatement(INSERT_USER_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            accountStatement.setString(++i, entity.getLogin());
            accountStatement.setString(++i, entity.getEmail());
            accountStatement.setString(++i, entity.getPassword());
            accountStatement.setBigDecimal(++i, entity.getPurse());
            accountStatement.setBigDecimal(++i, entity.getFrozenPurse());
            accountStatement.setLong(++i, entity.getUserContactId());
            accountStatement.executeUpdate();
            ResultSet resultSet = accountStatement.getGeneratedKeys();
            if (resultSet.next()){
                entity.setId(resultSet.getLong(1));
            } else {
                throw new DAOException("Create: id has not been generated");
            }
            LOGGER.info("Created user account with id " + entity.getId().toString());
            return entity.getId();
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during inserting: " + e.getMessage(), e);
        }

    }

    @Override
    public UserAccount read(Long id) throws DAOException {


        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement accountStatement = connection.get().prepareStatement(READ_USER_ACCOUNT);
            accountStatement.setLong(1, id);
            ResultSet accountResultSet = accountStatement.executeQuery();

            UserAccount userAccount;
            if (accountResultSet.next()) {
                userAccount = this.buildAccount(accountResultSet);
                return userAccount;
            } else {
                throw new UserNotFoundException("User with id " + id + " not found");
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading by id: " + e.getMessage(), e);
        }
    }

    private UserAccount buildAccount(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String email = resultSet.getString("email");
        BigDecimal cash = resultSet.getBigDecimal("purse");
        BigDecimal blockedCash = resultSet.getBigDecimal("frozen_purse");
        Long userContactId = resultSet.getLong("user_contact_id");
        return new UserAccount(id, login, email, password, cash, blockedCash, userContactId);
    }

    @Override
    public List<UserAccount> findAll() throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            List<UserAccount> accounts = new ArrayList<>();
            try (PreparedStatement accountStatement = connection.get().prepareStatement(READ_ALL_USER_ACCOUNTS);
                 ResultSet accountResultSet = accountStatement.executeQuery()) {
                while (accountResultSet.next()) {
                    UserAccount userAccount = buildAccount(accountResultSet);
                    accounts.add(userAccount);
                }
            }
            return accounts;
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during reading user accounts: " + e.getMessage(), e);
        }
    }

    @Override
    public void update(UserAccount entity) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(UPDATE_USER);
            int counter = 0;
            preparedStatement.setString(++counter, entity.getLogin());
            preparedStatement.setString(++counter, entity.getEmail());
            preparedStatement.setString(++counter, entity.getPassword());
            preparedStatement.setBigDecimal(++counter, entity.getPurse());
            preparedStatement.setBigDecimal(++counter, entity.getFrozenPurse());
            preparedStatement.setLong(++counter, entity.getUserContactId());
            preparedStatement.setLong(++counter, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during deleting user: " + e.getMessage(), e);
        }
    }

    @Override
    public void delete(Long id) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement preparedStatement = connection.get().prepareStatement(DELETE_USER);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during deleting user: " + e.getMessage(), e);
        }
    }

    @Override
    public List<Long> getUserIdList() throws DAOException {

        List<Long> result = new ArrayList<>();

        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement statement = connection.get().prepareStatement(SELECT_ID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id= resultSet.getLong(1);
                result.add(id);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during selecting login: " + e.getMessage(), e);
        }
        return result;
    }


    @Override
    public UserAccount getByLoginPassword(String login, String password) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement accountStatement = connection.get().prepareStatement(SELECT_BY_LOGIN);
            int i = 0;
            accountStatement.setString(++i, login);
            accountStatement.setString(++i, password);
            ResultSet accountResultSet = accountStatement.executeQuery();
            if (accountResultSet.next()) {
                return this.buildAccount(accountResultSet);
            } else {
                throw new UserNotFoundException("User with login " + login + " not found");
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown during selecting user account by login: " + e.getMessage(), e);
        }
    }

    @Override
    public boolean isLoginAvailable(String login) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement accountStatement = connection.get().prepareStatement(COUNT_USERS_BY_LOGIN);
            int i = 0;
            accountStatement.setString(++i, login);
            ResultSet accountResultSet = accountStatement.executeQuery();
            if (accountResultSet.next()) {
                int total = accountResultSet.getInt("total");
                return total == 0;
            }
            return false;
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown : " + e.getMessage(), e);
        }
    }

    @Override
    public boolean isEmailAvailable(String email) throws DAOException {
        try (ConnectionProxy connection = connectionProvider.getConnection()) {
            PreparedStatement accountStatement = connection.get().prepareStatement(COUNT_USERS_BY_EMAIL);
            accountStatement.setString(1, email);
            ResultSet accountResultSet = accountStatement.executeQuery();
            if (accountResultSet.next()) {
                int total = accountResultSet.getInt("total");
                return total == 0;
            }
            return false;
        } catch (SQLException e) {
            throw new DAOException("SQLException was thrown: " + e.getMessage(), e);
        }
    }


}
