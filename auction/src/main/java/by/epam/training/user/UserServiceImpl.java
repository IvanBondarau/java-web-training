package by.epam.training.user;

import by.epam.training.ApplicationConstant;
import by.epam.training.bet.BetService;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.TransactionManager;
import by.epam.training.entity.UserAccount;
import by.epam.training.entity.UserContact;
import by.epam.training.entity.UserRole;
import by.epam.training.lot.LotService;
import by.epam.training.lot.LotServiceException;
import lombok.Builder;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    private UserAccountDAO userAccountDAO;
    private UserContactDAO userContactDAO;
    private UserRoleDAO userRoleDAO;

    @Setter
    private LotService lotService;

    private TransactionManager transactionManager;

    public UserServiceImpl(UserAccountDAO userAccountDAO, UserContactDAO userContactDAO, UserRoleDAO userRoleDAO,
                           TransactionManager transactionManager) {
        this.userAccountDAO = userAccountDAO;
        this.userContactDAO = userContactDAO;
        this.userRoleDAO = userRoleDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public UserDTO signUpUser(String login, String email, String password) throws UserServiceException {
        transactionManager.beginTransaction();
        try {
            List<UserRole> roles = new ArrayList<>();
            roles.add(userRoleDAO.getByName(ApplicationConstant.DEFAULT_USER_ROLE_NAME));
            UserContact userContact = new UserContact();
            Long contactID = userContactDAO.create(userContact);
            LOGGER.info("New UserContact created with id " + contactID);
            UserAccount userAccount = new UserAccount(login, email, password, contactID);
            Long accountID =  userAccountDAO.create(userAccount);
            LOGGER.info("New UserAccount created with id " + accountID);
            for (UserRole role: roles) {
                userRoleDAO.assignToUser(accountID, role);
            }
            transactionManager.commitTransaction();
            return new UserDTO(userAccount, userContact, roles);
        } catch (DAOException e) {
            transactionManager.rollbackTransaction();
            LOGGER.error("DAO exception in UserService: " + e.getMessage());
            throw new UserServiceException("DAO exception occurred during registering new user: " + e.getMessage(), e);
        }
    }

    @Override
    public Optional<UserDTO> logInUser(String login, String password) throws UserServiceException {
        try {
            UserAccount userAccount = userAccountDAO.getByLoginPassword(login, password);
            if (userAccount == null) {
                return Optional.empty();
            }
            List<UserRole> userRoles = userRoleDAO.getByUserId(userAccount.getId());
            UserContact userContact = userContactDAO.read(userAccount.getUserContactId());
            return Optional.of(new UserDTO(userAccount, userContact, userRoles));
        }
        catch (UserNotFoundException e) {
            LOGGER.warn("Invalid login/password");
            return Optional.empty();
        }
        catch (DAOException e) {
            LOGGER.error("DAO exception in UserService: " + e.getMessage());
            throw new UserServiceException("DAO exception occurred during signing in new user: " + e.getMessage(), e);
        }
    }

    @Override
    public boolean checkLoginAvailable(String login) throws UserServiceException {
        try {
            return userAccountDAO.isLoginAvailable(login);
        } catch (DAOException e) {
            LOGGER.error("DAO exception in UserService: " + e.getMessage());
            throw new UserServiceException("DAO exception occurred: " + e.getMessage(), e);
        }
    }

    @Override
    public boolean checkEmailAvailable(String email) throws UserServiceException {
        try {
            return userAccountDAO.isEmailAvailable(email);
        } catch (DAOException e) {
            LOGGER.error("DAO exception in UserService: " + e.getMessage());
            throw new UserServiceException("DAO exception occurred: " + e.getMessage(), e);
        }
    }

    @Override
    public void updateUserContact(UserDTO userDTO) throws UserServiceException {
        UserContact userContact = userDTO.getUserContact();
        try {
            LOGGER.info("Updating user contact: " + userContact);
            userContactDAO.update(userContact);
        } catch (DAOException e) {
            LOGGER.error("DAOException in update: " + e.getMessage());
            throw new UserServiceException(e);
        }
    }

    @Override
    public void removeUserById(Long id) throws UserServiceException {
        transactionManager.beginTransaction();
        try {
            lotService.clearUserLots(id);
            UserAccount account = userAccountDAO.read(id);
            UserContact contact = userContactDAO.read(account.getUserContactId());
            List<UserRole> roles = userRoleDAO.getByUserId(id);
            for (UserRole role: roles) {
                userRoleDAO.removeRoleFromUser(id, role);
            }
            userContactDAO.delete(contact.getId());
            userAccountDAO.delete(account.getId());
            transactionManager.commitTransaction();
        } catch (DAOException e) {
            transactionManager.rollbackTransaction();
            LOGGER.error("DAOException in remove by id: " + e.getMessage());
            throw new UserServiceException(e);
        } catch (LotServiceException e) {
            throw new UserServiceException(e);
        }
    }

    @Override
    public List<UserDTO> getUserList() throws UserServiceException {
        try {
            List<Long> idList = userAccountDAO.getUserIdList();
            List<UserDTO> result = new LinkedList<>();
            for (Long id: idList) {
                UserAccount account = userAccountDAO.read(id);
                UserContact contact = userContactDAO.read(account.getUserContactId());
                List<UserRole> roles = userRoleDAO.getByUserId(account.getId());
                result.add(new UserDTO(account, contact, roles));
            }
            LOGGER.info("User list created");
            return result;
        } catch (DAOException e) {
            LOGGER.error("DAOException in update: " + e.getMessage());
            throw new UserServiceException(e);
        }
    }

    @Override
    public void promoteToAdmin(Long userID) throws UserServiceException {
        try {
            userRoleDAO.assignToUser(userID, userRoleDAO.getByName(ApplicationConstant.ADMIN_USER_ROLE_NAME));
        } catch (DAOException e) {
            transactionManager.rollbackTransaction();
            LOGGER.error("DAOException in promoteToAdmin: " + e.getMessage());
            throw new UserServiceException(e);
        }
    }

    @Override
    public void fillPurse(UserAccount user, BigDecimal value) throws UserServiceException {
        user.setPurse(user.getPurse().add(value));
        try {
            userAccountDAO.update(user);
        } catch (DAOException e) {
            throw new UserServiceException(e);
        }
    }

    @Override
    public void blockMoney(UserAccount user, BigDecimal value) throws UserServiceException {
        user.setPurse(user.getPurse().subtract(value));
        user.setFrozenPurse(user.getFrozenPurse().add(value));
        try {
            userAccountDAO.update(user);
        } catch (DAOException e) {
            throw new UserServiceException(e);
        }
    }

    @Override
    public void unlockMoney(UserAccount user, BigDecimal value) throws UserServiceException {
        user.setPurse(user.getPurse().add(value));
        user.setFrozenPurse(user.getFrozenPurse().subtract(value));
        try {
            userAccountDAO.update(user);
        } catch (DAOException e) {
            throw new UserServiceException(e);
        }
    }


}
