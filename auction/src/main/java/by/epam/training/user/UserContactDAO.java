package by.epam.training.user;

import by.epam.training.dao.CRUDDao;
import by.epam.training.entity.UserContact;

public interface UserContactDAO extends CRUDDao<UserContact> {

}
