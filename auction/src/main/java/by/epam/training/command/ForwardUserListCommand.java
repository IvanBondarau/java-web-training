package by.epam.training.command;

import by.epam.training.user.UserDTO;
import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ForwardUserListCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(ForwardUserListCommand.class);

    private UserService userService;

    public ForwardUserListCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        LOGGER.info("Started");
        try {
            List<UserDTO> users = userService.getUserList();
            LOGGER.info("Loaded " + users.size() + " users");
            req.getSession().setAttribute("userList", userService.getUserList());
            req.getSession().setAttribute("viewName", "userListPage");
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
            LOGGER.info("Successfully finished");
        } catch (ServletException | IOException e) {
            LOGGER.error("Request dispatch error: " + e.getMessage());
            throw new ServletCommandException("Request dispatch error:" + e.getMessage(), e);
        } catch (UserServiceException e) {
            LOGGER.error("UserServiceException occurred: " + e.getMessage());
            throw new ServletCommandException(e);
        }
    }
}
