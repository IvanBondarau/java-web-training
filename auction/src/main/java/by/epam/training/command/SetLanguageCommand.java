package by.epam.training.command;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SetLanguageCommand implements ServletCommand {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        String language = req.getParameter("language");
        req.getSession().setAttribute("language", language);
        Cookie languageCookie = new Cookie("language", language);
        languageCookie.setMaxAge(7200);
        resp.addCookie(languageCookie);
        try {
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
        throw new ServletCommandException("Request dispatch error:" + e.getMessage(), e);
    }
    }
}
