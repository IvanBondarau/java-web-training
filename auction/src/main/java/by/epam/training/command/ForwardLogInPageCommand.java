package by.epam.training.command;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ForwardLogInPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(ForwardLogInPageCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            LOGGER.info("Forward to view logInUserPage");
            req.getSession().setAttribute("viewName", "logInUserPage");
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new ServletCommandException("Request dispatch error: " + e.getMessage(), e);
        }
    }
}
