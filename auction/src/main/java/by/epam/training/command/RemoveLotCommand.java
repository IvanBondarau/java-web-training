package by.epam.training.command;

import by.epam.training.entity.Lot;
import by.epam.training.lot.LotDTO;
import by.epam.training.lot.LotService;
import by.epam.training.lot.LotServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class RemoveLotCommand implements ServletCommand{

    private static final Logger LOGGER = Logger.getLogger(RemoveLotCommand.class);

    private LotService lotService;

    public RemoveLotCommand(LotService lotService) {
        this.lotService = lotService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            Long lotId = Long.parseLong(req.getParameter("lotId"));
            LOGGER.info("Remove lot by id " + lotId);
            lotService.removeLotById(lotId);
            List<LotDTO> lots = lotService.getAllLots();
            req.getSession().setAttribute("lotList", lots);
            resp.sendRedirect(req.getContextPath() + "/jsp/layout.jsp");
        } catch (LotServiceException | IOException e) {
            LOGGER.error("Error while removing lot by id");
            throw new ServletCommandException(e);
        }

    }
}
