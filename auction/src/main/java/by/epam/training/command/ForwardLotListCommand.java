package by.epam.training.command;

import by.epam.training.lot.LotDTO;
import by.epam.training.lot.LotService;
import by.epam.training.lot.LotServiceException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ForwardLotListCommand implements ServletCommand {


    private static final Logger LOGGER = Logger.getLogger(ForwardUserListCommand.class);

    private LotService lotService;

    public ForwardLotListCommand(LotService lotService) {
        this.lotService = lotService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            List<LotDTO> lots = lotService.getAllLots();
            LOGGER.info("Loaded " + lots.size() + " lots");
            req.getSession().setAttribute("lotList", lots);
            req.getSession().setAttribute("viewName", "lotListPage");
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            LOGGER.error("Request dispatch error: " + e.getMessage());
            throw new ServletCommandException("Request dispatch error:" + e.getMessage(), e);
        } catch (LotServiceException e) {
            LOGGER.error("LotServiceException occurred: " + e.getMessage());
            throw new ServletCommandException(e);
        }
    }
}
