package by.epam.training.command;

import by.epam.training.entity.UserAccount;
import by.epam.training.user.UserDTO;
import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

public class FillPurseCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(FillPurseCommand.class);

    private UserService userService;

    public FillPurseCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            BigDecimal value = new BigDecimal(req.getParameter("value"));
            UserAccount userAccount = ((UserDTO)req.getSession().getAttribute("user")).getUserAccount();
            LOGGER.info("value = " + value + ", userAccount = " +  userAccount);
            userService.fillPurse(userAccount, value);
            resp.sendRedirect(req.getContextPath() + "/jsp/layout.jsp");
        } catch (IOException e) {
            LOGGER.error("Request dispatch error: " + e.getMessage());
            throw new ServletCommandException(e);
        } catch (UserServiceException e) {
            LOGGER.error("UserServiceException occurred: " + e.getMessage());
            throw new ServletCommandException("UserServiceException occurred: " + e.getMessage(), e);
        }
    }
}
