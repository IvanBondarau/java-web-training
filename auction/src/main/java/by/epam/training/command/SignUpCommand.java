package by.epam.training.command;

import by.epam.training.ApplicationConstant;
import by.epam.training.entity.UserRole;
import by.epam.training.security.CryptographyManager;
import by.epam.training.user.UserDTO;
import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;
import by.epam.training.validation.*;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SignUpCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(SignUpCommand.class);

    private UserService userService;
    private RequestValidator dataValidator;
    private CryptographyManager cryptographyManager;

    public SignUpCommand(UserService userService, RequestValidator requestValidator, CryptographyManager cryptographyManager) {
        this.userService = userService;
        this.dataValidator = requestValidator;
        this.cryptographyManager = cryptographyManager;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        ValidationResult result;
        try {
            result = dataValidator.validate(req);
        } catch (ValidatorException e) {
            throw new ServletCommandException("Validator exception: " + e.getMessage(), e);
        }
        if (!result.isValid()) {
            ValidationError firstError = result.getErrors().get(0);
            LOGGER.error("Unable to create new account" );
            LOGGER.error(result);
            req.setAttribute("error", firstError);

            String login = req.getParameter("login");
            String email = req.getParameter("email");

            req.setAttribute("login", login);
            req.setAttribute("email", email);

            req.getSession().setAttribute("viewName", "signUpPage");
            try {
                req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
            } catch (ServletException | IOException e) {
                throw new ServletCommandException("Request Dispatcher error: " + e.getMessage(), e);
            }
            return;
        }

        LOGGER.info("Creating new user account");

        String login = req.getParameter("login");
        String email = req.getParameter("email");
        String password = cryptographyManager.encrypt(req.getParameter("password"));

        try {
            UserDTO userDTO = userService.signUpUser(login, email, password);
            req.getSession().setAttribute("user", userDTO);
            req.getSession().setAttribute("viewName", "userPage");
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (UserServiceException e) {
            throw new ServletCommandException("UserServiceException occurred", e);
        } catch (ServletException | IOException e) {
            throw new ServletCommandException("Request Dispatcher error: " + e.getMessage(), e);
        }
    }
}
