package by.epam.training.command;

import by.epam.training.bet.BetDTO;
import by.epam.training.bet.BetService;
import by.epam.training.bet.BetServiceException;
import by.epam.training.entity.Lot;
import by.epam.training.entity.UserAccount;
import by.epam.training.lot.LotDTO;
import by.epam.training.lot.LotService;
import by.epam.training.lot.LotServiceException;
import by.epam.training.user.UserDTO;
import by.epam.training.user.UserService;
import by.epam.training.validation.PlaceBetDataValidator;
import by.epam.training.validation.ValidationError;
import by.epam.training.validation.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class PlaceBetCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(PlaceBetCommand.class);

    private BetService betService;
    private LotService lotService;
    private PlaceBetDataValidator placeBetDataValidator;

    public PlaceBetCommand(BetService betService, LotService lotService) {
        this.betService = betService;
        this.lotService = lotService;
        this.placeBetDataValidator = new PlaceBetDataValidator();
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            ValidationResult result = placeBetDataValidator.validate(req);
            if (!result.isValid()) {
                ValidationError firstError = result.getErrors().get(0);
                LOGGER.error("Unable to place bet: " + firstError.getSubject() + " " + firstError.getError());
                LOGGER.error(result);
                req.setAttribute("error", firstError);
                req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
                return;
            }


            UserAccount userAccount = ((UserDTO) req.getSession().getAttribute("user")).getUserAccount();
            Lot lot = ((LotDTO) req.getSession().getAttribute("lot")).getLot();
            BigDecimal value = new BigDecimal(req.getParameter("value"));

            betService.placeBet(userAccount, lot, value);

            LOGGER.info("placed bet: value = " + value + ", lotId = " + lot.getId() + ", userId = " + userAccount.getId());

            List<BetDTO> betDTOList = betService.getByLotId(lot.getId());
            req.getSession().setAttribute("betList", betDTOList);
            req.getSession().setAttribute("viewName", "viewLotPage");
            resp.sendRedirect(req.getContextPath() + "/jsp/layout.jsp");
        } catch (IOException | ServletException e) {
            throw new ServletCommandException("Request dispatch error:" + e.getMessage(), e);
        } catch (BetServiceException e) {
            throw new ServletCommandException(e.getMessage(), e);
        }
    }

}
