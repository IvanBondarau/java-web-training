package by.epam.training.command;

import by.epam.training.user.UserDTO;
import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class RemoveUserCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(RemoveUserCommand.class);

    private UserService userService;

    public RemoveUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            Long userId = Long.parseLong(req.getParameter("userId"));
            userService.removeUserById(userId);

            LOGGER.info("Removed user with id " + userId + " to admin");
            List<UserDTO> users = userService.getUserList();
            LOGGER.info("Loaded " + users.size() + " users");
            req.getSession().setAttribute("userList", userService.getUserList());
            req.getSession().setAttribute("viewName", "userListPage");
            resp.sendRedirect(req.getContextPath() + "/jsp/layout.jsp");
        } catch (IOException e) {
            throw new ServletCommandException("Request dispatch error: " + e.getMessage(), e);
        } catch (UserServiceException e) {
            throw new ServletCommandException("UserServiceException occurred: " + e.getMessage(), e);
        }
    }
}
