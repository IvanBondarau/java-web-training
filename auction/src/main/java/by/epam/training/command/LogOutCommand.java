package by.epam.training.command;

import by.epam.training.user.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogOutCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(LogOutCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            UserDTO user = (UserDTO)req.getSession().getAttribute("user");
            LOGGER.info(user.getUserAccount() + " log out");
            req.getSession().removeAttribute("user");
            req.getSession().removeAttribute("viewName");
            resp.sendRedirect(req.getContextPath() + "/jsp/layout.jsp");
        } catch (IOException e) {
            throw new ServletCommandException("Request dispatch error: " + e.getMessage(), e);
        }
    }
}
