package by.epam.training.command;

public class ServletCommandException extends Exception {

    public ServletCommandException() {
    }

    public ServletCommandException(String message) {
        super(message);
    }

    public ServletCommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServletCommandException(Throwable cause) {
        super(cause);
    }
}
