package by.epam.training.command;

import by.epam.training.entity.Lot;
import by.epam.training.entity.UserAccount;
import by.epam.training.lot.LotDTO;
import by.epam.training.lot.LotService;
import by.epam.training.lot.LotServiceException;
import by.epam.training.user.UserDTO;
import by.epam.training.validation.CreateLotDataValidator;
import by.epam.training.validation.ValidationError;
import by.epam.training.validation.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateLotCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(CreateLotCommand.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private LotService lotService;
    private CreateLotDataValidator createLotDataValidator;

    public CreateLotCommand(LotService lotService) {
        this.lotService = lotService;
        this.createLotDataValidator = new CreateLotDataValidator();

    }


    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            ValidationResult result = createLotDataValidator.validate(req);
            if (!result.isValid()) {
                ValidationError firstError = result.getErrors().get(0);
                LOGGER.error("Unable to create lot: " + firstError.getSubject() + " " + firstError.getError());
                LOGGER.error(result);
                req.setAttribute("error", firstError);
                req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
                return;
            }
        } catch (IOException | ServletException e) {
            LOGGER.error(e);
            throw new ServletCommandException("Unable to dispatch request: " + e.getMessage(), e);
        }

        UserAccount userAccount = ((UserDTO) req.getSession().getAttribute("user")).getUserAccount();
        LOGGER.info(userAccount);
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        Timestamp start;
        Timestamp end;
        try {

            Date startDate = dateFormat.parse(req.getParameter("start"));
            Date endDate = dateFormat.parse(req.getParameter("end"));
            start = new Timestamp(startDate.getTime());
            end = new Timestamp(endDate.getTime());

        } catch (ParseException e) {
            throw new ServletCommandException("Invalid date format: " + e.getMessage(), e);
        }

        BigDecimal startingPrice = new BigDecimal(req.getParameter("startingPrice"));
        BigDecimal step = new BigDecimal(req.getParameter("step"));

        try {
            LotDTO lot = lotService.createLot(userAccount, name, description, start, end, startingPrice, step);
            LOGGER.info("Created new lot " + lot.getLot());
            req.getSession().setAttribute("viewName", "lotListPage");
            req.getSession().setAttribute("lotList", lotService.getAllLots());
            resp.sendRedirect(req.getContextPath() + "/jsp/layout.jsp");
        } catch (LotServiceException e) {
            throw new ServletCommandException("LotServiceException occurred", e);
        } catch (IOException e) {
            throw new ServletCommandException("Request Dispatcher error: " + e.getMessage(), e);
        }
    }

}
