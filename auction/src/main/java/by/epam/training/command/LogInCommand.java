package by.epam.training.command;

import by.epam.training.security.CryptographyManager;
import by.epam.training.user.UserDTO;
import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class LogInCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(LogInCommand.class);

    private UserService userService;
    private CryptographyManager cryptographyManager;

    public LogInCommand(UserService userService, CryptographyManager cryptographyManager) {
        this.userService = userService;
        this.cryptographyManager = cryptographyManager;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        String login = req.getParameter("login");
        String password = cryptographyManager.encrypt(req.getParameter("password"));
        try {
            Optional<UserDTO> userDTO = userService.logInUser(login, password);
            if (userDTO.isPresent()) {
                UserDTO user = userDTO.get();
                LOGGER.info("User " + user.getUserAccount() + " has been loaded");
                req.getSession().setAttribute("user", userDTO.get());
                req.getSession().setAttribute("viewName", "userPage");
                resp.sendRedirect(req.getContextPath() + "/jsp/layout.jsp");
            } else {
                LOGGER.info("User not found");
                req.setAttribute("error", "error.login.user_not_found");
                req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
            }
        } catch (UserServiceException e) {
            throw new ServletCommandException("UserServiceException occurred in LogInCommand: " + e.getMessage(), e);
        } catch (IOException | ServletException e) {
            LOGGER.error(e);
            throw new ServletCommandException(e);
        }
    }
}
