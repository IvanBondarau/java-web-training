package by.epam.training.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CommandProviderImpl implements CommandProvider {
    private Map<String, ServletCommand> commands = new HashMap<>();

    @Override
    public void registerCommand(String commandName, ServletCommand command) {
        commands.put(commandName, command);
    }

    @Override
    public Optional<ServletCommand> getCommand(String commandName) {
        return Optional.ofNullable(commands.get(commandName));
    }
}
