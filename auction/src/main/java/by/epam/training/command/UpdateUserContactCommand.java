package by.epam.training.command;

import by.epam.training.user.UserDTO;
import by.epam.training.user.UserService;
import by.epam.training.user.UserServiceException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateUserContactCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(UpdateUserContactCommand.class);

    private UserService userService;

    public UpdateUserContactCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            UserDTO user = (UserDTO)req.getSession().getAttribute("user");
            if (user == null) {
                throw new ServletCommandException("User not found");
            }
            String phone = req.getParameter("phone");
            String firstName = req.getParameter("firstName");
            String lastName = req.getParameter("lastName");
            String address = req.getParameter("address");
            String contactEmail = req.getParameter("contactEmail");
            user.getUserContact().setPhone(phone);
            user.getUserContact().setFirstName(firstName);
            user.getUserContact().setLastName(lastName);
            user.getUserContact().setAddress(address);
            user.getUserContact().setContactEmail(contactEmail);
            userService.updateUserContact(user);
            LOGGER.info("Updated user contact " + user.getUserContact());
            resp.sendRedirect(req.getContextPath() + "/jsp/layout.jsp");
        } catch (UserServiceException e) {
            LOGGER.error("UserServiceException occurred: " +  e.getMessage());
            throw new ServletCommandException(e);
        } catch (IOException e) {
            throw new ServletCommandException("Request Dispatcher error: " + e.getMessage(), e);
        }
    }
}
