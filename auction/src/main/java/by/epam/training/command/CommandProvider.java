package by.epam.training.command;

import java.util.Optional;

public interface CommandProvider {
    Optional<ServletCommand> getCommand(String commandName);
    void registerCommand(String commandName, ServletCommand command);
}
