package by.epam.training.command;

import by.epam.training.bet.BetDTO;
import by.epam.training.bet.BetService;
import by.epam.training.bet.BetServiceException;
import by.epam.training.lot.LotDTO;
import by.epam.training.lot.LotService;
import by.epam.training.lot.LotServiceException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ForwardLotPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(ForwardLotPageCommand.class);

    private LotService lotService;
    private BetService betService;

    public ForwardLotPageCommand(LotService lotService, BetService betService) {
        this.lotService = lotService;
        this.betService = betService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        LOGGER.info("Started");
        try {
            Long id = Long.parseLong(req.getParameter("lotId"));
            LOGGER.info("id = " + id);
            LotDTO lot = lotService.getLotById(id);
            LOGGER.info("Loaded lot: " + lot);
            req.getSession().setAttribute("lot", lot);
            List<BetDTO> bets = betService.getByLotId(lot.getLot().getId());
            LOGGER.info("Loaded " + bets.size() + " bets");
            req.getSession().setAttribute("betList", bets);
            req.getSession().setAttribute("viewName", "viewLotPage");
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
            LOGGER.info("Successfully finished");
        } catch (ServletException | IOException e) {
            throw new ServletCommandException("Request dispatch error: " + e.getMessage(), e);
        } catch (LotServiceException e) {
            throw new ServletCommandException("LotServiceException occurred: " + e.getMessage(), e);
        } catch (BetServiceException e) {
            throw new ServletCommandException("BetServiceException occurred: " + e.getMessage(), e);
        }
    }
}
