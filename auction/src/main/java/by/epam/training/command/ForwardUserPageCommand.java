package by.epam.training.command;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ForwardUserPageCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(ForwardUserPageCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            LOGGER.info("Forward to view userPage");
            req.getSession().setAttribute("viewName", "userPage");
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            throw new ServletCommandException("Request dispatch error: " + e.getMessage(), e);
        }
    }
}
