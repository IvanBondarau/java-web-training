package by.epam.training.command;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ForwardCommand implements ServletCommand {

    private static final Logger LOGGER = Logger.getLogger(ForwardCommand.class);

    private String viewName;

    public ForwardCommand(String viewName) {
        this.viewName = viewName;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletCommandException {
        try {
            req.getSession().setAttribute("viewName", viewName);
            LOGGER.info("Forward to view " + viewName);
            req.getRequestDispatcher("/jsp/layout.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            LOGGER.error("Request dispatch error: " + e.getMessage());
            throw new ServletCommandException("Request dispatch error:" + e.getMessage(), e);
        }
    }
}
