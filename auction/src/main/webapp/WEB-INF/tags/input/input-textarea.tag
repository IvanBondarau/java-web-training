<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="label" required="true" %>
<%@ attribute name="param_name" required="true" %>
<%@ attribute name="placeholder" required="false" %>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--full-width">
    <textarea class="mdl-textfield__input " type="text" id="${pageScope.param_name}-input" name="${pageScope.param_name}"
    rows="5"
    <c:if test="${not empty placeholder}">
           placeholder="<fmt:message key="${placeholder}"/>"
    </c:if>
    ><c:if test="${not empty param[pageScope.param_name]}">${param[pageScope.param_name]}</c:if></textarea>
    <label class="mdl-textfield__label" for="${pageScope.param_name}-input">
        <fmt:message key="${label}"/>
    </label>
</div>
