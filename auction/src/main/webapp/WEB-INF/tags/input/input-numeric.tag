<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="label" required="true" %>
<%@ attribute name="param_name" required="true" %>
<%@ attribute name="placeholder" required="false" %>
<%@ attribute name="error" required="false" %>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input " type="text" id="${pageScope.param_name}-input" name="${pageScope.param_name}"
    pattern="[1-9](\d{0,8})([.]\d{0,8})?"
    <c:if test="${not empty placeholder}">
           placeholder="<fmt:message key="${placeholder}"/>"
    </c:if>

    <c:if test="${not empty param[pageScope.param_name]}">
           value="${param[pageScope.param_name]}"
    </c:if>
    >
    <label class="mdl-textfield__label" for="${pageScope.param_name}-input">
        <fmt:message key="${label}"/>
    </label>
    <c:if test="${not empty error}">
        <span class="mdl-textfield__error">
            <fmt:message key="${error}"/>
        </span>
    </c:if>
</div>
