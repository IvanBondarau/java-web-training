<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="label" required="true" %>
<%@ attribute name="param_name" required="true" %>
<%@ attribute name="placeholder" required="false" %>
<%@ attribute name="error" required="false" %>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="datetime-local" id="${pageScope.param_name}" name="${pageScope.param_name}"
           pattern="([0-2][0-9]|(3)[0-1])([.])(((0)[0-9])|((1)[0-2]))([.])\d{4} ([0-1]?\d|2[0-3])(?::([0-5]?\d))?"
    <c:if test="${not empty placeholder}">
        placeholder="<fmt:message key="${placeholder}"/>"
    </c:if>
    <c:if test="${not empty param[pageScope.param_name]}">
           value="${param[pageScope.param_name]}"
    </c:if>
    >
    <label class="mdl-textfield__label" for="${pageScope.param_name}">
        <fmt:message key="${label}"/>
    </label>
    <c:if test="${not empty error}">
        <span class="mdl-textfield__error">${error}</span>
    </c:if>
</div>
