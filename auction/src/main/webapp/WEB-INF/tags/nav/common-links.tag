<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag import="by.epam.training.ApplicationConstant" %>
<nav class="mdl-navigation">
    <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_HOME_PAGE}">
        <fmt:message key="layout.home_page"/>
    </a>
    <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_USER_LIST}">
        <fmt:message key="layout.users"/>
    </a>
    <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_LOT_LIST}">
        <fmt:message key="layout.lots"/>
    </a>
</nav>