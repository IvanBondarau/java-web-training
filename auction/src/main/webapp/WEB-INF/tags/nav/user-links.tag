<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag import="by.epam.training.ApplicationConstant" %>
<nav class="mdl-navigation">
    <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_CREATE_LOT_PAGE}">
        <fmt:message key="layout.button.createLot"/>
    </a>
    <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_USER_PAGE}">
        <fmt:message key="layout.account"/>
    </a>
    <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=${ApplicationConstant.LOG_OUT_COMMAND_NAME}">
        <fmt:message key="layout.logOut"/>
    </a>
</nav>
