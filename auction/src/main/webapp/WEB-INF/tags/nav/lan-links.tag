<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag import="by.epam.training.ApplicationConstant" pageEncoding="UTF-8" %>
<ul class="mdl-mini-footer__link-list">
    <li><a href="${pageContext.request.contextPath}?command=${ApplicationConstant.SET_LANGUAGE_COMMAND}&language=ru-RU">Русский</a></li>
    <li><a href="${pageContext.request.contextPath}?command=${ApplicationConstant.SET_LANGUAGE_COMMAND}&language=en-US">English</a></li>
    <li><a href="${pageContext.request.contextPath}?command=${ApplicationConstant.SET_LANGUAGE_COMMAND}&language=by-BE">Беларуская</a></li>
</ul>