<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag import="by.epam.training.ApplicationConstant" %>
<nav class="mdl-navigation">
    <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_SIGN_UP_FORM}">
        <fmt:message key="layout.signUp"/>
    </a>
    <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_LOG_IN_FORM}">
        <fmt:message key="layout.logIn"/>
    </a>
</nav>