<%@ page contentType="text/html;charset=UTF-8" language="java" import="by.epam.training.time.TimeService" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>


<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="layout" scope="session"/>

<!DOCTYPE html>
<html>
    <meta charset="utf-8"/>
<head>
    <title>Online auction</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/style/material.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/style/styles.css">
    <script defer src="${pageContext.request.contextPath}/static/style/material.min.js"></script>
</head>

<body>
<div class="demo-layout-waterfall mdl-layout mdl-js-layout">
    <header class="mdl-layout__header mdl-layout__header--waterfall">

        <div class="mdl-layout__header-row">

            <span class="mdl-layout-title">

                <c:choose>
                    <c:when test="${not empty sessionScope.user}">
                        ${sessionScope.user.userAccount.login}, ${sessionScope.user.userAccount.purse} BYN
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="layout.user"/>
                    </c:otherwise>
                </c:choose>
            </span>
            <div class="mdl-layout-spacer"></div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--align-right">
                ${TimeService.getCurrentDateTimeStr()}
            </div>
        </div>

        <div class="mdl-layout__header-row">
            <nav:common-links/>
            <div class="mdl-layout-spacer"></div>
            <c:choose>
                <c:when test="${empty sessionScope.user}">
                    <nav:nav-links />
                </c:when>
                <c:otherwise>
                    <nav:user-links />
                </c:otherwise>
            </c:choose>

        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">
            <fmt:message key="layout.navigation"/>
        </span>
        <c:choose>
            <c:when test="${empty sessionScope.user}">
                <nav:nav-links />
            </c:when>
            <c:otherwise>
                <nav:user-links />
            </c:otherwise>
        </c:choose>
        <nav:common-links/>
    </div>
    <main class="mdl-layout__content">
        <div class="page-content">
            <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--2-col">
                </div>
                <div class="mdl-cell mdl-cell--8-col">
                    <c:choose>
                        <c:when test="${not empty sessionScope.viewName}">
                            <jsp:include page="/jsp/views/${sessionScope.viewName}.jsp" />
                        </c:when>
                        <c:otherwise>
                            <jsp:include page="/jsp/views/homePage.jsp"/>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="mdl-cell mdl-cell--2-col">
                </div>
            </div>
        </div>
    </main>
    <footer class = "mdl-mini-footer">
        <div class = "mdl-mini-footer__left-section">
            <nav:lan-links/>
        </div>

    </footer>
</div>
</body>

</html>