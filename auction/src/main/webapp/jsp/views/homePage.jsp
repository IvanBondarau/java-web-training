<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="by.epam.training.ApplicationConstant" %>
<div class="mdl-grid">
    <c:choose>
        <c:when test="${empty sessionScope.user}">
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col">
                <h3>
                    <fmt:message key="layout.welcome"/>
                </h3>
            </div>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col">
                <h5>
                    <a href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_SIGN_UP_FORM}">
                        <fmt:message key="layout.signUp"/>
                    </a>
                </h5>
            </div>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col">
                <h5>
                    <a href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_LOG_IN_FORM}">
                        <fmt:message key="layout.logIn"/>
                    </a>
                </h5>
            </div>
            <div class="mdl-cell--4-col"></div>
        </c:when>
        <c:otherwise>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col">
                <h3>
                    <fmt:message key="layout.welcome"/>
                </h3>
            </div>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col">
                <h5>
                    <a href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_USER_LIST}">
                        <fmt:message key="layout.users"/>
                    </a>
                </h5>
            </div>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col">
                <h5>
                    <a href="${pageContext.request.contextPath}?command=${ApplicationConstant.FORWARD_LOT_LIST}">
                        <fmt:message key="layout.lots"/>
                    </a>
                </h5>
            </div>
            <div class="mdl-cell--4-col"></div>

        </c:otherwise>
    </c:choose>
</div>