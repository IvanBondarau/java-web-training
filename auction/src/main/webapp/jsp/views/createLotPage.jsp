<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="input" tagdir="/WEB-INF/tags/input"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<form action="${pageContext.request.contextPath}/" method="POST">
    <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--8-col">
            <h3>
                <fmt:message key="layout.button.createLot"/>
            </h3>
        </div>
        <div class="mdl-cell mdl-cell--2-col"></div>
        <input type="hidden" name="command" value="createLot">
        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--8-col">
            <input:input-text label="lot.name" param_name="name"/>
        </div>
        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--8-col">
            <input:input-textarea label="lot.description" param_name="description"/>
        </div>
        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--4-col">
            <input:input-datetime label="lot.start" param_name="start"  error="13.11.2020 12:49"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col">
            <input:input-datetime label="lot.end" param_name="end" error="13.11.2020 12:49"/>
        </div>
        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--4-col">
            <input:input-numeric label="lot.startingPrice" param_name="startingPrice"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col">
            <input:input-numeric label="lot.step" param_name="step"/>
        </div>
        <div class="mdl-cell mdl-cell--2-col"></div>


        <c:if test="${not empty requestScope.error}">
            <div class="mdl-cell mdl-cell--2-col"></div>
            <div class="mdl-cell mdl-cell--8-col">
                <span class="mdl-color-text--red">
                    <fmt:message key="${requestScope.error}"/>
                </span>
            </div>
            <div class="mdl-cell mdl-cell--2-col"></div>
        </c:if>

        <div class="mdl-cell mdl-cell--2-col"></div>
        <div class="mdl-cell mdl-cell--8-col">
            <input class="mdl-button mdl-js-button mdl-button--raised" type="submit"
                   value="<fmt:message key="layout.button.createLot"/>" />
        </div>
        <div class="mdl-cell mdl-cell--2-col"></div>
    </div>
</form>
