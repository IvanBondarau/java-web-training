<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="in" tagdir="/WEB-INF/tags/input" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="by.epam.training.ApplicationConstant" %>
<div class="mdl-grid">
    <div class="mdl-cell--4-col"></div>
    <div class="mdl-cell--4-col">
        <h3>
        ${sessionScope.lot.lot.name}
        </h3>
    </div>
    <div class="mdl-cell--4-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--6-col">
        <h5>
        <fmt:message key="layout.lot.owner"/>
        </h5>
        <span>${sessionScope.lot.owner.login}</span>

    </div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--6-col">
        <h5>
        <fmt:message key="layout.lot.description"/>
        </h5>
    </div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--6-col">
        <span>
            ${sessionScope.lot.lot.description}
        </span>
    </div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--2-col">
        <h5>
        <fmt:message key="layout.lot.startDate"/>
        </h5>
    </div>
    <div class="mdl-cell--2-col">
        <h5>
        <fmt:message key="layout.lot.endDate"/>
        </h5>
    </div>
    <div class="mdl-cell--5-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--2-col">
        <span>
        ${sessionScope.lot.lot.start}
        </span>
    </div>
    <div class="mdl-cell--2-col">
        <span>
        ${sessionScope.lot.lot.end}
        </span>
    </div>
    <div class="mdl-cell--5-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--2-col">
        <h5>
            <fmt:message key="layout.lot.startingPrice"/>
        </h5>
    </div>
    <div class="mdl-cell--2-col">
        <h5>
            <fmt:message key="layout.lot.currentPrice"/>
        </h5>
    </div>
    <div class="mdl-cell--2-col">
        <h5>
            <fmt:message key="lot.step"/>
        </h5>
    </div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--2-col">
        <span>
            ${sessionScope.lot.lot.startingPrice} BYN
        </span>
    </div>
    <div class="mdl-cell--2-col">
        <span>
            <c:choose>
                <c:when test="${not empty sessionScope.lot.lot.currentPrice}">
                    ${sessionScope.lot.lot.currentPrice} BYN
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </span>
    </div>
    <div class="mdl-cell--2-col">
        <span>
        ${sessionScope.lot.lot.step} BYN
        </span>
    </div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--6-col">
        <h5>
        <fmt:message key="layout.lot.status"/>
        </h5>
        <span>
        <fmt:message key="${sessionScope.lot.lotStatus.name}"/>
        </span>
    </div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--4-col">
    <c:choose>
        <c:when test="${(not empty sessionScope.user)
            and (sessionScope.lot.lotStatus.name.equals(ApplicationConstant.IN_PROGRESS_STATUS))
            and (not sessionScope.user.userAccount.equals(sessionScope.lot.owner))}">
            <form action="${pageContext.request.contextPath}/" method="POST">
                <ul class="mdl-list">
                    <li class="mdl-list__item">
                    <input type="hidden" name="command" value="placeBet">
                    <in:input-numeric label="layout.input.betValue" param_name="value"/>
                    </li>
                    <li class="mdl-list__item">
                    <input type="submit"
                           class="mdl-button mdl-js-button mdl-button--raised"
                                    value="<fmt:message key="layout.button.makeBet"/>">
                    </li>
                </ul>
            </form>
        </c:when>
    </c:choose>
    </div>
    <div class="mdl-cell--2-col">
        <span class="mdl-color-text--red">
            <c:if test="${not empty requestScope.error}">
                <fmt:message key="${requestScope.error}"/>
            </c:if>
        </span>
    </div>
    <div class="mdl-cell--3-col"></div>

    <div class="mdl-cell--3-col"></div>
    <div class="mdl-cell--6-col">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead>
        <tr>
            <th class="mdl-data-table__cell--non-numeric">
                <fmt:message key="layout.bet.creator"/>
            </th>
            <th class="mdl-data-table__cell--non-numeric">
                <fmt:message key="layout.bet.date"/>
            </th>
            <th class="mdl-data-table__cell--non-numeric">
                <fmt:message key="layout.bet.value"/>
            </th>
        </tr>
        </thead>
        <tbody>
            <c:forEach items="${sessionScope.betList}" var="betDTO">
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">${betDTO.creator.login}</td>
                    <td class="mdl-data-table__cell--non-numeric">${betDTO.bet.date}</td>
                    <td class="mdl-data-table__cell">
                            ${betDTO.bet.value}
                    </td>
                </tr>

            </c:forEach>
        </tbody>
    </table>
    </div>
    <div class="mdl-cell--3-col"></div>
</div>
