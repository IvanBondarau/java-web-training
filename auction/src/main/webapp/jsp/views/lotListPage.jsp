<%@ page contentType="text/html;charset=UTF-8"  language="java" import="by.epam.training.ApplicationConstant" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty sessionScope.user}">
    <c:forEach var="role" items="${sessionScope.user.roles}">
        <c:if test="${role.name.equals(ApplicationConstant.ADMIN_USER_ROLE_NAME)}">
            <c:set var="isAdmin" value="${true}"/>
        </c:if>
    </c:forEach>
</c:if>
<div class="mdl-grid">
    <div class="mdl-cell--2-col"></div>
    <div class="mdl-cell--1-col"></div>
    <div class="mdl-cell--4-col">
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
            <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.lot.owner"/>
                </th>
                <th class="mdl-data-table_post_cell--non-numeric">
                    <fmt:message key="layout.lot.name"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.lot.status"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.lot.currentPrice"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.actions"/>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${sessionScope.lotList}" var="lot">
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">${lot.owner.login}</td>
                    <td class="mdl-data-table__cell--non-numeric">${lot.lot.name}</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <fmt:message key="${lot.lotStatus.name}"/>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <c:choose>
                        <c:when test="${not empty lot.lot.currentPrice}">
                            ${lot.lot.currentPrice} BYN
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a href="${pageContext.request.contextPath}?command=forwardLotPage&lotId=${lot.lot.id}">
                            <fmt:message key="layout.view"/>
                        </a>
                        <c:if test="${isAdmin}">
                            <br>
                            <a href="${pageContext.request.contextPath}?command=removeLot&lotId=${lot.lot.id}">
                                <fmt:message key="layout.removeLot"/>
                            </a>
                        </c:if>

                    </td>
                </tr>

            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="mdl-cell--4-col"></div>
</div>