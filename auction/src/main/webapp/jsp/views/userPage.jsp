<%@ page contentType="text/html;charset=UTF-8" language="java" session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="in" tagdir="/WEB-INF/tags/input" %>
<div class="mdl-grid">
    <div class="mdl-cell--4-col"></div>
    <div class="mdl-cell--4-col">
        <c:choose>
            <c:when test="${not empty sessionScope.user}">
                    <h3>
                        <fmt:message key="layout.accountInfo"/>
                    </h3>
                    <ul class="mdl-list" style="margin-bottom: 0">
                        <li class="mdl-list__item"><fmt:message key="layout.user.login"/>: ${sessionScope.user.userAccount.login}</li>
                        <li class="mdl-list__item"><fmt:message key="layout.user.email"/>: ${sessionScope.user.userAccount.email}</li>
                        <li class="mdl-list__item"><fmt:message key="layout.user.purse"/>: ${sessionScope.user.userAccount.purse} BYN</li>
                        <li class="mdl-list__item"><fmt:message key="layout.user.frozen_purse"/>: ${sessionScope.user.userAccount.frozenPurse} BYN</li>
                        <li class="mdl-list__item">
                            <form method="post" action="${pageContext.request.contextPath}/">
                                <input type="hidden" name="command" value="fillPurse">
                                <in:input-numeric label="layout.purse.fill" param_name="value" error="layout.should_be_number"/>
                                <input type="submit"
                                       class="mdl-button mdl-js-button mdl-button--raised"
                                       value="<fmt:message key="layout.fill"/>">
                            </form>
                        </li>
                    </ul>

                    <form action="${pageContext.request.contextPath}/" method="POST">
                    <input type="hidden" value="updateUserContact" name="command" />
                    <ul class="mdl-list">
                        <h3>
                            <fmt:message key="layout.personalInfo"/>
                        </h3>
                        <li class="mdl-list__item">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input"
                                       id="firstName"
                                       type="text"
                                       name="firstName"
                                <c:if test="${not empty sessionScope.user.userContact.firstName}">
                                    value="<c:out value="${sessionScope.user.userContact.firstName}"/>"
                                </c:if>
                                >
                                <label class="mdl-textfield__label" for="firstName">
                                    <fmt:message key="layout.user.firstName"/>
                                </label>
                            </div>
                        </li>
                        <li class="mdl-list__item">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input"
                                       id="lastName"
                                       type="text"
                                       name="lastName"
                                <c:if test="${not empty sessionScope.user.userContact.lastName}">
                                       value="<c:out value="${sessionScope.user.userContact.lastName}"/>"
                                </c:if>
                                >
                                <label class="mdl-textfield__label" for="lastName">
                                    <fmt:message key="layout.user.lastName"/>
                                </label>
                            </div>
                        </li>
                        <li class="mdl-list__item">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input"
                                       id="phone"
                                       type="text"
                                       name="phone"
                                <c:if test="${not empty sessionScope.user.userContact.phone}">
                                       value="<c:out value="${sessionScope.user.userContact.phone}"/>"
                                </c:if>
                                >
                                <label class="mdl-textfield__label" for="phone">
                                    <fmt:message key="layout.user.phone"/>
                                </label>
                            </div>
                        </li>

                        <li class="mdl-list__item">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input"
                                       id="contactEmail"
                                       type="text"
                                       name="contactEmail"
                                <c:if test="${not empty sessionScope.user.userContact.contactEmail}">
                                       value="<c:out value="${sessionScope.user.userContact.contactEmail}"/>"
                                </c:if>
                                >
                                <label class="mdl-textfield__label" for="contactEmail">
                                    <fmt:message key="layout.user.contactEmail"/>
                                </label>
                            </div>
                        </li>

                        <li class="mdl-list__item">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input"
                                       id="address"
                                       type="text"
                                       name="address"
                                <c:if test="${not empty sessionScope.user.userContact.address}">
                                       value="<c:out value="${sessionScope.user.userContact.address}"/>"
                                </c:if>
                                >
                                <label class="mdl-textfield__label" for="address">
                                    <fmt:message key="layout.user.address"/>
                                </label>
                            </div>
                        </li>


                    </ul>
                    <input class="mdl-button mdl-js-button mdl-button--raised" type="submit" value="<fmt:message key="layout.button.saveChanges"/>">
                </form>

            </c:when>
            <c:otherwise>
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--12-col">
                        First log in or sign up
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="mdl-cell--4-col"></div>
</div>