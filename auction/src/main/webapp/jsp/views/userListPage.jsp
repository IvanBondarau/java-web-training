<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="by.epam.training.ApplicationConstant" %>
<c:if test="${not empty sessionScope.user}">
    <c:forEach var="role" items="${sessionScope.user.roles}">
        <c:if test="${role.name.equals(ApplicationConstant.ADMIN_USER_ROLE_NAME)}">
            <c:set var="isAdmin" value="${true}"/>
        </c:if>
    </c:forEach>
</c:if>
<c:choose>
    <c:when test="${(not empty sessionScope.user) && (isAdmin)}">
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
            <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.login"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.email"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.purse"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.frozen_purse"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.roles"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.name"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.contactEmail"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.phone"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.address"/>
                </th>
                <th class="mdl-data-table__cell--non-numeric">
                    <fmt:message key="layout.user.actions"/>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${sessionScope.userList}" var="user">
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">${user.userAccount.login}</td>
                    <td class="mdl-data-table__cell--non-numeric">${user.userAccount.email}</td>
                    <td class="mdl-data-table__cell">${user.userAccount.purse}</td>
                    <td class="mdl-data-table__cell">${user.userAccount.frozenPurse}</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <c:forEach var="role" items="${user.roles}">
                            ${role.name}
                        </c:forEach>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <c:choose>
                            <c:when test="${(not empty user.userContact.firstName) || (not empty user.userContact.lastName)}">
                                <c:set var="firstName" value="${empty user.userContact.firstName ? '' : user.userContact.firstName}"/>
                                <c:set var="lastName" value="${empty user.userContact.lastName ? '' : ' '.concat(user.userContact.lastName)}"/>
                                ${firstName.concat(lastName)}
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="layout.notSpecified"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <c:choose>
                            <c:when test="${not empty user.userContact.contactEmail}">
                                ${user.userContact.contactEmail}
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="layout.notSpecified"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <c:choose>
                            <c:when test="${not empty user.userContact.phone}">
                                ${user.userContact.phone}
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="layout.notSpecified"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <c:choose>
                            <c:when test="${not empty user.userContact.address}">
                                ${user.userContact.address}
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="layout.notSpecified"/>
                            </c:otherwise>
                        </c:choose>
                    </td>

                    <td class="mdl-data-table__cell--non-numeric">
                        <c:if test="${user != sessionScope.user}">
                            <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=removeUserById&userId=${user.userAccount.id}">
                                <fmt:message key="layout.ban"/>
                            </a>
                            <c:set var="currentUserAdmin" value="${false}"/>
                            <c:forEach var="role" items="${user.roles}">
                                <c:if test="${role.name.equals(ApplicationConstant.ADMIN_USER_ROLE_NAME)}">
                                    <c:set var="currentUserAdmin" value="${true}"/>
                                </c:if>
                            </c:forEach>
                            <c:if test="${not currentUserAdmin}">
                            <br>
                            <a class="mdl-navigation__link" href="${pageContext.request.contextPath}?command=promoteToAdmin&userId=${user.userAccount.id}">

                                <fmt:message key="layout.promote"/>
                            </a>
                            </c:if>
                        </c:if>
                    </td>

                </tr>

            </c:forEach>
            </tbody>
        </table>
    </c:when>
    <c:otherwise>
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
            <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric">
                        <fmt:message key="layout.user.login"/>
                    </th>
                    <th class="mdl-data-table__cell--non-numeric">
                        <fmt:message key="layout.user.name"/>
                    </th>
                    <th class="mdl-data-table__cell--non-numeric">
                        <fmt:message key="layout.user.contactEmail"/>
                    </th>
                    <th class="mdl-data-table__cell--non-numeric">
                        <fmt:message key="layout.user.phone"/>
                    </th>
                    <th class="mdl-data-table__cell--non-numeric">
                        <fmt:message key="layout.user.address"/></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${sessionScope.userList}" var="user">
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">${user.userAccount.login}</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <c:choose>
                                <c:when test="${(not empty user.userContact.firstName) || (not empty user.userContact.lastName)}">
                                    <c:set var="firstName" value="${empty user.userContact.firstName ? '' : user.userContact.firstName}"/>
                                    <c:set var="lastName" value="${empty user.userContact.lastName ? '' : ' '.concat(user.userContact.lastName)}"/>
                                    ${firstName.concat(lastName)}
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="layout.notSpecified"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <c:choose>
                                <c:when test="${not empty user.userContact.contactEmail}">
                                    ${user.userContact.contactEmail}
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="layout.notSpecified"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <c:choose>
                                <c:when test="${not empty user.userContact.phone}">
                                    ${user.userContact.phone}
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="layout.notSpecified"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <c:choose>
                                <c:when test="${not empty user.userContact.address}">
                                    ${user.userContact.address}
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="layout.notSpecified"/>
                                </c:otherwise>
                            </c:choose>
                        </td>

                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:otherwise>
</c:choose>
