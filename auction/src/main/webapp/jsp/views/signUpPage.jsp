<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="mdl-grid">
    <div class="mdl-cell--4-col"></div>
    <div class="mdl-cell--4-col">
        <h3>
            <fmt:message key="layout.title.signUp"/>
        </h3>
        <form action="${pageContext.request.contextPath}/" method="POST">
            <ul class="mdl-list">
                <input type="hidden" value="signUpUser" name="command" />
                <li class="mdl-list__item">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="_user.email" name="email"
                        <c:if test="${not empty requestScope.email}">
                                value="${requestScope.email}"
                        </c:if> >
                        <label class="mdl-textfield__label" for="_user.email">
                            <fmt:message key="layout.user.email"/>
                        </label>
                    </div>
                </li>
                <li class="mdl-list__item">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="_user.login" name="login"
                        <c:if test="${not empty requestScope.login}">
                               value="${requestScope.login}"
                        </c:if> >
                        <label class="mdl-textfield__label" for="_user.login">
                            <fmt:message key="layout.user.login"/>
                        </label>
                    </div>
                </li>
                <li class="mdl-list__item">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="password" id="_user.password" name="password">
                        <label class="mdl-textfield__label" for="_user.password">
                            <fmt:message key="layout.user.password"/>
                        </label>
                    </div>
                </li>
                <li class="mdl-list__item">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="password" id="_user.passwordRepeat" name="passwordRepeat">
                        <label class="mdl-textfield__label" for="_user.passwordRepeat">
                            <fmt:message key="layout.user.passwordRepeat"/>
                        </label>
                    </div>
                </li>
                <c:if test="${not empty requestScope.error}">
                    <li class="mdl-list__item">
                        <span class="mdl-color-text--red">
                            <fmt:message key="${requestScope.error}"/>
                        </span>
                    </li>
                </c:if>
                <li class="mdl-list__item">
                    <input class="mdl-button mdl-js-button mdl-button--raised" type="submit" value="<fmt:message key="layout.button.signUp"/>" />
                </li>

            </ul>
        </form>

    </div>
    <div class="mdl-cell--4-col"></div>
</div>