<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="by.epam.training.ApplicationConstant" %>

<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="layout" scope="session"/>

<html>
<meta charset="UTF-8">
    <head>
        <title>Online auction</title>
        <link rel="stylesheet" href="static/style/material.min.css" />
        <link rel="stylesheet" href="static/style/styles.css">
        <script defer src="static/style/material.min.js"></script>
    </head>
    <body>
        <div class="mdl-grid">
            <div class="mdl-cell--4-col"></div>
            <div class="mdl-cell--4-col">
                <div class="mdl-grid">
                    <div class="mdl-cell--12-col">
                        <h3>
                            <fmt:message key="layout.error"/>
                        </h3>
                    </div>
                    <div class="mdl-cell--12-col">
                        <a href="?command=${ApplicationConstant.FORWARD_HOME_PAGE}">
                            <fmt:message key="layout.return_home"/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="mdl-cell--4-col"></div>
        </div>
    </body>
</html>
